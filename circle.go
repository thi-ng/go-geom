package geom

import (
	"fmt"
	"math"

	"go.thi.ng/dcons"
	"go.thi.ng/geom/math64"
	"go.thi.ng/geom/vec"
)

// Circle2 defines a 2D circle shape
type Circle2 struct {
	Pos vec.Vec2
	R   float64
	attributed
}

// NewCircle creates a new circle at given position and radius
func NewCircle(pos *vec.Vec2, r float64) *Circle2 {
	return &Circle2{
		Pos: *pos,
		R:   r}
}

// NewCircleWithRadius creates a new circle with given radius around
// world origin
func NewCircleWithRadius(r float64) *Circle2 {
	return &Circle2{R: r}
}

func NewBoundingCircleFromVertexList(c *vec.Vec2, verts *dcons.DCons) *Circle2 {
	maxd := -1.0
	for cell, n := verts.Head, verts.Len(); n > 0; n-- {
		d := cell.Value.(*vec.Vec2).DistSq(c)
		if d > maxd {
			maxd = d
		}
		cell = cell.Next
	}
	return NewCircle(c, math.Sqrt(maxd))
}

func (c *Circle2) Copy() *Circle2 {
	return &Circle2{
		Pos:        c.Pos,
		R:          c.R,
		attributed: c.attributed.Copy()}
}

func (c *Circle2) Area() float64 {
	return math.Pi * c.R * c.R
}

func (c *Circle2) Circumference() float64 {
	return 2 * math.Pi * c.R
}

func (c *Circle2) BoundingCircle() *Circle2 {
	return c
}

func (c *Circle2) BoundingRect() *Rect2 {
	return NewRect(c.Pos.Copy().SubN(c.R), &vec.Vec2{c.R * 2, c.R * 2})
}

func (c *Circle2) Width() float64 {
	return c.R * 2
}

func (c *Circle2) Height() float64 {
	return c.R * 2
}

func (c *Circle2) Depth() float64 {
	return 0
}

func (c *Circle2) Center(o *vec.Vec2) *Circle2 {
	c.Pos.Set(o)
	return c
}

func (c *Circle2) Centroid() *vec.Vec2 {
	return c.Pos.Copy()
}

func (c *Circle2) ClassifyPoint(p *vec.Vec2) float64 {
	return math64.Sign(c.R*c.R - c.Pos.DistSq(p))
}

func (c *Circle2) ContainsPoint(p *vec.Vec2) bool {
	return c.Pos.DistSq(p) <= c.R*c.R
}

func (c *Circle2) Vertices(n int) []*vec.Vec2 {
	verts := make([]*vec.Vec2, n)
	delta := math64.Tau / float64(n)
	for i := 0; i < n; i++ {
		verts[i] = (&vec.Vec2{c.R, float64(i) * delta}).Cartesian().Add(&c.Pos)
	}
	return verts
}

func (c *Circle2) Edges(n int) []*Line2 {
	return EdgesFromVertices2(c.Vertices(n), true)
}

func (c *Circle2) String() string {
	return fmt.Sprintf(
		"Circle2 {pos: %v, r: %.4f, attribs: %v}",
		c.Pos, c.R, c.attribs)
}

func (c *Circle2) Svg() string {
	return fmt.Sprintf(
		`<circle cx="%.4f" cy="%.4f" r="%.4f" %s/>`,
		c.Pos[0], c.Pos[1], c.R, c.attributed.Svg())
}
