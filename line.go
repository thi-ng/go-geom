package geom

import (
	"fmt"
	"strings"

	"go.thi.ng/dcons"
	"go.thi.ng/geom/math64"
	"go.thi.ng/geom/vec"
)

type Line2 struct {
	A, B vec.Vec2
	attributed
}

type Line3 struct {
	A, B vec.Vec3
	attributed
}

type LineStrip2 struct {
	Points *dcons.DCons
	attributed
}

type LineStrip3 struct {
	Points *dcons.DCons
	attributed
}

func NewLine2(a, b *vec.Vec2) *Line2 {
	return &Line2{A: *a, B: *b}
}

func NewLine3(a, b *vec.Vec3) *Line3 {
	return &Line3{A: *a, B: *b}
}

func NewLineStrip2WithVertices(pts []*vec.Vec2) *LineStrip2 {
	n := len(pts)
	verts := dcons.New()
	l := &LineStrip2{Points: verts}
	for i := 0; i < n; i++ {
		verts.Push(pts[i])
	}
	return l
}

func NewLineStrip3WithVertices(pts []*vec.Vec3) *LineStrip3 {
	n := len(pts)
	verts := dcons.New()
	l := &LineStrip3{Points: verts}
	for i := 0; i < n; i++ {
		verts.Push(pts[i])
	}
	return l
}

func EdgesFromVertices2(verts []*vec.Vec2, closed bool) []*Line2 {
	n := len(verts) - 1
	m := n
	if closed {
		m++
	}
	edges := make([]*Line2, m)
	for i := 0; i < n; i++ {
		edges[i] = NewLine2(verts[i], verts[i+1])
	}
	if closed {
		edges[n] = NewLine2(verts[n], verts[0])
	}
	return edges
}

func EdgesFromVertices3(verts []*vec.Vec3, closed bool) []*Line3 {
	n := len(verts) - 1
	m := n
	if closed {
		m++
	}
	edges := make([]*Line3, m)
	for i := 0; i < n; i++ {
		edges[i] = NewLine3(verts[i], verts[i+1])
	}
	if closed {
		edges[n] = NewLine3(verts[n], verts[0])
	}
	return edges
}

func (l *Line2) Copy() *Line2 {
	return &Line2{
		A:          l.A,
		B:          l.B,
		attributed: l.attributed.Copy(),
	}
}

func (l *Line3) Copy() *Line3 {
	return &Line3{
		A:          l.A,
		B:          l.B,
		attributed: l.attributed.Copy(),
	}
}

func (p *LineStrip2) Copy() *LineStrip2 {
	return &LineStrip2{
		Points:     CopyVertexList2(p.Points, false),
		attributed: p.attributed.Copy()}
}

func (p *LineStrip3) Copy() *LineStrip3 {
	return &LineStrip3{
		Points:     CopyVertexList3(p.Points, false),
		attributed: p.attributed.Copy()}
}

func (l *Line2) BoundingCircle() *Circle2 {
	dir := l.B.Copy().Sub(&l.A)
	r := dir.Mag() / 2
	return NewCircle(dir.MulN(0.5).Add(&l.A), r)
}

func (l *LineStrip2) BoundingCircle() *Circle2 {
	return NewBoundingCircleFromVertexList(l.Centroid(), l.Points)
}

func (l *Line2) BoundingRect() *Rect2 {
	return NewRectFromMinMax(
		l.A.Copy().Min(&l.B),
		l.A.Copy().Max(&l.B))
}

func (l *LineStrip2) BoundingRect() *Rect2 {
	return NewBoundingRectFromVertexList(l.Points)
}

func (l *Line2) Width() float64 {
	return math64.AbsDiff(l.A[0], l.B[0])
}

func (l *Line3) Width() float64 {
	return math64.AbsDiff(l.A[0], l.B[0])
}

func (l *Line2) Height() float64 {
	return math64.AbsDiff(l.A[1], l.B[1])
}

func (l *Line3) Height() float64 {
	return math64.AbsDiff(l.A[1], l.B[1])
}

func (l *Line2) Depth() float64 {
	return 0
}

func (l *Line3) Depth() float64 {
	return math64.AbsDiff(l.A[2], l.B[2])
}

func (l *Line2) Center(o *vec.Vec2) *Line2 {
	c := l.A.Copy().Mix(&l.B, -0.5)
	if o != nil {
		c.Add(o)
	}
	l.A.Add(c)
	l.B.Add(c)
	return l
}

func (l *Line3) Center(o *vec.Vec3) *Line3 {
	c := l.A.Copy().Mix(&l.B, -0.5)
	if o != nil {
		c.Add(o)
	}
	l.A.Add(c)
	l.B.Add(c)
	return l
}

func (l *Line2) Centroid() *vec.Vec2 {
	return l.A.Copy().Mix(&l.B, 0.5)
}

func (l *Line3) Centroid() *vec.Vec3 {
	return l.A.Copy().Mix(&l.B, 0.5)
}

func (l *LineStrip2) Centroid() *vec.Vec2 {
	return CentroidFromVertexList2(l.Points)
}

func (l *LineStrip3) Centroid() *vec.Vec3 {
	return CentroidFromVertexList3(l.Points)
}

func (l *Line2) PointAt(t float64) *vec.Vec2 {
	return l.A.Copy().Mix(&l.B, t)
}

func (l *Line3) PointAt(t float64) *vec.Vec3 {
	return l.A.Copy().Mix(&l.B, t)
}

func (l *Line2) HasEndPoint(p *vec.Vec2, eps float64) bool {
	return l.A.EqDelta(p, eps) ||
		l.B.EqDelta(p, eps)
}

func (l *Line3) HasEndPoint(p *vec.Vec3, eps float64) bool {
	return l.A.EqDelta(p, eps) ||
		l.B.EqDelta(p, eps)
}

func (l *Line2) Length() float64 {
	return l.A.Dist(&l.B)
}

func (l *Line3) Length() float64 {
	return l.A.Dist(&l.B)
}

func (l *LineStrip2) Length() float64 {
	return ArcLengthVertexList2(l.Points, false)
}

func (l *LineStrip3) Length() float64 {
	return ArcLengthVertexList3(l.Points, false)
}

func (l *Line2) Direction() *vec.Vec2 {
	return l.B.Copy().Sub(&l.A).Normalize(1)
}

func (l *Line3) Direction() *vec.Vec3 {
	return l.B.Copy().Sub(&l.A).Normalize(1)
}

func (l *Line2) Vertices() []*vec.Vec2 {
	verts := [2]*vec.Vec2{&l.A, &l.B}
	return verts[:]
}

func (l *Line3) Vertices() []*vec.Vec3 {
	verts := [2]*vec.Vec3{&l.A, &l.B}
	return verts[:]
}

func (l *Line2) String() string {
	return fmt.Sprintf(
		"Line2 {a: %v, b: %v, attribs: %v}",
		l.A, l.B, l.attribs)
}

func (l *Line3) String() string {
	return fmt.Sprintf(
		"Line3 {a: %v, b: %v, attribs: %v}",
		l.A, l.B, l.attribs)
}

func (l *Line2) Svg() string {
	return fmt.Sprintf(
		`<line x1="%.4f" y1="%.4f" x2="%.4f" y2="%.4f" %s/>`,
		l.A[0], l.A[1], l.B[0], l.B[1], l.attributed.Svg())
}

func (l *LineStrip2) Svg() string {
	str := &strings.Builder{}
	str.WriteString(`<polyline points="`)
	FormatSVGPointList(str, l.Points)
	fmt.Fprintf(str, `" fill="none" %s/>`, l.attributed.Svg())
	return str.String()
}

func (l *LineStrip2) Subdivide(n int, t float64) *LineStrip2 {
	var dest *dcons.DCons
	for src := l.Points; n > 0; n-- {
		dest = dcons.New()
		dest.Push(src.Head.Value.(*vec.Vec2).Copy())
		src.Do(func(v *dcons.Cell) bool {
			a := v.Value.(*vec.Vec2)
			if v.Next != nil {
				b := v.Next.Value.(*vec.Vec2)
				dest.Push(a.Copy().Mix(b, t))
				dest.Push(b.Copy().Mix(a, t))
			} else {
				dest.Push(a.Copy())
			}
			return true
		})
		src = dest
	}
	return &LineStrip2{
		Points:     dest,
		attributed: l.attributed.Copy(),
	}
}
