package geom

import (
	"math"

	"go.thi.ng/geom/math64"
	"go.thi.ng/geom/vec"
)

// Quat represents a 4D quaternion
type Quat [4]float64

func NewQuat(x, y, z, w float64) *Quat {
	return &Quat{x, y, z, w}
}

func NewQuatFromAxisAngle(axis *vec.Vec3, theta float64) *Quat {
	theta /= 2
	s := math.Sin(theta)
	return &Quat{
		s * axis[0],
		s * axis[1],
		s * axis[2],
		math.Cos(theta),
	}
}

func (q *Quat) Elem() (x, y, z, w float64) {
	return q[0], q[1], q[2], q[3]
}

func (q *Quat) Copy() *Quat {
	return &Quat{q[0], q[1], q[2], q[3]}
}

func (q *Quat) EqDelta(q2 *Quat, eps float64) bool {
	return math64.EqDelta(q[0], q2[0], eps) &&
		math64.EqDelta(q[1], q2[1], eps) &&
		math64.EqDelta(q[2], q2[2], eps) &&
		math64.EqDelta(q[3], q2[3], eps)
}

func (q *Quat) Mul(q2 *Quat) *Quat {
	ax, ay, az, aw := q.Elem()
	bx, by, bz, bw := q2.Elem()
	q[0] = ax*bw + aw*bx + ay*bz - az*by
	q[1] = ay*bw + aw*by + az*bx - ax*bz
	q[2] = az*bw + aw*bz + ax*by - ay*bx
	q[3] = aw*bw - ax*bx - ay*by - az*bz
	return q
}

func (q *Quat) Dot(q2 *Quat) float64 {
	return q[0]*q2[0] + q[1]*q2[1] + q[2]*q2[2] + q[3]*q2[3]
}

func (q *Quat) MagSq() float64 {
	return q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]
}

func (q *Quat) Mag() float64 {
	return math.Sqrt(q.MagSq())
}

func (q *Quat) Normalize() *Quat {
	m := q.Mag()
	if m > 0 {
		m := 1 / m
		q[0] *= m
		q[1] *= m
		q[2] *= m
		q[3] *= m
	}
	return q
}

func (q *Quat) Conjugate() *Quat {
	q[0] *= -1
	q[1] *= -1
	q[2] *= -1
	return q
}

func (q *Quat) Invert() *Quat {
	d := q.MagSq()
	if d > math64.Eps {
		d = 1 / d
	} else {
		d = 0
	}
	q[0] *= -d
	q[1] *= -d
	q[2] *= -d
	q[3] *= d
	return q
}

// Mix performs spherical interpolation (SLERP) to target quaternion
func (q *Quat) Mix(q2 *Quat, t float64) *Quat {
	d := q.Dot(q2)
	if math.Abs(d) < 1 {
		theta := math.Acos(d)
		stheta := math.Sqrt(1 - d*d)
		var u, v float64
		if math.Abs(stheta) < 1e-4 {
			u = 0.5
			v = 0.5
		} else {
			u = math.Sin(theta*(1-t)) / stheta
			v = math.Sin(theta*t) / stheta
		}
		q[0] = q[0]*u + q2[0]*v
		q[1] = q[1]*u + q2[1]*v
		q[2] = q[2]*u + q2[2]*v
		q[3] = q[3]*u + q2[3]*v
	}
	return q
}

func (q *Quat) MulVec3(v *vec.Vec3) *vec.Vec3 {
	qx, qy, qz, qw := q.Elem()
	vx, vy, vz := v.Elem()
	ix := qw*vx + qy*vz - qz*vy
	iy := qw*vy + qz*vx - qx*vz
	iz := qw*vz + qx*vy - qy*vx
	iw := -qx*vx - qy*vy - qz*vz
	v[0] = ix*qw - iw*qx - iy*qz + iz*qy
	v[1] = iy*qw - iw*qy - iz*qx + ix*qz
	v[2] = iz*qw - iw*qz - ix*qy + iy*qx
	return v
}
