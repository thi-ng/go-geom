package math64

import (
	"math"
	"math/rand"
)

type (
	ArrayF64 []float64
)

const (
	Eps float64 = 1e-6

	Pi     = math.Pi
	Tau    = math.Pi * 2
	HalfPi = math.Pi / 2

	Deg2Rad = Pi / 180
	Rad2Deg = 180 / Pi

	Sqrt2 = 0.7071067811865475244008443621048490
)

func AbsDiff(a, b float64) float64 {
	if a < b {
		return b - a
	}
	return a - b
}

func Sign(x float64) float64 {
	if x < 0 {
		return -1
	}
	if x > 0 {
		return 1
	}
	return 0
}

func Min(a, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

func Min3(a, b, c float64) float64 {
	return Min(Min(a, b), c)
}

func Max(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}

func Max3(a, b, c float64) float64 {
	return Max(Max(a, b), c)
}

func Clamp(x, a, b float64) float64 {
	if x < a {
		return a
	}
	if x > b {
		return b
	}
	return x
}

func EqDelta(x, y, eps float64) bool {
	return AbsDiff(x, y) <= eps
}

func InRange(x, min, max float64) bool {
	return x >= min && x <= max
}

func Radians(x float64) float64 {
	return x * Deg2Rad
}

func Degrees(x float64) float64 {
	return x * Rad2Deg
}

func Atan2Abs(y, x float64) float64 {
	t := math.Atan2(y, x)
	if t < 0 {
		return t + math.Pi
	}
	return t
}

func Frac(x float64) float64 {
	return x - math.Floor(x)
}

func Mod(x, y float64) float64 {
	return x - y*math.Floor(x/y)
}

func Mix(a, b, t float64) float64 {
	return (b-a)*t + a
}

// MixBilinear computes bilinear interpolation between a, b, c, d at
// normalized position u, v.
//
//     C   D   V
//     +---+   ^
//     |   |   |
//     +---+   +----> U
//     A   B
//
func MixBilinear(a, b, c, d, u, v float64) float64 {
	t := a + (b-a)*u
	return t + (c+(d-c)*u-t)*v
}

func MixCircular(a, b, t float64) float64 {
	t = 1 - t
	return (b-a)*math.Sqrt(1-t*t) + a
}

func MixCos(a, b, t float64) float64 {
	return (a-b)*(math.Cos(t*math.Pi)*0.5+0.5) + b
}

func MixExp(a, b, t, e float64) float64 {
	return (b-a)*math.Pow(t, e) + a
}

func MixDecimated(a, b, t, n float64) float64 {
	return (b-a)*math.Floor(t*n)/n + a
}

func Step(e, x float64) float64 {
	if x < e {
		return 0
	}
	return 1
}

func SmoothStep(e, e2, x float64) float64 {
	t := Clamp((x-e)/(e2-e), 0, 1)
	return (-2*t + 3) * t * t
}

func SmootherStep(e, e2, x float64) float64 {
	t := Clamp((x-e)/(e2-e), 0, 1)
	return t * t * t * (t*(t*6-15) + 10)
}

func Impulse(k, t float64) float64 {
	h := k * t
	return h * math.Exp(1-h)
}

func RandNorm() float64 {
	return rand.Float64()*2 - 1
}

func RandMinMax(min, max float64) float64 {
	return min + (max-min)*rand.Float64()
}
