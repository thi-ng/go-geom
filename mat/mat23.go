package mat

import (
	"fmt"
	"math"

	"go.thi.ng/geom/math32"
	"go.thi.ng/geom/math64"
	"go.thi.ng/geom/vec"
)

type Mat23 [6]float64

func NewMat23() *Mat23 {
	return &Mat23{
		1, 0,
		0, 1,
		0, 0}
}

func NewRotationMat23(theta float64) *Mat23 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat23{c, s, -s, c, 0, 0}
}

func NewUniformScaleMat23(s float64) *Mat23 {
	return &Mat23{s, 0, 0, s, 0, 0}
}

func NewScaleMat23(v *vec.Vec2) *Mat23 {
	return &Mat23{v[0], 0, 0, v[1], 0, 0}
}

func NewTranslationMat23(v *vec.Vec2) *Mat23 {
	return &Mat23{1, 0, 0, 1, v[0], v[1]}
}

func (m *Mat23) ToArrayF32(dest math32.ArrayF32, idx int) math32.ArrayF32 {
	for i := range m {
		dest[idx+i] = float32(m[i])
	}
	return dest
}

func (m *Mat23) ToArrayF64(dest math64.ArrayF64, idx int) math64.ArrayF64 {
	for i := range m {
		dest[idx+i] = m[i]
	}
	return dest
}

func (m *Mat23) Identity() *Mat23 {
	*m = [6]float64{
		1, 0,
		0, 1,
		0, 0}
	return m
}

func (m *Mat23) Elem() (m00, m01, m10, m11, m20, m21 float64) {
	m00 = m[0]
	m01 = m[1]
	m10 = m[2]
	m11 = m[3]
	m20 = m[4]
	m21 = m[5]
	return
}

func (m *Mat23) Copy() *Mat23 {
	mm := *m
	return &mm
}

func (m *Mat23) Column(n int, v *vec.Vec2) *vec.Vec2 {
	if v == nil {
		v = &vec.Vec2{}
	}
	n *= 2
	v[1] = m[n+1]
	v[0] = m[n]
	return v
}

func (m *Mat23) ColumnElem(n int) (a, b float64) {
	n *= 2
	b = m[n+1]
	a = m[n]
	return
}

func (m *Mat23) SetColumn(n int, v *vec.Vec2) *Mat23 {
	n *= 2
	m[n+1] = v[1]
	m[n] = v[0]
	return m
}

func (m *Mat23) Row(n int, v *vec.Vec3) *vec.Vec3 {
	if v == nil {
		v = &vec.Vec3{}
	}
	v[2] = m[n+4]
	v[1] = m[n+2]
	v[0] = m[n]
	return v
}

func (m *Mat23) RowElem(n int) (a, b, c float64) {
	c = m[n+4]
	b = m[n+2]
	a = m[n]
	return
}

func (m *Mat23) SetRow(n int, v *vec.Vec3) *Mat23 {
	m[n+4] = v[2]
	m[n+2] = v[1]
	m[n] = v[0]
	return m
}

func (m *Mat23) Determinant() float64 {
	return m[0]*m[3] - m[1]*m[2]
}

func (m *Mat23) Invert() (inv *Mat23, ok bool) {
	a0, a1, a2, a3, a4, a5 := m.Elem()
	det := m.Determinant()
	if det == 0 {
		return m, false
	}
	det = 1.0 / det
	m[0] = a3 * det
	m[1] = -a1 * det
	m[2] = -a2 * det
	m[3] = a0 * det
	m[4] = (a2*a5 - a3*a4) * det
	m[5] = (a1*a4 - a0*a5) * det
	return m, true
}

func (m *Mat23) MulV(v *vec.Vec2) *vec.Vec2 {
	x, y := v.Elem()
	v[0] = m[0]*x + m[2]*y + m[4]
	v[1] = m[1]*x + m[3]*y + m[5]
	return v
}

func (m *Mat23) Mul(m2 *Mat23) *Mat23 {
	m00, m01, m10, m11, m20, m21 := m.Elem()
	i, j := m2[0], m2[1]
	m[0] = m00*i + m10*j
	m[1] = m01*i + m11*j
	i, j = m2[2], m2[3]
	m[2] = m00*i + m10*j
	m[3] = m01*i + m11*j
	i, j = m2[4], m2[5]
	m[4] = m00*i + m10*j + m20
	m[5] = m01*i + m11*j + m21
	return m
}

func (m *Mat23) ToMat44() *Mat44 {
	return &Mat44{
		m[0], m[1], 0, 0,
		m[2], m[3], 0, 0,
		0, 0, 1, 0,
		m[4], m[5], 0, 1}
}

func (m *Mat23) String() string {
	return fmt.Sprintf(
		"|%9.4f %9.4f %9.4f |\n|%9.4f %9.4f %9.4f |\n",
		m[0], m[2], m[4], m[1], m[3], m[5],
	)
}

func (m *Mat23) Svg() string {
	return fmt.Sprintf(
		"matrix(%.4f %.4f %.4f %.4f %.4f %.4f)",
		m[0], m[1], m[2],
		m[3], m[4], m[5],
	)
}
