package mat

import (
	"fmt"

	"go.thi.ng/geom/vec"
)

const (
	TRS = iota
	TSR
	RST
	RTS
)

type Transform2 struct {
	pos   vec.Vec2
	scale vec.Vec2
	rot   float64
	mat   Mat23
	order int
	dirty bool
}

type Transform3 struct {
	pos   vec.Vec3
	scale vec.Vec3
	rot   vec.Vec3
	mat   Mat44
	order int
	dirty bool
}

func NewTransform2() *Transform2 {
	return &Transform2{
		scale: vec.Vec2{1, 1},
		order: TRS,
		dirty: true}
}

func NewTransform3() *Transform3 {
	return &Transform3{
		scale: vec.Vec3{1, 1},
		order: TRS,
		dirty: true}
}

func (t *Transform2) Matrix() *Mat23 {
	if t.dirty {
		t.Update()
	}
	return &t.mat
}

func (t *Transform3) Matrix() *Mat44 {
	if t.dirty {
		t.Update()
	}
	return &t.mat
}

func (t *Transform2) Update() *Mat23 {
	tmat := NewTranslationMat23(&t.pos)
	rmat := NewRotationMat23(t.rot)
	smat := NewScaleMat23(&t.scale)
	switch t.order {
	case TRS:
		t.mat = *tmat.Mul(rmat).Mul(smat)
	case TSR:
		t.mat = *tmat.Mul(smat).Mul(rmat)
	case RST:
		t.mat = *rmat.Mul(smat).Mul(tmat)
	case RTS:
		t.mat = *rmat.Mul(tmat).Mul(smat)
	default:
		panic(fmt.Sprint("invalid transform order:", t.order))
	}
	t.dirty = false
	return &t.mat
}

func (t *Transform3) Update() *Mat44 {
	tmat := NewTranslationMat44(&t.pos)
	smat := NewScaleMat44(&t.scale)
	rmat := NewRotationXMat44(t.rot[0]).
		Mul(NewRotationYMat44(t.rot[1])).
		Mul(NewRotationZMat44(t.rot[2]))
	switch t.order {
	case TRS:
		t.mat = *tmat.Mul(rmat).Mul(smat)
	case TSR:
		t.mat = *tmat.Mul(smat).Mul(rmat)
	case RST:
		t.mat = *rmat.Mul(smat).Mul(tmat)
	case RTS:
		t.mat = *rmat.Mul(tmat).Mul(smat)
	default:
		panic(fmt.Sprint("invalid transform order:", t.order))
	}
	t.dirty = false
	return &t.mat
}

func (t *Transform2) Pos() vec.Vec2 {
	return t.pos
}

func (t *Transform2) SetPos(v *vec.Vec2) *Transform2 {
	t.pos.Set(v)
	t.dirty = true
	return t
}

func (t *Transform3) Pos() vec.Vec3 {
	return t.pos
}

func (t *Transform3) SetPos(v *vec.Vec3) *Transform3 {
	t.pos.Set(v)
	t.dirty = true
	return t
}

func (t *Transform2) Scale() vec.Vec2 {
	return t.scale
}

func (t *Transform2) SetUniformScale(s float64) *Transform2 {
	t.scale.SetN(s)
	t.dirty = true
	return t
}

func (t *Transform2) SetScale(v *vec.Vec2) *Transform2 {
	t.scale.Set(v)
	t.dirty = true
	return t
}

func (t *Transform3) Scale() vec.Vec3 {
	return t.scale
}

func (t *Transform3) SetUniformScale(s float64) *Transform3 {
	t.scale.SetN(s)
	t.dirty = true
	return t
}

func (t *Transform3) SetScale(v *vec.Vec3) *Transform3 {
	t.scale.Set(v)
	t.dirty = true
	return t
}

func (t *Transform2) Rotation() float64 {
	return t.rot
}

func (t *Transform2) SetRotation(theta float64) *Transform2 {
	t.rot = theta
	t.dirty = true
	return t
}

func (t *Transform3) Rotation() vec.Vec3 {
	return t.scale
}

func (t *Transform3) SetRotation(rot *vec.Vec3) *Transform3 {
	t.rot.Set(rot)
	t.dirty = true
	return t
}

func (t *Transform2) Order() int {
	return t.order
}

func (t *Transform2) SetOrder(order int) *Transform2 {
	t.order = order
	t.dirty = true
	return t
}

func (t *Transform3) Order() int {
	return t.order
}

func (t *Transform3) SetOrder(order int) *Transform3 {
	t.order = order
	t.dirty = true
	return t
}

func (t *Transform2) String() string {
	return fmt.Sprintf(
		"Transform2 {pos: %v, scale: %v, rot: %.4f}",
		t.pos, t.scale, t.rot,
	)
}

func (t *Transform3) String() string {
	return fmt.Sprintf(
		"Transform3 {pos: %v, scale: %v, rot: %.4f}",
		t.pos, t.scale, t.rot,
	)
}

func (t *Transform2) Svg() string {
	return t.Matrix().Svg()
}
