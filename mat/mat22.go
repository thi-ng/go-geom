package mat

import (
	"fmt"
	"math"

	"go.thi.ng/geom/math32"
	"go.thi.ng/geom/math64"
	"go.thi.ng/geom/vec"
)

type Mat22 [4]float64

func NewMat22() *Mat22 {
	return &Mat22{1, 0, 0, 1}
}

func NewRotationMat22(theta float64) *Mat22 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat22{c, s, -s, c}
}

func NewScaleMat22(v *vec.Vec2) *Mat22 {
	return &Mat22{v[0], 0, 0, v[1]}
}

func (m *Mat22) ToArrayF32(dest math32.ArrayF32, idx int) math32.ArrayF32 {
	dest[idx+3] = float32(m[3])
	dest[idx+2] = float32(m[2])
	dest[idx+1] = float32(m[1])
	dest[idx] = float32(m[0])
	return dest
}

func (m *Mat22) ToArrayF64(dest math64.ArrayF64, idx int) math64.ArrayF64 {
	dest[idx+3] = m[3]
	dest[idx+2] = m[2]
	dest[idx+1] = m[1]
	dest[idx] = m[0]
	return dest
}

func (m *Mat22) Identity() *Mat22 {
	m[0] = 1
	m[1] = 0
	m[2] = 0
	m[3] = 1
	return m
}

func (m *Mat22) Elem() (m00, m01, m10, m11 float64) {
	m00 = m[0]
	m01 = m[1]
	m10 = m[2]
	m11 = m[3]
	return
}

func (m *Mat22) Copy() *Mat22 {
	mm := *m
	return &mm
}

func (m *Mat22) Transpose() *Mat22 {
	t := m[1]
	m[1] = m[2]
	m[2] = t
	return m
}

func (m *Mat22) Diag() *vec.Vec2 {
	return &vec.Vec2{m[0], m[3]}
}

func (m *Mat22) MulV(v *vec.Vec2) *vec.Vec2 {
	x, y := v.Elem()
	v[0] = m[0]*x + m[2]*y
	v[1] = m[1]*x + m[3]*y
	return v
}

func (m *Mat22) Mul(m2 *Mat22) *Mat22 {
	m00, m01, m10, m11 := m.Elem()
	i, j := m2[0], m2[1]
	m[0] = m00*i + m10*j
	m[1] = m01*i + m11*j
	i, j = m2[2], m2[3]
	m[2] = m00*i + m10*j
	m[3] = m01*i + m11*j
	return m
}

func (m *Mat22) String() string {
	return fmt.Sprintf(
		"|%9.4f %9.4f |\n|%9.4f %9.4f |\n",
		m[0], m[2],
		m[1], m[3],
	)
}
