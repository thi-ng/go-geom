package mat

import (
	"fmt"
	"math"

	"go.thi.ng/geom/math32"
	"go.thi.ng/geom/math64"
	"go.thi.ng/geom/vec"
)

type Mat44 [16]float64

func NewMat44() *Mat44 {
	return &Mat44{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1}
}

func NewRotationXMat44(theta float64) *Mat44 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat44{
		1, 0, 0, 0,
		0, c, s, 0,
		0, -s, c, 0,
		0, 0, 0, 1}
}

func NewRotationYMat44(theta float64) *Mat44 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat44{
		c, 0, -s, 0,
		0, 1, 0, 0,
		s, 0, c, 0,
		0, 0, 0, 1}
}

func NewRotationZMat44(theta float64) *Mat44 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat44{
		c, s, 0, 0,
		-s, c, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1}
}

func NewUniformScaleMat44(s float64) *Mat44 {
	return &Mat44{
		s, 0, 0, 0,
		0, s, 0, 0,
		0, 0, s, 0,
		0, 0, 0, 1}
}

func NewScaleMat44(s *vec.Vec3) *Mat44 {
	return &Mat44{
		s[0], 0, 0, 0,
		0, s[1], 0, 0,
		0, 0, s[2], 0,
		0, 0, 0, 1}
}

func NewTranslationMat44(v *vec.Vec3) *Mat44 {
	return &Mat44{
		1, 0, 0, v[0],
		0, 1, 0, v[1],
		0, 0, 1, v[2],
		0, 0, 0, 1}
}

func (m *Mat44) ToArrayF32(dest math32.ArrayF32, idx int) math32.ArrayF32 {
	for i := range m {
		dest[idx+i] = float32(m[i])
	}
	return dest
}

func (m *Mat44) ToArrayF64(dest math64.ArrayF64, idx int) math64.ArrayF64 {
	for i := range m {
		dest[idx+i] = m[i]
	}
	return dest
}

func (m *Mat44) Identity() *Mat44 {
	*m = [16]float64{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1}
	return m
}

func (m *Mat44) Copy() *Mat44 {
	mm := *m
	return &mm
}

func (m *Mat44) Elem() (m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33 float64) {
	m00 = m[0]
	m01 = m[1]
	m02 = m[2]
	m03 = m[3]
	m10 = m[4]
	m11 = m[5]
	m12 = m[6]
	m13 = m[7]
	m20 = m[8]
	m21 = m[9]
	m22 = m[10]
	m23 = m[11]
	m30 = m[12]
	m31 = m[13]
	m32 = m[14]
	m33 = m[15]
	return
}

func (m *Mat44) Column(n int, v *vec.Vec4) *vec.Vec4 {
	if v == nil {
		v = &vec.Vec4{}
	}
	n <<= 2
	v[3] = m[n+3]
	v[2] = m[n+2]
	v[1] = m[n+1]
	v[0] = m[n]
	return v
}

func (m *Mat44) ColumnElem(n int) (a, b, c, d float64) {
	n <<= 2
	d = m[n+3]
	c = m[n+2]
	b = m[n+1]
	a = m[n]
	return
}

func (m *Mat44) SetColumn(n int, v *vec.Vec4) *Mat44 {
	n <<= 2
	m[n+3] = v[3]
	m[n+2] = v[2]
	m[n+1] = v[1]
	m[n] = v[0]
	return m
}

func (m *Mat44) Row(n int, v *vec.Vec4) *vec.Vec4 {
	if v == nil {
		v = &vec.Vec4{}
	}
	v[3] = m[n+12]
	v[2] = m[n+8]
	v[1] = m[n+4]
	v[0] = m[n]
	return v
}

func (m *Mat44) RowElem(n int) (a, b, c, d float64) {
	d = m[n+12]
	c = m[n+8]
	b = m[n+4]
	a = m[n]
	return
}

func (m *Mat44) SetRow(n int, v *vec.Vec4) *Mat44 {
	m[n+12] = v[3]
	m[n+8] = v[2]
	m[n+4] = v[1]
	m[n] = v[0]
	return m
}

func (m *Mat44) Basis(x, y, z *vec.Vec3) (*vec.Vec3, *vec.Vec3, *vec.Vec3) {
	if x == nil {
		x = &vec.Vec3{}
	}
	if y == nil {
		y = &vec.Vec3{}
	}
	if z == nil {
		z = &vec.Vec3{}
	}
	x[0] = m[0]
	x[1] = m[1]
	x[2] = m[2]
	y[0] = m[4]
	y[1] = m[5]
	y[2] = m[6]
	z[0] = m[8]
	z[1] = m[9]
	z[2] = m[10]
	return x, y, z
}

func (m *Mat44) SetBasis(x, y, z *vec.Vec3) *Mat44 {
	m[0] = x[0]
	m[1] = x[1]
	m[2] = x[2]
	m[4] = y[0]
	m[5] = y[1]
	m[6] = y[2]
	m[8] = z[0]
	m[9] = z[1]
	m[10] = z[2]
	return m
}

func (m *Mat44) Transpose() *Mat44 {
	t := m[1]
	m[1] = m[4]
	m[4] = t
	t = m[2]
	m[2] = m[8]
	m[8] = t
	t = m[3]
	m[3] = m[12]
	m[12] = t
	t = m[6]
	m[6] = m[9]
	m[9] = t
	t = m[7]
	m[7] = m[13]
	m[13] = t
	t = m[11]
	m[11] = m[14]
	m[14] = t
	return m
}

func (m *Mat44) Diag() *vec.Vec4 {
	return &vec.Vec4{m[0], m[5], m[10], m[15]}
}

func (m *Mat44) determinantCoeffs() (a, b, c, d, e, f, g, h, i, j, k, l float64) {
	m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33 := m.Elem()
	return m00*m11 - m01*m10,
		m00*m12 - m02*m10,
		m00*m13 - m03*m10,
		m01*m12 - m02*m11,
		m01*m13 - m03*m11,
		m02*m13 - m03*m12,
		m20*m31 - m21*m30,
		m20*m32 - m22*m30,
		m20*m33 - m23*m30,
		m21*m32 - m22*m31,
		m21*m33 - m23*m31,
		m22*m33 - m23*m32
}

func (m *Mat44) Determinant() float64 {
	d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11 := m.determinantCoeffs()
	return d00*d11 - d01*d10 + d02*d09 + d03*d08 - d04*d07 + d05*d06
}

func (m *Mat44) Invert() (inv *Mat44, ok bool) {
	d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11 := m.determinantCoeffs()
	det := d00*d11 - d01*d10 + d02*d09 + d03*d08 - d04*d07 + d05*d06
	if det == 0 {
		return m, false
	}
	det = 1.0 / det
	m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33 := m.Elem()
	m[0] = (m11*d11 - m12*d10 + m13*d09) * det
	m[1] = (-m01*d11 + m02*d10 - m03*d09) * det
	m[2] = (m31*d05 - m32*d04 + m33*d03) * det
	m[3] = (-m21*d05 + m22*d04 - m23*d03) * det
	m[4] = (-m10*d11 + m12*d08 - m13*d07) * det
	m[5] = (m00*d11 - m02*d08 + m03*d07) * det
	m[6] = (-m30*d05 + m32*d02 - m33*d01) * det
	m[7] = (m20*d05 - m22*d02 + m23*d01) * det
	m[8] = (m10*d10 - m11*d08 + m13*d06) * det
	m[9] = (-m00*d10 + m01*d08 - m03*d06) * det
	m[10] = (m30*d04 - m31*d02 + m33*d00) * det
	m[11] = (-m20*d04 + m21*d02 - m23*d00) * det
	m[12] = (-m10*d09 + m11*d07 - m12*d06) * det
	m[13] = (m00*d09 - m01*d07 + m02*d06) * det
	m[14] = (-m30*d03 + m31*d01 - m32*d00) * det
	m[15] = (m20*d03 - m21*d01 + m22*d00) * det
	return m, true
}

func (m *Mat44) MulVec4(v *vec.Vec4) *vec.Vec4 {
	x, y, z, w := v.Elem()
	v[0] = m[0]*x + m[4]*y + m[8]*z + m[12]*w
	v[1] = m[1]*x + m[5]*y + m[9]*z + m[13]*w
	v[2] = m[2]*x + m[6]*y + m[10]*z + m[14]*w
	v[3] = m[3]*x + m[7]*y + m[11]*z + m[15]*w
	return v
}

func (m *Mat44) MulVec3(v *vec.Vec3) *vec.Vec3 {
	x, y, z := v.Elem()
	v[0] = m[0]*x + m[4]*y + m[8]*z + m[12]
	v[1] = m[1]*x + m[5]*y + m[9]*z + m[13]
	v[2] = m[2]*x + m[6]*y + m[10]*z + m[14]
	return v
}

func (m *Mat44) Mul(m2 *Mat44) *Mat44 {
	m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33 := m.Elem()
	i, j, k, l := m2[0], m2[1], m2[2], m2[3]
	m[0] = m00*i + m10*j + m20*k + m30*l
	m[1] = m01*i + m11*j + m21*k + m31*l
	m[2] = m02*i + m12*j + m22*k + m32*l
	m[3] = m03*i + m13*j + m23*k + m33*l
	i, j, k, l = m2[4], m2[5], m2[6], m2[7]
	m[4] = m00*i + m10*j + m20*k + m30*l
	m[5] = m01*i + m11*j + m21*k + m31*l
	m[6] = m02*i + m12*j + m22*k + m32*l
	m[7] = m03*i + m13*j + m23*k + m33*l
	i, j, k, l = m2[8], m2[9], m2[10], m2[11]
	m[8] = m00*i + m10*j + m20*k + m30*l
	m[9] = m01*i + m11*j + m21*k + m31*l
	m[10] = m02*i + m12*j + m22*k + m32*l
	m[11] = m03*i + m13*j + m23*k + m33*l
	i, j, k, l = m2[12], m2[13], m2[14], m2[15]
	m[12] = m00*i + m10*j + m20*k + m30*l
	m[13] = m01*i + m11*j + m21*k + m31*l
	m[14] = m02*i + m12*j + m22*k + m32*l
	m[15] = m03*i + m13*j + m23*k + m33*l
	return m
}

func (m *Mat44) ToMat33() *Mat33 {
	return &Mat33{
		m[0], m[1], m[2],
		m[4], m[5], m[6],
		m[8], m[9], m[10]}
}

func (m *Mat44) String() string {
	return fmt.Sprintf(
		"|%9.4f %9.4f %9.4f %9.4f |\n|%9.4f %9.4f %9.4f %9.4f |\n|%9.4f %9.4f %9.4f %9.4f |\n|%9.4f %9.4f %9.4f %9.4f |\n",
		m[0], m[4], m[8], m[12],
		m[1], m[5], m[9], m[13],
		m[2], m[6], m[10], m[14],
		m[3], m[7], m[11], m[15],
	)
}
