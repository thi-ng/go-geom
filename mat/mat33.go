package mat

import (
	"fmt"
	"math"

	"go.thi.ng/geom/math32"
	"go.thi.ng/geom/math64"
	"go.thi.ng/geom/vec"
)

type Mat33 [9]float64

func NewMat33() *Mat33 {
	return &Mat33{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1}
}

func NewRotationXMat33(theta float64) *Mat33 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat33{1, 0, 0, 0, c, s, 0, -s, c}
}

func NewRotationYMat33(theta float64) *Mat33 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat33{c, 0, -s, 0, 1, 0, s, 0, c}
}

func NewRotationZMat33(theta float64) *Mat33 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	return &Mat33{c, s, 0, -s, c, 0, 0, 0, 1}
}

func NewUniformScaleMat33(s float64) *Mat33 {
	return &Mat33{s, 0, 0, 0, s, 0, 0, 0, s}
}

func NewScaleMat33(s *vec.Vec3) *Mat33 {
	return &Mat33{s[0], 0, 0, 0, s[1], 0, 0, 0, s[2]}
}

func (m *Mat33) ToArrayF32(dest math32.ArrayF32, idx int) math32.ArrayF32 {
	for i := range m {
		dest[idx+i] = float32(m[i])
	}
	return dest
}

func (m *Mat33) ToArrayF64(dest math64.ArrayF64, idx int) math64.ArrayF64 {
	for i := range m {
		dest[idx+i] = m[i]
	}
	return dest
}

func (m *Mat33) Identity() *Mat33 {
	*m = [9]float64{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1}
	return m
}

func (m *Mat33) Elem() (m00, m01, m02, m10, m11, m12, m20, m21, m22 float64) {
	m00 = m[0]
	m01 = m[1]
	m02 = m[2]
	m10 = m[3]
	m11 = m[4]
	m12 = m[5]
	m20 = m[6]
	m21 = m[7]
	m22 = m[8]
	return
}

func (m *Mat33) Copy() *Mat33 {
	mm := *m
	return &mm
}

func (m *Mat33) Column(n int, v *vec.Vec3) *vec.Vec3 {
	if v == nil {
		v = &vec.Vec3{}
	}
	n *= 3
	v[2] = m[n+2]
	v[1] = m[n+1]
	v[0] = m[n]
	return v
}

func (m *Mat33) ColumnElem(n int) (a, b, c float64) {
	n *= 3
	c = m[n+2]
	b = m[n+1]
	a = m[n]
	return
}

func (m *Mat33) SetColumn(n int, v *vec.Vec3) *Mat33 {
	n *= 3
	m[n+2] = v[2]
	m[n+1] = v[1]
	m[n] = v[0]
	return m
}

func (m *Mat33) Row(n int, v *vec.Vec3) *vec.Vec3 {
	if v == nil {
		v = &vec.Vec3{}
	}
	v[2] = m[n+6]
	v[1] = m[n+3]
	v[0] = m[n]
	return v
}

func (m *Mat33) RowElem(n int) (a, b, c float64) {
	c = m[n+6]
	b = m[n+3]
	a = m[n]
	return
}

func (m *Mat33) SetRow(n int, v *vec.Vec3) *Mat33 {
	m[n+6] = v[2]
	m[n+3] = v[1]
	m[n] = v[0]
	return m
}

func (m *Mat33) Transpose() *Mat33 {
	t := m[1]
	m[1] = m[3]
	m[3] = t
	t = m[2]
	m[2] = m[6]
	m[6] = t
	t = m[5]
	m[5] = m[7]
	m[7] = t
	return m
}

func (m *Mat33) Diag() *vec.Vec3 {
	return &vec.Vec3{m[0], m[4], m[8]}
}

func (m *Mat33) Determinant() float64 {
	m00, m01, m02, m10, m11, m12, m20, m21, m22 := m.Elem()
	d01 := m22*m11 - m12*m21
	d11 := -m22*m10 + m12*m20
	d21 := m21*m10 - m11*m20
	return m00*d01 + m01*d11 + m02*d21
}

func (m *Mat33) Invert() (inv *Mat33, ok bool) {
	m00, m01, m02, m10, m11, m12, m20, m21, m22 := m.Elem()
	d01 := m22*m11 - m12*m21
	d11 := -m22*m10 + m12*m20
	d21 := m21*m10 - m11*m20
	det := m00*d01 + m01*d11 + m02*d21
	if det == 0 {
		return m, false
	}
	det = 1.0 / det
	m[0] = d01 * det
	m[1] = (-m22*m01 + m02*m21) * det
	m[2] = (m12*m01 - m02*m11) * det
	m[3] = d11 * det
	m[4] = (m22*m00 - m02*m20) * det
	m[5] = (-m12*m00 + m02*m10) * det
	m[6] = d21 * det
	m[7] = (-m21*m00 + m01*m20) * det
	m[8] = (m11*m00 - m01*m10) * det
	return m, true
}

func (m *Mat33) MulVec3(v *vec.Vec3) *vec.Vec3 {
	x, y, z := v.Elem()
	v[0] = m[0]*x + m[3]*y + m[6]*z
	v[1] = m[1]*x + m[4]*y + m[7]*z
	v[2] = m[2]*x + m[5]*y + m[8]*z
	return v
}

func (m *Mat33) Mul(m2 *Mat33) *Mat33 {
	m00, m01, m02, m10, m11, m12, m20, m21, m22 := m.Elem()
	i, j, k := m2[0], m2[1], m2[2]
	m[0] = m00*i + m10*j + m20*k
	m[1] = m01*i + m11*j + m21*k
	m[2] = m02*i + m12*j + m22*k
	i, j, k = m2[3], m2[4], m2[5]
	m[3] = m00*i + m10*j + m20*k
	m[4] = m01*i + m11*j + m21*k
	m[5] = m02*i + m12*j + m22*k
	i, j, k = m2[6], m2[7], m2[8]
	m[6] = m00*i + m10*j + m20*k
	m[7] = m01*i + m11*j + m21*k
	m[8] = m02*i + m12*j + m22*k
	return m
}

func (m *Mat33) String() string {
	return fmt.Sprintf(
		"|%9.4f %9.4f %9.4f |\n|%9.4f %9.4f %9.4f |\n|%9.4f %9.4f %9.4f |\n",
		m[0], m[3], m[6],
		m[1], m[4], m[7],
		m[2], m[5], m[8],
	)
}
