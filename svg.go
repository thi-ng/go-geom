package geom

import (
	"fmt"
	"strings"

	"go.thi.ng/dcons"
	"go.thi.ng/geom/vec"
)

const NamespaceSVG = "http://www.w3.org/2000/svg"

type (
	// SVGConvertible is implemented by types which can be serialized
	// into SVG format
	SVGConvertible interface {
		Svg() string
	}

	SVGGroup struct {
		Items []SVGConvertible
		attributed
	}

	SVGDoc struct {
		Defs []SVGConvertible
		SVGGroup
	}

	SVGText struct {
		Pos  vec.Vec2
		Body string
		attributed
	}

	SVGColl []SVGConvertible

	SVGString struct {
		S string
	}
)

func (s SVGString) Svg() string {
	return s.S
}

func (g *SVGGroup) Svg() string {
	str := &strings.Builder{}
	fmt.Fprintf(str, "<g %s>", g.attributed.Svg())
	for _, v := range g.Items {
		str.WriteString(v.Svg())
	}
	str.WriteString("</g>")
	return str.String()
}

func (doc *SVGDoc) Svg() string {
	str := &strings.Builder{}
	fmt.Fprintf(str, `<svg version="1.1" xmlns="%s" %s>`, NamespaceSVG, doc.attributed.Svg())
	if len(doc.Defs) > 0 {
		str.WriteString("<defs>")
		for _, v := range doc.Defs {
			str.WriteString(v.Svg())
		}
		str.WriteString("</defs>")
	}
	for _, v := range doc.Items {
		str.WriteString(v.Svg())
	}
	str.WriteString("</svg>")
	return str.String()
}

func (coll SVGColl) Svg() string {
	str := &strings.Builder{}
	for _, v := range coll {
		str.WriteString(v.Svg())
	}
	return str.String()
}

func (t *SVGText) Svg() string {
	return fmt.Sprintf(
		`<text x="%.4f" y="%.4f" %s>%s</text>`,
		t.Pos[0], t.Pos[1], t.attributed.Svg(), t.Body,
	)
}

func (g *SVGGroup) Add(s ...SVGConvertible) *SVGGroup {
	g.Items = append(g.Items, s...)
	return g
}

func (g *SVGGroup) AddAll(src SVGColl) *SVGGroup {
	g.Items = append(g.Items, src)
	return g
}

func FormatSVGPoints(str *strings.Builder, pts []*vec.Vec2) {
	for _, p := range pts {
		fmt.Fprintf(str, "%.4f,%.4f ", p[0], p[1])
	}
}

func FormatSVGPointList(str *strings.Builder, pts *dcons.DCons) {
	i := pts.Head
	for n := pts.Len(); n > 0; n-- {
		p := i.Value.(*vec.Vec2)
		fmt.Fprintf(str, "%.4f,%.4f ", p[0], p[1])
		i = i.Next
	}
}
