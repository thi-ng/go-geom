package math32i

import (
	"math/rand"

	"go.thi.ng/geom/math32"
)

type (
	ArrayI8  []int8
	ArrayU8  []uint8
	ArrayI16 []int16
	ArrayU16 []uint16
	ArrayI32 []int32
	ArrayU32 []uint32
)

func Abs(x int32) int32 {
	if x < 0 {
		return -x
	}
	return x
}

func AbsDiff(a, b int32) int32 {
	if a < b {
		return b - a
	}
	return a - b
}

func Sign(x int32) int32 {
	if x < 0 {
		return -1
	}
	if x > 0 {
		return 1
	}
	return 0
}

func Min(a, b int32) int32 {
	if a < b {
		return a
	}
	return b
}

func Min3(a, b, c int32) int32 {
	return Min(Min(a, b), c)
}

func Max(a, b int32) int32 {
	if a > b {
		return a
	}
	return b
}

func Max3(a, b, c int32) int32 {
	return Max(Max(a, b), c)
}

func Clamp(x, a, b int32) int32 {
	if x < a {
		return a
	}
	if x > b {
		return b
	}
	return x
}

func EqDelta(x, y, eps int32) bool {
	return AbsDiff(x, y) <= eps
}

func InRange(x, min, max int32) bool {
	return x >= min && x <= max
}

func RandNorm(n int32) int32 {
	return rand.Int31n(2*n) - n
}

func RandMinMax(min, max int32) int32 {
	return min + int32(float64(max-min)*rand.Float64())
}

func Atan2Abs(y, x int32) float32 {
	return math32.Atan2Abs(float32(y), float32(x))
}
