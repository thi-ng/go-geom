package geom

import (
	"fmt"
	"strings"

	"go.thi.ng/dcons"
	"go.thi.ng/geom/vec"
)

type Polygon2 struct {
	Points *dcons.DCons
	attributed
}

func NewPolygon() *Polygon2 {
	return &Polygon2{Points: dcons.New()}
}

func NewPolygonWithVertices(pts []*vec.Vec2) *Polygon2 {
	n := len(pts)
	verts := dcons.New()
	p := &Polygon2{Points: verts}
	for i := 0; i < n; i++ {
		verts.Push(pts[i])
	}
	verts.Circular()
	return p
}

// NewStarPolygon creates a new star shaped polygon with `n` vertices
// and given inner / outer radii, centered around the world origin. All
// even numbered vertices will be positioned at inner radius `r1`, odd
// ones at outer radius `r2`.
func NewStarPolygon(n int, r1, r2 float64) *Polygon2 {
	scale := r1 / r2
	verts := NewCircleWithRadius(r2).Vertices(n)
	for i := 0; i < n; i += 2 {
		verts[i].MulN(scale)
	}
	return NewPolygonWithVertices(verts)
}

func (p *Polygon2) Copy() *Polygon2 {
	return &Polygon2{
		Points:     CopyVertexList2(p.Points, true),
		attributed: p.attributed.Copy()}
}

func (p *Polygon2) Area() float64 {
	area := 0.0
	i := p.Points.Head
	for n := p.Points.Len(); n > 0; n-- {
		j := i.Next
		area += i.Value.(*vec.Vec2).Cross(j.Value.(*vec.Vec2))
		i = j
	}
	return area / 2
}

func (p *Polygon2) Center(c *vec.Vec2) *Polygon2 {
	o := c.Copy().Sub(p.Centroid())
	i := p.Points.Head
	for n := p.Points.Len(); n > 0; n-- {
		i.Value.(*vec.Vec2).Add(o)
		i = i.Next
	}
	return p
}

func (p *Polygon2) Centroid() *vec.Vec2 {
	centroid := &vec.Vec2{0, 0}
	area := 0.0
	i := p.Points.Head
	for n := p.Points.Len(); n > 0; n-- {
		j := i.Next
		p := i.Value.(*vec.Vec2)
		q := j.Value.(*vec.Vec2)
		a := p.Cross(q)
		area += a
		centroid[0] += (p[0] + q[0]) * a
		centroid[1] += (p[1] + q[1]) * a
		i = j
	}
	return centroid.DivN(3 * area)
}

func (p *Polygon2) Circumference() float64 {
	return ArcLengthVertexList2(p.Points, true)
}

func (p *Polygon2) BoundingCircle() *Circle2 {
	return NewBoundingCircleFromVertexList(p.Centroid(), p.Points)
}

func (p *Polygon2) BoundingRect() *Rect2 {
	return NewBoundingRectFromVertexList(p.Points)
}

func (p *Polygon2) Width() float64 {
	return p.BoundingRect().Size[0]
}

func (p *Polygon2) Height() float64 {
	return p.BoundingRect().Size[1]
}

func (p *Polygon2) Depth() float64 {
	return 0
}

func (p *Polygon2) Vertices() []*vec.Vec2 {
	n := p.Points.Len()
	verts := make([]*vec.Vec2, n)
	i := p.Points.Tail
	for n--; n >= 0; n-- {
		verts[n] = i.Value.(*vec.Vec2)
		i = i.Prev
	}
	return verts
}

func (p *Polygon2) Edges() []*Line2 {
	n := p.Points.Len()
	edges := make([]*Line2, n)
	b := p.Points.Head
	for n--; n >= 0; n-- {
		a := b.Prev
		edges[n] = NewLine2(a.Value.(*vec.Vec2), b.Value.(*vec.Vec2))
		b = a
	}
	return edges
}

func (p *Polygon2) ClassifyPoint(q *vec.Vec2) int {
	// TODO on boundary
	px, py := q.Elem()
	inside := false
	i := p.Points.Head
	for n := p.Points.Len(); n > 0; n-- {
		j := i.Next
		ax, ay := i.Value.(*vec.Vec2).Elem()
		bx, by := j.Value.(*vec.Vec2).Elem()
		if ((by < py && ay >= py) ||
			(ay < py && by >= py)) &&
			((py-by)/(ay-by)*(ax-bx)+bx) < px {
			inside = !inside
		}
		i = j
	}
	if inside {
		return 1
	}
	return -1
}

func (p *Polygon2) String() string {
	return fmt.Sprintf(
		"Polygon2 {n: %d, points: %v, attribs: %v}",
		p.Points.Len(), p.Points, p.attribs)
}

func (p *Polygon2) Svg() string {
	str := &strings.Builder{}
	str.WriteString(`<polygon points="`)
	FormatSVGPointList(str, p.Points)
	fmt.Fprintf(str, `" fill="none" %s/>`, p.attributed.Svg())
	return str.String()
}

func (p *Polygon2) Subdivide(n int, t float64) *Polygon2 {
	var dest *dcons.DCons
	for src := p.Points; n > 0; n-- {
		dest = dcons.New()
		src.Do(func(v *dcons.Cell) bool {
			a := v.Value.(*vec.Vec2)
			b := v.Next.Value.(*vec.Vec2)
			dest.Push(a.Copy().Mix(b, t))
			dest.Push(b.Copy().Mix(a, t))
			return true
		})
		dest.Circular()
		src = dest
	}
	return &Polygon2{
		Points:     dest,
		attributed: p.attributed.Copy(),
	}
}
