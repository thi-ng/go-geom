package geom

import (
	"fmt"
	"strings"

	"go.thi.ng/geom/vec"
)

type BezierPatch2 struct {
	Cpoints [16]vec.Vec2
	attributed
}

var edgeIDs = [...][2]int{
	{0, 1}, {1, 2}, {2, 3}, {0, 4}, {1, 5}, {2, 6},
	{3, 7}, {4, 5}, {5, 6}, {6, 7}, {4, 8}, {5, 9},
	{6, 10}, {7, 11}, {8, 9}, {9, 10}, {10, 11}, {8, 12},
	{9, 13}, {10, 14}, {11, 15}, {12, 13}, {13, 14}, {14, 15},
}

func NewBezierPatch2FromQuad(quad []*vec.Vec2) *BezierPatch2 {
	patch := BezierPatch2{}
	a, b, c, d := quad[0], quad[1], quad[2], quad[3]
	for i, u := 0, 0; u < 4; u++ {
		nu := float64(u) / 3
		for v := 0; v < 4; v++ {
			patch.Cpoints[i] = *a.Copy().MixBilinear(b, d, c, nu, float64(v)/3)
			i++
		}
	}
	return &patch
}

func NewBezierPatch2FromHex(hex []*vec.Vec2) *BezierPatch2 {
	return &BezierPatch2{
		Cpoints: [16]vec.Vec2{
			*hex[4], *hex[4], *hex[5], *hex[5],
			*hex[3], *hex[3], *hex[0], *hex[0],
			*hex[3], *hex[3], *hex[0], *hex[0],
			*hex[2], *hex[2], *hex[1], *hex[1],
		}}
}

func (b *BezierPatch2) Edges() []*Line2 {
	n := len(edgeIDs)
	edges := make([]*Line2, n)
	for i, e := range edgeIDs {
		edges[i] = NewLine2(&b.Cpoints[e[0]], &b.Cpoints[e[1]])
	}
	return edges
}

func (b *BezierPatch2) UnmapPoint(p *vec.Vec2) *vec.Vec2 {
	u := p[0]
	v := p[1]
	u2 := u * u
	v2 := v * v
	u1 := 1 - u
	v1 := 1 - v
	u12 := u1 * u1
	v12 := v1 * v1
	ua := u12 * u1
	ub := u12 * u * 3
	uc := u1 * u2 * 3
	ud := u * u2
	va := v12 * v1
	vb := v12 * v * 3
	vc := v1 * v2 * 3
	vd := v2 * v
	cp := b.Cpoints
	return &vec.Vec2{
		ua*(cp[0][0]*va+cp[4][0]*vb+cp[8][0]*vc+cp[12][0]*vd) +
			ub*(cp[1][0]*va+cp[5][0]*vb+cp[9][0]*vc+cp[13][0]*vd) +
			uc*(cp[2][0]*va+cp[6][0]*vb+cp[10][0]*vc+cp[14][0]*vd) +
			ud*(cp[3][0]*va+cp[7][0]*vb+cp[11][0]*vc+cp[15][0]*vd),
		ua*(cp[0][1]*va+cp[4][1]*vb+cp[8][1]*vc+cp[12][1]*vd) +
			ub*(cp[1][1]*va+cp[5][1]*vb+cp[9][1]*vc+cp[13][1]*vd) +
			uc*(cp[2][1]*va+cp[6][1]*vb+cp[10][1]*vc+cp[14][1]*vd) +
			ud*(cp[3][1]*va+cp[7][1]*vb+cp[11][1]*vc+cp[15][1]*vd),
	}
}

func (b *BezierPatch2) WarpPoints(src []*vec.Vec2, bounds *Rect2) []*vec.Vec2 {
	n := len(src)
	warped := make([]*vec.Vec2, n)
	if bounds == nil {
		bounds = NewBoundingRectFromVertices(src)
	}
	for i, p := range src {
		warped[i] = b.UnmapPoint(bounds.MapPoint(p))
	}
	return warped
}

func (b *BezierPatch2) Svg(res int) string {
	str := &strings.Builder{}
	fmt.Fprintf(str, "<g %s>", b.attributed.Svg())
	delta := 1.0 / float64(res)
	p := &vec.Vec2{}
	for u := 0; u <= res; u++ {
		col := &strings.Builder{}
		row := &strings.Builder{}
		col.WriteString(`<polyline points="`)
		row.WriteString(`<polyline points="`)
		p[0] = float64(u) * delta
		for v := 0; v <= res; v++ {
			p[1] = float64(v) * delta
			q := b.UnmapPoint(p)
			fmt.Fprintf(col, "%.4f,%.4f ", q[0], q[1])
			q = b.UnmapPoint(p.YX())
			fmt.Fprintf(row, "%.4f,%.4f ", q[0], q[1])
		}
		col.WriteString(`"/>`)
		row.WriteString(`"/>`)
		str.WriteString(col.String())
		str.WriteString(row.String())
	}
	str.WriteString("</g>")
	return str.String()
}
