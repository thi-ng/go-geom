package geom

import (
	"fmt"
	"strings"
)

type Attribs map[string]interface{}

type attributed struct {
	attribs Attribs
}

type Attributed interface {
	GetAttribs() Attribs
	GetAttrib(id string) interface{}
	SetAttribs(attribs Attribs)
	SetAttrib(id string, val interface{})
	DeleteAttrib(id string)
}

func (a *attributed) Copy() attributed {
	if a == nil {
		return attributed{}
	}
	a2 := make(map[string]interface{})
	for k, v := range a.attribs {
		a2[k] = v
	}
	return attributed{attribs: a2}
}

func (c *attributed) GetAttribs() Attribs {
	return c.attribs
}

func (c *attributed) SetAttribs(attr Attribs) {
	c.attribs = attr
}

func (c *attributed) GetAttrib(id string) interface{} {
	return c.attribs[id]
}

func (c *attributed) SetAttrib(id string, val interface{}) {
	if c.attribs == nil {
		c.attribs = make(map[string]interface{})
	}
	c.attribs[id] = val
}

func (c *attributed) DeleteAttrib(id string) {
	delete(c.attribs, id)
}

func (c *attributed) Svg() string {
	svg := &strings.Builder{}
	for k, v := range c.attribs {
		if _, hasSvg := v.(SVGConvertible); hasSvg {
			v = v.(SVGConvertible).Svg()
		}
		fmt.Fprintf(svg, `%v="%v" `, k, v)
	}
	return svg.String()
}
