package geom

import (
	"fmt"
	"strings"

	"go.thi.ng/geom/vec"
)

type Quadratic2 struct {
	Points []*vec.Vec2
	attributed
}

type Cubic2 struct {
	Points []*vec.Vec2
	attributed
}

func (c *Quadratic2) Vertices(res int) []*vec.Vec2 {
	n := len(c.Points) - 3
	verts := make([]*vec.Vec2, res*n+1)
	dt := 1.0 / float64(res)
	for i, j := 0, 0; i <= n; i += 2 {
		a, b, c := c.Points[i], c.Points[i+1], c.Points[i+2]
		for t := 0; t < res; t++ {
			verts[j] = MixQuadratic2(a, b, c, float64(t)*dt)
			j++
		}
		if i+2 >= n {
			verts[j] = c.Copy()
		}
	}
	return verts
}

func (c *Cubic2) Vertices(res int) []*vec.Vec2 {
	n := len(c.Points) - 4
	verts := make([]*vec.Vec2, res*n+1)
	dt := 1.0 / float64(res)
	for i, j := 0, 0; i <= n; i += 3 {
		a, b, c, d := c.Points[i], c.Points[i+1], c.Points[i+2], c.Points[i+3]
		for t := 0; t < res; t++ {
			verts[j] = MixCubic2(a, b, c, d, float64(t)*dt)
			j++
		}
		if i+3 >= n {
			verts[j] = d.Copy()
		}
	}
	return verts
}

func (s *Quadratic2) Svg() string {
	return splineSvg(s.Points, s.attributed, "Q")
}

func (s *Cubic2) Svg() string {
	return splineSvg(s.Points, s.attributed, "C")
}

func splineSvg(verts []*vec.Vec2, attr attributed, segType string) string {
	p := verts[0]
	str := &strings.Builder{}
	fmt.Fprintf(str, `<path d="M%.4f,%.4f %s`, p[0], p[1], segType)
	FormatSVGPoints(str, verts[1:])
	fmt.Fprintf(str, `" %s/>`, attr.Svg())
	return str.String()
}

func MixQuadratic2(a, b, c *vec.Vec2, t float64) *vec.Vec2 {
	s := 1.0 - t
	return a.Copy().
		MulN(s*s).
		MaddN(b, 2*s*t).
		MaddN(c, t*t)
}

func MixCubic2(a, b, c, d *vec.Vec2, t float64) *vec.Vec2 {
	s := 1.0 - t
	s2 := s * s
	t2 := t * t
	return a.Copy().
		MulN(s2*s).
		MaddN(b, 3*s2*t).
		MaddN(c, 3*t2*s).
		MaddN(d, t2*t)
}
