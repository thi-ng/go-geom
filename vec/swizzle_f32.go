package vec

func (v *Vec2f) XX() *Vec2f   { return &Vec2f{v[0], v[0]} }
func (v *Vec2f) XXX() *Vec3f  { return &Vec3f{v[0], v[0], v[0]} }
func (v *Vec2f) XXXX() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[0]} }
func (v *Vec2f) XXXY() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[1]} }
func (v *Vec2f) XXY() *Vec3f  { return &Vec3f{v[0], v[0], v[1]} }
func (v *Vec2f) XXYX() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[0]} }
func (v *Vec2f) XXYY() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[1]} }
func (v *Vec2f) XY() *Vec2f   { return &Vec2f{v[0], v[1]} }
func (v *Vec2f) XYX() *Vec3f  { return &Vec3f{v[0], v[1], v[0]} }
func (v *Vec2f) XYXX() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[0]} }
func (v *Vec2f) XYXY() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[1]} }
func (v *Vec2f) XYY() *Vec3f  { return &Vec3f{v[0], v[1], v[1]} }
func (v *Vec2f) XYYX() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[0]} }
func (v *Vec2f) XYYY() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[1]} }
func (v *Vec2f) YX() *Vec2f   { return &Vec2f{v[1], v[0]} }
func (v *Vec2f) YXX() *Vec3f  { return &Vec3f{v[1], v[0], v[0]} }
func (v *Vec2f) YXXX() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[0]} }
func (v *Vec2f) YXXY() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[1]} }
func (v *Vec2f) YXY() *Vec3f  { return &Vec3f{v[1], v[0], v[1]} }
func (v *Vec2f) YXYX() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[0]} }
func (v *Vec2f) YXYY() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[1]} }
func (v *Vec2f) YY() *Vec2f   { return &Vec2f{v[1], v[1]} }
func (v *Vec2f) YYX() *Vec3f  { return &Vec3f{v[1], v[1], v[0]} }
func (v *Vec2f) YYXX() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[0]} }
func (v *Vec2f) YYXY() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[1]} }
func (v *Vec2f) YYY() *Vec3f  { return &Vec3f{v[1], v[1], v[1]} }
func (v *Vec2f) YYYX() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[0]} }
func (v *Vec2f) YYYY() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[1]} }

func (v *Vec3f) XX() *Vec2f   { return &Vec2f{v[0], v[0]} }
func (v *Vec3f) XXX() *Vec3f  { return &Vec3f{v[0], v[0], v[0]} }
func (v *Vec3f) XXXX() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[0]} }
func (v *Vec3f) XXXY() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[1]} }
func (v *Vec3f) XXXZ() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[2]} }
func (v *Vec3f) XXY() *Vec3f  { return &Vec3f{v[0], v[0], v[1]} }
func (v *Vec3f) XXYX() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[0]} }
func (v *Vec3f) XXYY() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[1]} }
func (v *Vec3f) XXYZ() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[2]} }
func (v *Vec3f) XXZ() *Vec3f  { return &Vec3f{v[0], v[0], v[2]} }
func (v *Vec3f) XXZX() *Vec4f { return &Vec4f{v[0], v[0], v[2], v[0]} }
func (v *Vec3f) XXZY() *Vec4f { return &Vec4f{v[0], v[0], v[2], v[1]} }
func (v *Vec3f) XXZZ() *Vec4f { return &Vec4f{v[0], v[0], v[2], v[2]} }
func (v *Vec3f) XY() *Vec2f   { return &Vec2f{v[0], v[1]} }
func (v *Vec3f) XYX() *Vec3f  { return &Vec3f{v[0], v[1], v[0]} }
func (v *Vec3f) XYXX() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[0]} }
func (v *Vec3f) XYXY() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[1]} }
func (v *Vec3f) XYXZ() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[2]} }
func (v *Vec3f) XYY() *Vec3f  { return &Vec3f{v[0], v[1], v[1]} }
func (v *Vec3f) XYYX() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[0]} }
func (v *Vec3f) XYYY() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[1]} }
func (v *Vec3f) XYYZ() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[2]} }
func (v *Vec3f) XYZ() *Vec3f  { return &Vec3f{v[0], v[1], v[2]} }
func (v *Vec3f) XYZX() *Vec4f { return &Vec4f{v[0], v[1], v[2], v[0]} }
func (v *Vec3f) XYZY() *Vec4f { return &Vec4f{v[0], v[1], v[2], v[1]} }
func (v *Vec3f) XYZZ() *Vec4f { return &Vec4f{v[0], v[1], v[2], v[2]} }
func (v *Vec3f) XZ() *Vec2f   { return &Vec2f{v[0], v[2]} }
func (v *Vec3f) XZX() *Vec3f  { return &Vec3f{v[0], v[2], v[0]} }
func (v *Vec3f) XZXX() *Vec4f { return &Vec4f{v[0], v[2], v[0], v[0]} }
func (v *Vec3f) XZXY() *Vec4f { return &Vec4f{v[0], v[2], v[0], v[1]} }
func (v *Vec3f) XZXZ() *Vec4f { return &Vec4f{v[0], v[2], v[0], v[2]} }
func (v *Vec3f) XZY() *Vec3f  { return &Vec3f{v[0], v[2], v[1]} }
func (v *Vec3f) XZYX() *Vec4f { return &Vec4f{v[0], v[2], v[1], v[0]} }
func (v *Vec3f) XZYY() *Vec4f { return &Vec4f{v[0], v[2], v[1], v[1]} }
func (v *Vec3f) XZYZ() *Vec4f { return &Vec4f{v[0], v[2], v[1], v[2]} }
func (v *Vec3f) XZZ() *Vec3f  { return &Vec3f{v[0], v[2], v[2]} }
func (v *Vec3f) XZZX() *Vec4f { return &Vec4f{v[0], v[2], v[2], v[0]} }
func (v *Vec3f) XZZY() *Vec4f { return &Vec4f{v[0], v[2], v[2], v[1]} }
func (v *Vec3f) XZZZ() *Vec4f { return &Vec4f{v[0], v[2], v[2], v[2]} }
func (v *Vec3f) YX() *Vec2f   { return &Vec2f{v[1], v[0]} }
func (v *Vec3f) YXX() *Vec3f  { return &Vec3f{v[1], v[0], v[0]} }
func (v *Vec3f) YXXX() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[0]} }
func (v *Vec3f) YXXY() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[1]} }
func (v *Vec3f) YXXZ() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[2]} }
func (v *Vec3f) YXY() *Vec3f  { return &Vec3f{v[1], v[0], v[1]} }
func (v *Vec3f) YXYX() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[0]} }
func (v *Vec3f) YXYY() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[1]} }
func (v *Vec3f) YXYZ() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[2]} }
func (v *Vec3f) YXZ() *Vec3f  { return &Vec3f{v[1], v[0], v[2]} }
func (v *Vec3f) YXZX() *Vec4f { return &Vec4f{v[1], v[0], v[2], v[0]} }
func (v *Vec3f) YXZY() *Vec4f { return &Vec4f{v[1], v[0], v[2], v[1]} }
func (v *Vec3f) YXZZ() *Vec4f { return &Vec4f{v[1], v[0], v[2], v[2]} }
func (v *Vec3f) YY() *Vec2f   { return &Vec2f{v[1], v[1]} }
func (v *Vec3f) YYX() *Vec3f  { return &Vec3f{v[1], v[1], v[0]} }
func (v *Vec3f) YYXX() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[0]} }
func (v *Vec3f) YYXY() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[1]} }
func (v *Vec3f) YYXZ() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[2]} }
func (v *Vec3f) YYY() *Vec3f  { return &Vec3f{v[1], v[1], v[1]} }
func (v *Vec3f) YYYX() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[0]} }
func (v *Vec3f) YYYY() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[1]} }
func (v *Vec3f) YYYZ() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[2]} }
func (v *Vec3f) YYZ() *Vec3f  { return &Vec3f{v[1], v[1], v[2]} }
func (v *Vec3f) YYZX() *Vec4f { return &Vec4f{v[1], v[1], v[2], v[0]} }
func (v *Vec3f) YYZY() *Vec4f { return &Vec4f{v[1], v[1], v[2], v[1]} }
func (v *Vec3f) YYZZ() *Vec4f { return &Vec4f{v[1], v[1], v[2], v[2]} }
func (v *Vec3f) YZ() *Vec2f   { return &Vec2f{v[1], v[2]} }
func (v *Vec3f) YZX() *Vec3f  { return &Vec3f{v[1], v[2], v[0]} }
func (v *Vec3f) YZXX() *Vec4f { return &Vec4f{v[1], v[2], v[0], v[0]} }
func (v *Vec3f) YZXY() *Vec4f { return &Vec4f{v[1], v[2], v[0], v[1]} }
func (v *Vec3f) YZXZ() *Vec4f { return &Vec4f{v[1], v[2], v[0], v[2]} }
func (v *Vec3f) YZY() *Vec3f  { return &Vec3f{v[1], v[2], v[1]} }
func (v *Vec3f) YZYX() *Vec4f { return &Vec4f{v[1], v[2], v[1], v[0]} }
func (v *Vec3f) YZYY() *Vec4f { return &Vec4f{v[1], v[2], v[1], v[1]} }
func (v *Vec3f) YZYZ() *Vec4f { return &Vec4f{v[1], v[2], v[1], v[2]} }
func (v *Vec3f) YZZ() *Vec3f  { return &Vec3f{v[1], v[2], v[2]} }
func (v *Vec3f) YZZX() *Vec4f { return &Vec4f{v[1], v[2], v[2], v[0]} }
func (v *Vec3f) YZZY() *Vec4f { return &Vec4f{v[1], v[2], v[2], v[1]} }
func (v *Vec3f) YZZZ() *Vec4f { return &Vec4f{v[1], v[2], v[2], v[2]} }
func (v *Vec3f) ZX() *Vec2f   { return &Vec2f{v[2], v[0]} }
func (v *Vec3f) ZXX() *Vec3f  { return &Vec3f{v[2], v[0], v[0]} }
func (v *Vec3f) ZXXX() *Vec4f { return &Vec4f{v[2], v[0], v[0], v[0]} }
func (v *Vec3f) ZXXY() *Vec4f { return &Vec4f{v[2], v[0], v[0], v[1]} }
func (v *Vec3f) ZXXZ() *Vec4f { return &Vec4f{v[2], v[0], v[0], v[2]} }
func (v *Vec3f) ZXY() *Vec3f  { return &Vec3f{v[2], v[0], v[1]} }
func (v *Vec3f) ZXYX() *Vec4f { return &Vec4f{v[2], v[0], v[1], v[0]} }
func (v *Vec3f) ZXYY() *Vec4f { return &Vec4f{v[2], v[0], v[1], v[1]} }
func (v *Vec3f) ZXYZ() *Vec4f { return &Vec4f{v[2], v[0], v[1], v[2]} }
func (v *Vec3f) ZXZ() *Vec3f  { return &Vec3f{v[2], v[0], v[2]} }
func (v *Vec3f) ZXZX() *Vec4f { return &Vec4f{v[2], v[0], v[2], v[0]} }
func (v *Vec3f) ZXZY() *Vec4f { return &Vec4f{v[2], v[0], v[2], v[1]} }
func (v *Vec3f) ZXZZ() *Vec4f { return &Vec4f{v[2], v[0], v[2], v[2]} }
func (v *Vec3f) ZY() *Vec2f   { return &Vec2f{v[2], v[1]} }
func (v *Vec3f) ZYX() *Vec3f  { return &Vec3f{v[2], v[1], v[0]} }
func (v *Vec3f) ZYXX() *Vec4f { return &Vec4f{v[2], v[1], v[0], v[0]} }
func (v *Vec3f) ZYXY() *Vec4f { return &Vec4f{v[2], v[1], v[0], v[1]} }
func (v *Vec3f) ZYXZ() *Vec4f { return &Vec4f{v[2], v[1], v[0], v[2]} }
func (v *Vec3f) ZYY() *Vec3f  { return &Vec3f{v[2], v[1], v[1]} }
func (v *Vec3f) ZYYX() *Vec4f { return &Vec4f{v[2], v[1], v[1], v[0]} }
func (v *Vec3f) ZYYY() *Vec4f { return &Vec4f{v[2], v[1], v[1], v[1]} }
func (v *Vec3f) ZYYZ() *Vec4f { return &Vec4f{v[2], v[1], v[1], v[2]} }
func (v *Vec3f) ZYZ() *Vec3f  { return &Vec3f{v[2], v[1], v[2]} }
func (v *Vec3f) ZYZX() *Vec4f { return &Vec4f{v[2], v[1], v[2], v[0]} }
func (v *Vec3f) ZYZY() *Vec4f { return &Vec4f{v[2], v[1], v[2], v[1]} }
func (v *Vec3f) ZYZZ() *Vec4f { return &Vec4f{v[2], v[1], v[2], v[2]} }
func (v *Vec3f) ZZ() *Vec2f   { return &Vec2f{v[2], v[2]} }
func (v *Vec3f) ZZX() *Vec3f  { return &Vec3f{v[2], v[2], v[0]} }
func (v *Vec3f) ZZXX() *Vec4f { return &Vec4f{v[2], v[2], v[0], v[0]} }
func (v *Vec3f) ZZXY() *Vec4f { return &Vec4f{v[2], v[2], v[0], v[1]} }
func (v *Vec3f) ZZXZ() *Vec4f { return &Vec4f{v[2], v[2], v[0], v[2]} }
func (v *Vec3f) ZZY() *Vec3f  { return &Vec3f{v[2], v[2], v[1]} }
func (v *Vec3f) ZZYX() *Vec4f { return &Vec4f{v[2], v[2], v[1], v[0]} }
func (v *Vec3f) ZZYY() *Vec4f { return &Vec4f{v[2], v[2], v[1], v[1]} }
func (v *Vec3f) ZZYZ() *Vec4f { return &Vec4f{v[2], v[2], v[1], v[2]} }
func (v *Vec3f) ZZZ() *Vec3f  { return &Vec3f{v[2], v[2], v[2]} }
func (v *Vec3f) ZZZX() *Vec4f { return &Vec4f{v[2], v[2], v[2], v[0]} }
func (v *Vec3f) ZZZY() *Vec4f { return &Vec4f{v[2], v[2], v[2], v[1]} }
func (v *Vec3f) ZZZZ() *Vec4f { return &Vec4f{v[2], v[2], v[2], v[2]} }

func (v *Vec4f) WW() *Vec2f   { return &Vec2f{v[3], v[3]} }
func (v *Vec4f) WWW() *Vec3f  { return &Vec3f{v[3], v[3], v[3]} }
func (v *Vec4f) WWWW() *Vec4f { return &Vec4f{v[3], v[3], v[3], v[3]} }
func (v *Vec4f) WWWX() *Vec4f { return &Vec4f{v[3], v[3], v[3], v[0]} }
func (v *Vec4f) WWWY() *Vec4f { return &Vec4f{v[3], v[3], v[3], v[1]} }
func (v *Vec4f) WWWZ() *Vec4f { return &Vec4f{v[3], v[3], v[3], v[2]} }
func (v *Vec4f) WWX() *Vec3f  { return &Vec3f{v[3], v[3], v[0]} }
func (v *Vec4f) WWXW() *Vec4f { return &Vec4f{v[3], v[3], v[0], v[3]} }
func (v *Vec4f) WWXX() *Vec4f { return &Vec4f{v[3], v[3], v[0], v[0]} }
func (v *Vec4f) WWXY() *Vec4f { return &Vec4f{v[3], v[3], v[0], v[1]} }
func (v *Vec4f) WWXZ() *Vec4f { return &Vec4f{v[3], v[3], v[0], v[2]} }
func (v *Vec4f) WWY() *Vec3f  { return &Vec3f{v[3], v[3], v[1]} }
func (v *Vec4f) WWYW() *Vec4f { return &Vec4f{v[3], v[3], v[1], v[3]} }
func (v *Vec4f) WWYX() *Vec4f { return &Vec4f{v[3], v[3], v[1], v[0]} }
func (v *Vec4f) WWYY() *Vec4f { return &Vec4f{v[3], v[3], v[1], v[1]} }
func (v *Vec4f) WWYZ() *Vec4f { return &Vec4f{v[3], v[3], v[1], v[2]} }
func (v *Vec4f) WWZ() *Vec3f  { return &Vec3f{v[3], v[3], v[2]} }
func (v *Vec4f) WWZW() *Vec4f { return &Vec4f{v[3], v[3], v[2], v[3]} }
func (v *Vec4f) WWZX() *Vec4f { return &Vec4f{v[3], v[3], v[2], v[0]} }
func (v *Vec4f) WWZY() *Vec4f { return &Vec4f{v[3], v[3], v[2], v[1]} }
func (v *Vec4f) WWZZ() *Vec4f { return &Vec4f{v[3], v[3], v[2], v[2]} }
func (v *Vec4f) WX() *Vec2f   { return &Vec2f{v[3], v[0]} }
func (v *Vec4f) WXW() *Vec3f  { return &Vec3f{v[3], v[0], v[3]} }
func (v *Vec4f) WXWW() *Vec4f { return &Vec4f{v[3], v[0], v[3], v[3]} }
func (v *Vec4f) WXWX() *Vec4f { return &Vec4f{v[3], v[0], v[3], v[0]} }
func (v *Vec4f) WXWY() *Vec4f { return &Vec4f{v[3], v[0], v[3], v[1]} }
func (v *Vec4f) WXWZ() *Vec4f { return &Vec4f{v[3], v[0], v[3], v[2]} }
func (v *Vec4f) WXX() *Vec3f  { return &Vec3f{v[3], v[0], v[0]} }
func (v *Vec4f) WXXW() *Vec4f { return &Vec4f{v[3], v[0], v[0], v[3]} }
func (v *Vec4f) WXXX() *Vec4f { return &Vec4f{v[3], v[0], v[0], v[0]} }
func (v *Vec4f) WXXY() *Vec4f { return &Vec4f{v[3], v[0], v[0], v[1]} }
func (v *Vec4f) WXXZ() *Vec4f { return &Vec4f{v[3], v[0], v[0], v[2]} }
func (v *Vec4f) WXY() *Vec3f  { return &Vec3f{v[3], v[0], v[1]} }
func (v *Vec4f) WXYW() *Vec4f { return &Vec4f{v[3], v[0], v[1], v[3]} }
func (v *Vec4f) WXYX() *Vec4f { return &Vec4f{v[3], v[0], v[1], v[0]} }
func (v *Vec4f) WXYY() *Vec4f { return &Vec4f{v[3], v[0], v[1], v[1]} }
func (v *Vec4f) WXYZ() *Vec4f { return &Vec4f{v[3], v[0], v[1], v[2]} }
func (v *Vec4f) WXZ() *Vec3f  { return &Vec3f{v[3], v[0], v[2]} }
func (v *Vec4f) WXZW() *Vec4f { return &Vec4f{v[3], v[0], v[2], v[3]} }
func (v *Vec4f) WXZX() *Vec4f { return &Vec4f{v[3], v[0], v[2], v[0]} }
func (v *Vec4f) WXZY() *Vec4f { return &Vec4f{v[3], v[0], v[2], v[1]} }
func (v *Vec4f) WXZZ() *Vec4f { return &Vec4f{v[3], v[0], v[2], v[2]} }
func (v *Vec4f) WY() *Vec2f   { return &Vec2f{v[3], v[1]} }
func (v *Vec4f) WYW() *Vec3f  { return &Vec3f{v[3], v[1], v[3]} }
func (v *Vec4f) WYWW() *Vec4f { return &Vec4f{v[3], v[1], v[3], v[3]} }
func (v *Vec4f) WYWX() *Vec4f { return &Vec4f{v[3], v[1], v[3], v[0]} }
func (v *Vec4f) WYWY() *Vec4f { return &Vec4f{v[3], v[1], v[3], v[1]} }
func (v *Vec4f) WYWZ() *Vec4f { return &Vec4f{v[3], v[1], v[3], v[2]} }
func (v *Vec4f) WYX() *Vec3f  { return &Vec3f{v[3], v[1], v[0]} }
func (v *Vec4f) WYXW() *Vec4f { return &Vec4f{v[3], v[1], v[0], v[3]} }
func (v *Vec4f) WYXX() *Vec4f { return &Vec4f{v[3], v[1], v[0], v[0]} }
func (v *Vec4f) WYXY() *Vec4f { return &Vec4f{v[3], v[1], v[0], v[1]} }
func (v *Vec4f) WYXZ() *Vec4f { return &Vec4f{v[3], v[1], v[0], v[2]} }
func (v *Vec4f) WYY() *Vec3f  { return &Vec3f{v[3], v[1], v[1]} }
func (v *Vec4f) WYYW() *Vec4f { return &Vec4f{v[3], v[1], v[1], v[3]} }
func (v *Vec4f) WYYX() *Vec4f { return &Vec4f{v[3], v[1], v[1], v[0]} }
func (v *Vec4f) WYYY() *Vec4f { return &Vec4f{v[3], v[1], v[1], v[1]} }
func (v *Vec4f) WYYZ() *Vec4f { return &Vec4f{v[3], v[1], v[1], v[2]} }
func (v *Vec4f) WYZ() *Vec3f  { return &Vec3f{v[3], v[1], v[2]} }
func (v *Vec4f) WYZW() *Vec4f { return &Vec4f{v[3], v[1], v[2], v[3]} }
func (v *Vec4f) WYZX() *Vec4f { return &Vec4f{v[3], v[1], v[2], v[0]} }
func (v *Vec4f) WYZY() *Vec4f { return &Vec4f{v[3], v[1], v[2], v[1]} }
func (v *Vec4f) WYZZ() *Vec4f { return &Vec4f{v[3], v[1], v[2], v[2]} }
func (v *Vec4f) WZ() *Vec2f   { return &Vec2f{v[3], v[2]} }
func (v *Vec4f) WZW() *Vec3f  { return &Vec3f{v[3], v[2], v[3]} }
func (v *Vec4f) WZWW() *Vec4f { return &Vec4f{v[3], v[2], v[3], v[3]} }
func (v *Vec4f) WZWX() *Vec4f { return &Vec4f{v[3], v[2], v[3], v[0]} }
func (v *Vec4f) WZWY() *Vec4f { return &Vec4f{v[3], v[2], v[3], v[1]} }
func (v *Vec4f) WZWZ() *Vec4f { return &Vec4f{v[3], v[2], v[3], v[2]} }
func (v *Vec4f) WZX() *Vec3f  { return &Vec3f{v[3], v[2], v[0]} }
func (v *Vec4f) WZXW() *Vec4f { return &Vec4f{v[3], v[2], v[0], v[3]} }
func (v *Vec4f) WZXX() *Vec4f { return &Vec4f{v[3], v[2], v[0], v[0]} }
func (v *Vec4f) WZXY() *Vec4f { return &Vec4f{v[3], v[2], v[0], v[1]} }
func (v *Vec4f) WZXZ() *Vec4f { return &Vec4f{v[3], v[2], v[0], v[2]} }
func (v *Vec4f) WZY() *Vec3f  { return &Vec3f{v[3], v[2], v[1]} }
func (v *Vec4f) WZYW() *Vec4f { return &Vec4f{v[3], v[2], v[1], v[3]} }
func (v *Vec4f) WZYX() *Vec4f { return &Vec4f{v[3], v[2], v[1], v[0]} }
func (v *Vec4f) WZYY() *Vec4f { return &Vec4f{v[3], v[2], v[1], v[1]} }
func (v *Vec4f) WZYZ() *Vec4f { return &Vec4f{v[3], v[2], v[1], v[2]} }
func (v *Vec4f) WZZ() *Vec3f  { return &Vec3f{v[3], v[2], v[2]} }
func (v *Vec4f) WZZW() *Vec4f { return &Vec4f{v[3], v[2], v[2], v[3]} }
func (v *Vec4f) WZZX() *Vec4f { return &Vec4f{v[3], v[2], v[2], v[0]} }
func (v *Vec4f) WZZY() *Vec4f { return &Vec4f{v[3], v[2], v[2], v[1]} }
func (v *Vec4f) WZZZ() *Vec4f { return &Vec4f{v[3], v[2], v[2], v[2]} }
func (v *Vec4f) XW() *Vec2f   { return &Vec2f{v[0], v[3]} }
func (v *Vec4f) XWW() *Vec3f  { return &Vec3f{v[0], v[3], v[3]} }
func (v *Vec4f) XWWW() *Vec4f { return &Vec4f{v[0], v[3], v[3], v[3]} }
func (v *Vec4f) XWWX() *Vec4f { return &Vec4f{v[0], v[3], v[3], v[0]} }
func (v *Vec4f) XWWY() *Vec4f { return &Vec4f{v[0], v[3], v[3], v[1]} }
func (v *Vec4f) XWWZ() *Vec4f { return &Vec4f{v[0], v[3], v[3], v[2]} }
func (v *Vec4f) XWX() *Vec3f  { return &Vec3f{v[0], v[3], v[0]} }
func (v *Vec4f) XWXW() *Vec4f { return &Vec4f{v[0], v[3], v[0], v[3]} }
func (v *Vec4f) XWXX() *Vec4f { return &Vec4f{v[0], v[3], v[0], v[0]} }
func (v *Vec4f) XWXY() *Vec4f { return &Vec4f{v[0], v[3], v[0], v[1]} }
func (v *Vec4f) XWXZ() *Vec4f { return &Vec4f{v[0], v[3], v[0], v[2]} }
func (v *Vec4f) XWY() *Vec3f  { return &Vec3f{v[0], v[3], v[1]} }
func (v *Vec4f) XWYW() *Vec4f { return &Vec4f{v[0], v[3], v[1], v[3]} }
func (v *Vec4f) XWYX() *Vec4f { return &Vec4f{v[0], v[3], v[1], v[0]} }
func (v *Vec4f) XWYY() *Vec4f { return &Vec4f{v[0], v[3], v[1], v[1]} }
func (v *Vec4f) XWYZ() *Vec4f { return &Vec4f{v[0], v[3], v[1], v[2]} }
func (v *Vec4f) XWZ() *Vec3f  { return &Vec3f{v[0], v[3], v[2]} }
func (v *Vec4f) XWZW() *Vec4f { return &Vec4f{v[0], v[3], v[2], v[3]} }
func (v *Vec4f) XWZX() *Vec4f { return &Vec4f{v[0], v[3], v[2], v[0]} }
func (v *Vec4f) XWZY() *Vec4f { return &Vec4f{v[0], v[3], v[2], v[1]} }
func (v *Vec4f) XWZZ() *Vec4f { return &Vec4f{v[0], v[3], v[2], v[2]} }
func (v *Vec4f) XX() *Vec2f   { return &Vec2f{v[0], v[0]} }
func (v *Vec4f) XXW() *Vec3f  { return &Vec3f{v[0], v[0], v[3]} }
func (v *Vec4f) XXWW() *Vec4f { return &Vec4f{v[0], v[0], v[3], v[3]} }
func (v *Vec4f) XXWX() *Vec4f { return &Vec4f{v[0], v[0], v[3], v[0]} }
func (v *Vec4f) XXWY() *Vec4f { return &Vec4f{v[0], v[0], v[3], v[1]} }
func (v *Vec4f) XXWZ() *Vec4f { return &Vec4f{v[0], v[0], v[3], v[2]} }
func (v *Vec4f) XXX() *Vec3f  { return &Vec3f{v[0], v[0], v[0]} }
func (v *Vec4f) XXXW() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[3]} }
func (v *Vec4f) XXXX() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[0]} }
func (v *Vec4f) XXXY() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[1]} }
func (v *Vec4f) XXXZ() *Vec4f { return &Vec4f{v[0], v[0], v[0], v[2]} }
func (v *Vec4f) XXY() *Vec3f  { return &Vec3f{v[0], v[0], v[1]} }
func (v *Vec4f) XXYW() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[3]} }
func (v *Vec4f) XXYX() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[0]} }
func (v *Vec4f) XXYY() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[1]} }
func (v *Vec4f) XXYZ() *Vec4f { return &Vec4f{v[0], v[0], v[1], v[2]} }
func (v *Vec4f) XXZ() *Vec3f  { return &Vec3f{v[0], v[0], v[2]} }
func (v *Vec4f) XXZW() *Vec4f { return &Vec4f{v[0], v[0], v[2], v[3]} }
func (v *Vec4f) XXZX() *Vec4f { return &Vec4f{v[0], v[0], v[2], v[0]} }
func (v *Vec4f) XXZY() *Vec4f { return &Vec4f{v[0], v[0], v[2], v[1]} }
func (v *Vec4f) XXZZ() *Vec4f { return &Vec4f{v[0], v[0], v[2], v[2]} }
func (v *Vec4f) XY() *Vec2f   { return &Vec2f{v[0], v[1]} }
func (v *Vec4f) XYW() *Vec3f  { return &Vec3f{v[0], v[1], v[3]} }
func (v *Vec4f) XYWW() *Vec4f { return &Vec4f{v[0], v[1], v[3], v[3]} }
func (v *Vec4f) XYWX() *Vec4f { return &Vec4f{v[0], v[1], v[3], v[0]} }
func (v *Vec4f) XYWY() *Vec4f { return &Vec4f{v[0], v[1], v[3], v[1]} }
func (v *Vec4f) XYWZ() *Vec4f { return &Vec4f{v[0], v[1], v[3], v[2]} }
func (v *Vec4f) XYX() *Vec3f  { return &Vec3f{v[0], v[1], v[0]} }
func (v *Vec4f) XYXW() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[3]} }
func (v *Vec4f) XYXX() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[0]} }
func (v *Vec4f) XYXY() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[1]} }
func (v *Vec4f) XYXZ() *Vec4f { return &Vec4f{v[0], v[1], v[0], v[2]} }
func (v *Vec4f) XYY() *Vec3f  { return &Vec3f{v[0], v[1], v[1]} }
func (v *Vec4f) XYYW() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[3]} }
func (v *Vec4f) XYYX() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[0]} }
func (v *Vec4f) XYYY() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[1]} }
func (v *Vec4f) XYYZ() *Vec4f { return &Vec4f{v[0], v[1], v[1], v[2]} }
func (v *Vec4f) XYZ() *Vec3f  { return &Vec3f{v[0], v[1], v[2]} }
func (v *Vec4f) XYZW() *Vec4f { return &Vec4f{v[0], v[1], v[2], v[3]} }
func (v *Vec4f) XYZX() *Vec4f { return &Vec4f{v[0], v[1], v[2], v[0]} }
func (v *Vec4f) XYZY() *Vec4f { return &Vec4f{v[0], v[1], v[2], v[1]} }
func (v *Vec4f) XYZZ() *Vec4f { return &Vec4f{v[0], v[1], v[2], v[2]} }
func (v *Vec4f) XZ() *Vec2f   { return &Vec2f{v[0], v[2]} }
func (v *Vec4f) XZW() *Vec3f  { return &Vec3f{v[0], v[2], v[3]} }
func (v *Vec4f) XZWW() *Vec4f { return &Vec4f{v[0], v[2], v[3], v[3]} }
func (v *Vec4f) XZWX() *Vec4f { return &Vec4f{v[0], v[2], v[3], v[0]} }
func (v *Vec4f) XZWY() *Vec4f { return &Vec4f{v[0], v[2], v[3], v[1]} }
func (v *Vec4f) XZWZ() *Vec4f { return &Vec4f{v[0], v[2], v[3], v[2]} }
func (v *Vec4f) XZX() *Vec3f  { return &Vec3f{v[0], v[2], v[0]} }
func (v *Vec4f) XZXW() *Vec4f { return &Vec4f{v[0], v[2], v[0], v[3]} }
func (v *Vec4f) XZXX() *Vec4f { return &Vec4f{v[0], v[2], v[0], v[0]} }
func (v *Vec4f) XZXY() *Vec4f { return &Vec4f{v[0], v[2], v[0], v[1]} }
func (v *Vec4f) XZXZ() *Vec4f { return &Vec4f{v[0], v[2], v[0], v[2]} }
func (v *Vec4f) XZY() *Vec3f  { return &Vec3f{v[0], v[2], v[1]} }
func (v *Vec4f) XZYW() *Vec4f { return &Vec4f{v[0], v[2], v[1], v[3]} }
func (v *Vec4f) XZYX() *Vec4f { return &Vec4f{v[0], v[2], v[1], v[0]} }
func (v *Vec4f) XZYY() *Vec4f { return &Vec4f{v[0], v[2], v[1], v[1]} }
func (v *Vec4f) XZYZ() *Vec4f { return &Vec4f{v[0], v[2], v[1], v[2]} }
func (v *Vec4f) XZZ() *Vec3f  { return &Vec3f{v[0], v[2], v[2]} }
func (v *Vec4f) XZZW() *Vec4f { return &Vec4f{v[0], v[2], v[2], v[3]} }
func (v *Vec4f) XZZX() *Vec4f { return &Vec4f{v[0], v[2], v[2], v[0]} }
func (v *Vec4f) XZZY() *Vec4f { return &Vec4f{v[0], v[2], v[2], v[1]} }
func (v *Vec4f) XZZZ() *Vec4f { return &Vec4f{v[0], v[2], v[2], v[2]} }
func (v *Vec4f) YW() *Vec2f   { return &Vec2f{v[1], v[3]} }
func (v *Vec4f) YWW() *Vec3f  { return &Vec3f{v[1], v[3], v[3]} }
func (v *Vec4f) YWWW() *Vec4f { return &Vec4f{v[1], v[3], v[3], v[3]} }
func (v *Vec4f) YWWX() *Vec4f { return &Vec4f{v[1], v[3], v[3], v[0]} }
func (v *Vec4f) YWWY() *Vec4f { return &Vec4f{v[1], v[3], v[3], v[1]} }
func (v *Vec4f) YWWZ() *Vec4f { return &Vec4f{v[1], v[3], v[3], v[2]} }
func (v *Vec4f) YWX() *Vec3f  { return &Vec3f{v[1], v[3], v[0]} }
func (v *Vec4f) YWXW() *Vec4f { return &Vec4f{v[1], v[3], v[0], v[3]} }
func (v *Vec4f) YWXX() *Vec4f { return &Vec4f{v[1], v[3], v[0], v[0]} }
func (v *Vec4f) YWXY() *Vec4f { return &Vec4f{v[1], v[3], v[0], v[1]} }
func (v *Vec4f) YWXZ() *Vec4f { return &Vec4f{v[1], v[3], v[0], v[2]} }
func (v *Vec4f) YWY() *Vec3f  { return &Vec3f{v[1], v[3], v[1]} }
func (v *Vec4f) YWYW() *Vec4f { return &Vec4f{v[1], v[3], v[1], v[3]} }
func (v *Vec4f) YWYX() *Vec4f { return &Vec4f{v[1], v[3], v[1], v[0]} }
func (v *Vec4f) YWYY() *Vec4f { return &Vec4f{v[1], v[3], v[1], v[1]} }
func (v *Vec4f) YWYZ() *Vec4f { return &Vec4f{v[1], v[3], v[1], v[2]} }
func (v *Vec4f) YWZ() *Vec3f  { return &Vec3f{v[1], v[3], v[2]} }
func (v *Vec4f) YWZW() *Vec4f { return &Vec4f{v[1], v[3], v[2], v[3]} }
func (v *Vec4f) YWZX() *Vec4f { return &Vec4f{v[1], v[3], v[2], v[0]} }
func (v *Vec4f) YWZY() *Vec4f { return &Vec4f{v[1], v[3], v[2], v[1]} }
func (v *Vec4f) YWZZ() *Vec4f { return &Vec4f{v[1], v[3], v[2], v[2]} }
func (v *Vec4f) YX() *Vec2f   { return &Vec2f{v[1], v[0]} }
func (v *Vec4f) YXW() *Vec3f  { return &Vec3f{v[1], v[0], v[3]} }
func (v *Vec4f) YXWW() *Vec4f { return &Vec4f{v[1], v[0], v[3], v[3]} }
func (v *Vec4f) YXWX() *Vec4f { return &Vec4f{v[1], v[0], v[3], v[0]} }
func (v *Vec4f) YXWY() *Vec4f { return &Vec4f{v[1], v[0], v[3], v[1]} }
func (v *Vec4f) YXWZ() *Vec4f { return &Vec4f{v[1], v[0], v[3], v[2]} }
func (v *Vec4f) YXX() *Vec3f  { return &Vec3f{v[1], v[0], v[0]} }
func (v *Vec4f) YXXW() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[3]} }
func (v *Vec4f) YXXX() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[0]} }
func (v *Vec4f) YXXY() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[1]} }
func (v *Vec4f) YXXZ() *Vec4f { return &Vec4f{v[1], v[0], v[0], v[2]} }
func (v *Vec4f) YXY() *Vec3f  { return &Vec3f{v[1], v[0], v[1]} }
func (v *Vec4f) YXYW() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[3]} }
func (v *Vec4f) YXYX() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[0]} }
func (v *Vec4f) YXYY() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[1]} }
func (v *Vec4f) YXYZ() *Vec4f { return &Vec4f{v[1], v[0], v[1], v[2]} }
func (v *Vec4f) YXZ() *Vec3f  { return &Vec3f{v[1], v[0], v[2]} }
func (v *Vec4f) YXZW() *Vec4f { return &Vec4f{v[1], v[0], v[2], v[3]} }
func (v *Vec4f) YXZX() *Vec4f { return &Vec4f{v[1], v[0], v[2], v[0]} }
func (v *Vec4f) YXZY() *Vec4f { return &Vec4f{v[1], v[0], v[2], v[1]} }
func (v *Vec4f) YXZZ() *Vec4f { return &Vec4f{v[1], v[0], v[2], v[2]} }
func (v *Vec4f) YY() *Vec2f   { return &Vec2f{v[1], v[1]} }
func (v *Vec4f) YYW() *Vec3f  { return &Vec3f{v[1], v[1], v[3]} }
func (v *Vec4f) YYWW() *Vec4f { return &Vec4f{v[1], v[1], v[3], v[3]} }
func (v *Vec4f) YYWX() *Vec4f { return &Vec4f{v[1], v[1], v[3], v[0]} }
func (v *Vec4f) YYWY() *Vec4f { return &Vec4f{v[1], v[1], v[3], v[1]} }
func (v *Vec4f) YYWZ() *Vec4f { return &Vec4f{v[1], v[1], v[3], v[2]} }
func (v *Vec4f) YYX() *Vec3f  { return &Vec3f{v[1], v[1], v[0]} }
func (v *Vec4f) YYXW() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[3]} }
func (v *Vec4f) YYXX() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[0]} }
func (v *Vec4f) YYXY() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[1]} }
func (v *Vec4f) YYXZ() *Vec4f { return &Vec4f{v[1], v[1], v[0], v[2]} }
func (v *Vec4f) YYY() *Vec3f  { return &Vec3f{v[1], v[1], v[1]} }
func (v *Vec4f) YYYW() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[3]} }
func (v *Vec4f) YYYX() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[0]} }
func (v *Vec4f) YYYY() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[1]} }
func (v *Vec4f) YYYZ() *Vec4f { return &Vec4f{v[1], v[1], v[1], v[2]} }
func (v *Vec4f) YYZ() *Vec3f  { return &Vec3f{v[1], v[1], v[2]} }
func (v *Vec4f) YYZW() *Vec4f { return &Vec4f{v[1], v[1], v[2], v[3]} }
func (v *Vec4f) YYZX() *Vec4f { return &Vec4f{v[1], v[1], v[2], v[0]} }
func (v *Vec4f) YYZY() *Vec4f { return &Vec4f{v[1], v[1], v[2], v[1]} }
func (v *Vec4f) YYZZ() *Vec4f { return &Vec4f{v[1], v[1], v[2], v[2]} }
func (v *Vec4f) YZ() *Vec2f   { return &Vec2f{v[1], v[2]} }
func (v *Vec4f) YZW() *Vec3f  { return &Vec3f{v[1], v[2], v[3]} }
func (v *Vec4f) YZWW() *Vec4f { return &Vec4f{v[1], v[2], v[3], v[3]} }
func (v *Vec4f) YZWX() *Vec4f { return &Vec4f{v[1], v[2], v[3], v[0]} }
func (v *Vec4f) YZWY() *Vec4f { return &Vec4f{v[1], v[2], v[3], v[1]} }
func (v *Vec4f) YZWZ() *Vec4f { return &Vec4f{v[1], v[2], v[3], v[2]} }
func (v *Vec4f) YZX() *Vec3f  { return &Vec3f{v[1], v[2], v[0]} }
func (v *Vec4f) YZXW() *Vec4f { return &Vec4f{v[1], v[2], v[0], v[3]} }
func (v *Vec4f) YZXX() *Vec4f { return &Vec4f{v[1], v[2], v[0], v[0]} }
func (v *Vec4f) YZXY() *Vec4f { return &Vec4f{v[1], v[2], v[0], v[1]} }
func (v *Vec4f) YZXZ() *Vec4f { return &Vec4f{v[1], v[2], v[0], v[2]} }
func (v *Vec4f) YZY() *Vec3f  { return &Vec3f{v[1], v[2], v[1]} }
func (v *Vec4f) YZYW() *Vec4f { return &Vec4f{v[1], v[2], v[1], v[3]} }
func (v *Vec4f) YZYX() *Vec4f { return &Vec4f{v[1], v[2], v[1], v[0]} }
func (v *Vec4f) YZYY() *Vec4f { return &Vec4f{v[1], v[2], v[1], v[1]} }
func (v *Vec4f) YZYZ() *Vec4f { return &Vec4f{v[1], v[2], v[1], v[2]} }
func (v *Vec4f) YZZ() *Vec3f  { return &Vec3f{v[1], v[2], v[2]} }
func (v *Vec4f) YZZW() *Vec4f { return &Vec4f{v[1], v[2], v[2], v[3]} }
func (v *Vec4f) YZZX() *Vec4f { return &Vec4f{v[1], v[2], v[2], v[0]} }
func (v *Vec4f) YZZY() *Vec4f { return &Vec4f{v[1], v[2], v[2], v[1]} }
func (v *Vec4f) YZZZ() *Vec4f { return &Vec4f{v[1], v[2], v[2], v[2]} }
func (v *Vec4f) ZW() *Vec2f   { return &Vec2f{v[2], v[3]} }
func (v *Vec4f) ZWW() *Vec3f  { return &Vec3f{v[2], v[3], v[3]} }
func (v *Vec4f) ZWWW() *Vec4f { return &Vec4f{v[2], v[3], v[3], v[3]} }
func (v *Vec4f) ZWWX() *Vec4f { return &Vec4f{v[2], v[3], v[3], v[0]} }
func (v *Vec4f) ZWWY() *Vec4f { return &Vec4f{v[2], v[3], v[3], v[1]} }
func (v *Vec4f) ZWWZ() *Vec4f { return &Vec4f{v[2], v[3], v[3], v[2]} }
func (v *Vec4f) ZWX() *Vec3f  { return &Vec3f{v[2], v[3], v[0]} }
func (v *Vec4f) ZWXW() *Vec4f { return &Vec4f{v[2], v[3], v[0], v[3]} }
func (v *Vec4f) ZWXX() *Vec4f { return &Vec4f{v[2], v[3], v[0], v[0]} }
func (v *Vec4f) ZWXY() *Vec4f { return &Vec4f{v[2], v[3], v[0], v[1]} }
func (v *Vec4f) ZWXZ() *Vec4f { return &Vec4f{v[2], v[3], v[0], v[2]} }
func (v *Vec4f) ZWY() *Vec3f  { return &Vec3f{v[2], v[3], v[1]} }
func (v *Vec4f) ZWYW() *Vec4f { return &Vec4f{v[2], v[3], v[1], v[3]} }
func (v *Vec4f) ZWYX() *Vec4f { return &Vec4f{v[2], v[3], v[1], v[0]} }
func (v *Vec4f) ZWYY() *Vec4f { return &Vec4f{v[2], v[3], v[1], v[1]} }
func (v *Vec4f) ZWYZ() *Vec4f { return &Vec4f{v[2], v[3], v[1], v[2]} }
func (v *Vec4f) ZWZ() *Vec3f  { return &Vec3f{v[2], v[3], v[2]} }
func (v *Vec4f) ZWZW() *Vec4f { return &Vec4f{v[2], v[3], v[2], v[3]} }
func (v *Vec4f) ZWZX() *Vec4f { return &Vec4f{v[2], v[3], v[2], v[0]} }
func (v *Vec4f) ZWZY() *Vec4f { return &Vec4f{v[2], v[3], v[2], v[1]} }
func (v *Vec4f) ZWZZ() *Vec4f { return &Vec4f{v[2], v[3], v[2], v[2]} }
func (v *Vec4f) ZX() *Vec2f   { return &Vec2f{v[2], v[0]} }
func (v *Vec4f) ZXW() *Vec3f  { return &Vec3f{v[2], v[0], v[3]} }
func (v *Vec4f) ZXWW() *Vec4f { return &Vec4f{v[2], v[0], v[3], v[3]} }
func (v *Vec4f) ZXWX() *Vec4f { return &Vec4f{v[2], v[0], v[3], v[0]} }
func (v *Vec4f) ZXWY() *Vec4f { return &Vec4f{v[2], v[0], v[3], v[1]} }
func (v *Vec4f) ZXWZ() *Vec4f { return &Vec4f{v[2], v[0], v[3], v[2]} }
func (v *Vec4f) ZXX() *Vec3f  { return &Vec3f{v[2], v[0], v[0]} }
func (v *Vec4f) ZXXW() *Vec4f { return &Vec4f{v[2], v[0], v[0], v[3]} }
func (v *Vec4f) ZXXX() *Vec4f { return &Vec4f{v[2], v[0], v[0], v[0]} }
func (v *Vec4f) ZXXY() *Vec4f { return &Vec4f{v[2], v[0], v[0], v[1]} }
func (v *Vec4f) ZXXZ() *Vec4f { return &Vec4f{v[2], v[0], v[0], v[2]} }
func (v *Vec4f) ZXY() *Vec3f  { return &Vec3f{v[2], v[0], v[1]} }
func (v *Vec4f) ZXYW() *Vec4f { return &Vec4f{v[2], v[0], v[1], v[3]} }
func (v *Vec4f) ZXYX() *Vec4f { return &Vec4f{v[2], v[0], v[1], v[0]} }
func (v *Vec4f) ZXYY() *Vec4f { return &Vec4f{v[2], v[0], v[1], v[1]} }
func (v *Vec4f) ZXYZ() *Vec4f { return &Vec4f{v[2], v[0], v[1], v[2]} }
func (v *Vec4f) ZXZ() *Vec3f  { return &Vec3f{v[2], v[0], v[2]} }
func (v *Vec4f) ZXZW() *Vec4f { return &Vec4f{v[2], v[0], v[2], v[3]} }
func (v *Vec4f) ZXZX() *Vec4f { return &Vec4f{v[2], v[0], v[2], v[0]} }
func (v *Vec4f) ZXZY() *Vec4f { return &Vec4f{v[2], v[0], v[2], v[1]} }
func (v *Vec4f) ZXZZ() *Vec4f { return &Vec4f{v[2], v[0], v[2], v[2]} }
func (v *Vec4f) ZY() *Vec2f   { return &Vec2f{v[2], v[1]} }
func (v *Vec4f) ZYW() *Vec3f  { return &Vec3f{v[2], v[1], v[3]} }
func (v *Vec4f) ZYWW() *Vec4f { return &Vec4f{v[2], v[1], v[3], v[3]} }
func (v *Vec4f) ZYWX() *Vec4f { return &Vec4f{v[2], v[1], v[3], v[0]} }
func (v *Vec4f) ZYWY() *Vec4f { return &Vec4f{v[2], v[1], v[3], v[1]} }
func (v *Vec4f) ZYWZ() *Vec4f { return &Vec4f{v[2], v[1], v[3], v[2]} }
func (v *Vec4f) ZYX() *Vec3f  { return &Vec3f{v[2], v[1], v[0]} }
func (v *Vec4f) ZYXW() *Vec4f { return &Vec4f{v[2], v[1], v[0], v[3]} }
func (v *Vec4f) ZYXX() *Vec4f { return &Vec4f{v[2], v[1], v[0], v[0]} }
func (v *Vec4f) ZYXY() *Vec4f { return &Vec4f{v[2], v[1], v[0], v[1]} }
func (v *Vec4f) ZYXZ() *Vec4f { return &Vec4f{v[2], v[1], v[0], v[2]} }
func (v *Vec4f) ZYY() *Vec3f  { return &Vec3f{v[2], v[1], v[1]} }
func (v *Vec4f) ZYYW() *Vec4f { return &Vec4f{v[2], v[1], v[1], v[3]} }
func (v *Vec4f) ZYYX() *Vec4f { return &Vec4f{v[2], v[1], v[1], v[0]} }
func (v *Vec4f) ZYYY() *Vec4f { return &Vec4f{v[2], v[1], v[1], v[1]} }
func (v *Vec4f) ZYYZ() *Vec4f { return &Vec4f{v[2], v[1], v[1], v[2]} }
func (v *Vec4f) ZYZ() *Vec3f  { return &Vec3f{v[2], v[1], v[2]} }
func (v *Vec4f) ZYZW() *Vec4f { return &Vec4f{v[2], v[1], v[2], v[3]} }
func (v *Vec4f) ZYZX() *Vec4f { return &Vec4f{v[2], v[1], v[2], v[0]} }
func (v *Vec4f) ZYZY() *Vec4f { return &Vec4f{v[2], v[1], v[2], v[1]} }
func (v *Vec4f) ZYZZ() *Vec4f { return &Vec4f{v[2], v[1], v[2], v[2]} }
func (v *Vec4f) ZZ() *Vec2f   { return &Vec2f{v[2], v[2]} }
func (v *Vec4f) ZZW() *Vec3f  { return &Vec3f{v[2], v[2], v[3]} }
func (v *Vec4f) ZZWW() *Vec4f { return &Vec4f{v[2], v[2], v[3], v[3]} }
func (v *Vec4f) ZZWX() *Vec4f { return &Vec4f{v[2], v[2], v[3], v[0]} }
func (v *Vec4f) ZZWY() *Vec4f { return &Vec4f{v[2], v[2], v[3], v[1]} }
func (v *Vec4f) ZZWZ() *Vec4f { return &Vec4f{v[2], v[2], v[3], v[2]} }
func (v *Vec4f) ZZX() *Vec3f  { return &Vec3f{v[2], v[2], v[0]} }
func (v *Vec4f) ZZXW() *Vec4f { return &Vec4f{v[2], v[2], v[0], v[3]} }
func (v *Vec4f) ZZXX() *Vec4f { return &Vec4f{v[2], v[2], v[0], v[0]} }
func (v *Vec4f) ZZXY() *Vec4f { return &Vec4f{v[2], v[2], v[0], v[1]} }
func (v *Vec4f) ZZXZ() *Vec4f { return &Vec4f{v[2], v[2], v[0], v[2]} }
func (v *Vec4f) ZZY() *Vec3f  { return &Vec3f{v[2], v[2], v[1]} }
func (v *Vec4f) ZZYW() *Vec4f { return &Vec4f{v[2], v[2], v[1], v[3]} }
func (v *Vec4f) ZZYX() *Vec4f { return &Vec4f{v[2], v[2], v[1], v[0]} }
func (v *Vec4f) ZZYY() *Vec4f { return &Vec4f{v[2], v[2], v[1], v[1]} }
func (v *Vec4f) ZZYZ() *Vec4f { return &Vec4f{v[2], v[2], v[1], v[2]} }
func (v *Vec4f) ZZZ() *Vec3f  { return &Vec3f{v[2], v[2], v[2]} }
func (v *Vec4f) ZZZW() *Vec4f { return &Vec4f{v[2], v[2], v[2], v[3]} }
func (v *Vec4f) ZZZX() *Vec4f { return &Vec4f{v[2], v[2], v[2], v[0]} }
func (v *Vec4f) ZZZY() *Vec4f { return &Vec4f{v[2], v[2], v[2], v[1]} }
func (v *Vec4f) ZZZZ() *Vec4f { return &Vec4f{v[2], v[2], v[2], v[2]} }

func (v *Vec2f) X() float32 { return v[0] }
func (v *Vec2f) Y() float32 { return v[1] }

func (v *Vec3f) X() float32 { return v[0] }
func (v *Vec3f) Y() float32 { return v[1] }
func (v *Vec3f) Z() float32 { return v[2] }

func (v *Vec4f) X() float32 { return v[0] }
func (v *Vec4f) Y() float32 { return v[1] }
func (v *Vec4f) Z() float32 { return v[2] }
func (v *Vec4f) W() float32 { return v[3] }

func (v *Vec2f) XYZ(z float32) *Vec3f {
	return &Vec3f{v[0], v[1], z}
}

func (v *Vec2f) XYZW(z, w float32) *Vec4f {
	return &Vec4f{v[0], v[1], z, w}
}

func (v *Vec3f) XYZW(w float32) *Vec4f {
	return &Vec4f{v[0], v[1], v[2], w}
}

func (v *Vec2f) Elem() (x, y float32) {
	return v[0], v[1]
}

func (v *Vec3f) Elem() (x, y, z float32) {
	return v[0], v[1], v[2]
}

func (v *Vec4f) Elem() (x, y, z, w float32) {
	return v[0], v[1], v[2], v[3]
}
