package vec

type (
	Order2 uint
	Order3 uint
	Order4 uint

	Axis = uint
)

const (
	X = 0
	Y = 1
	Z = 2
	W = 3
)

// Vector component ordering constants for use with CompareBy()
const (
	OrderXY Order2 = X<<2 | Y
	OrderYX Order2 = Y<<2 | X

	OrderXYZ Order3 = X<<4 | Y<<2 | Z
	OrderXZY Order3 = X<<4 | Z<<2 | Y
	OrderYXZ Order3 = Y<<4 | X<<2 | Z
	OrderYZX Order3 = Y<<4 | Z<<2 | X
	OrderZXY Order3 = Z<<4 | X<<2 | Y
	OrderZYX Order3 = Z<<4 | Y<<2 | X

	OrderXYZW Order4 = X<<6 | Y<<4 | Z<<2 | W
	OrderXYWZ Order4 = X<<6 | Y<<4 | W<<2 | Z
	OrderXZYW Order4 = X<<6 | Z<<4 | Y<<2 | W
	OrderXZWY Order4 = X<<6 | Z<<4 | W<<2 | Y
	OrderXWYZ Order4 = X<<6 | W<<4 | Y<<2 | Z
	OrderXWZY Order4 = X<<6 | W<<4 | Z<<2 | Y

	OrderYXZW Order4 = Y<<6 | X<<4 | Z<<2 | W
	OrderYXWZ Order4 = Y<<6 | X<<4 | W<<2 | Z
	OrderYZXW Order4 = Y<<6 | Z<<4 | X<<2 | W
	OrderYZWX Order4 = Y<<6 | Z<<4 | W<<2 | X
	OrderYWXZ Order4 = Y<<6 | W<<4 | X<<2 | Z
	OrderYWZX Order4 = Y<<6 | W<<4 | Z<<2 | X

	OrderZXYW Order4 = Z<<6 | X<<4 | Y<<2 | W
	OrderZXWY Order4 = Z<<6 | X<<4 | W<<2 | Y
	OrderZYXW Order4 = Z<<6 | Y<<4 | X<<2 | W
	OrderZYWX Order4 = Z<<6 | Y<<4 | W<<2 | X
	OrderZWXY Order4 = Z<<6 | W<<4 | X<<2 | Y
	OrderZWYX Order4 = Z<<6 | W<<4 | Y<<2 | X

	OrderWXYZ Order4 = W<<6 | X<<4 | Y<<2 | Z
	OrderWXZY Order4 = W<<6 | X<<4 | Z<<2 | Y
	OrderWYXZ Order4 = W<<6 | Y<<4 | X<<2 | Z
	OrderWYZX Order4 = W<<6 | Y<<4 | Z<<2 | X
	OrderWZXY Order4 = W<<6 | Z<<4 | X<<2 | Y
	OrderWZYX Order4 = W<<6 | Z<<4 | Y<<2 | X
)
