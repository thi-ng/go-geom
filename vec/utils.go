package vec

func CopySliceVec2(pts []*Vec2) []*Vec2 {
	n := len(pts)
	res := make([]*Vec2, n, n)
	for n--; n >= 0; n-- {
		res[n] = pts[n].Copy()
	}
	return res
}

func CopySliceVec3(pts []*Vec3) []*Vec3 {
	n := len(pts)
	res := make([]*Vec3, n, n)
	for n--; n >= 0; n-- {
		res[n] = pts[n].Copy()
	}
	return res
}

func CopySliceVec4(pts []*Vec4) []*Vec4 {
	n := len(pts)
	res := make([]*Vec4, n, n)
	for n--; n >= 0; n-- {
		res[n] = pts[n].Copy()
	}
	return res
}
