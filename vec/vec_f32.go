package vec

import (
	"fmt"

	"go.thi.ng/geom/math32"
)

type (
	Vec2f [2]float32
	Vec3f [3]float32
	Vec4f [4]float32
)

// Axis / vector component constants

func NewVec2f(x, y float32) *Vec2f {
	return &Vec2f{x, y}
}

func NewVec3f(x, y, z float32) *Vec3f {
	return &Vec3f{x, y, z}
}

func NewVec4f(x, y, z, w float32) *Vec4f {
	return &Vec4f{x, y, z, w}
}

func RandVec2f(n float32) *Vec2f {
	return &Vec2f{
		math32.RandNorm() * n,
		math32.RandNorm() * n}
}

func RandVec3f(n float32) *Vec3f {
	return &Vec3f{
		math32.RandNorm() * n,
		math32.RandNorm() * n,
		math32.RandNorm() * n}
}

func RandVec4f(n float32) *Vec4f {
	return &Vec4f{
		math32.RandNorm() * n,
		math32.RandNorm() * n,
		math32.RandNorm() * n,
		math32.RandNorm() * n}
}

func NewVec2fFrom(buf []float32, idx int) *Vec2f {
	return &Vec2f{buf[idx], buf[idx+Y]}
}

func NewVec3fFrom(buf []float32, idx int) *Vec3f {
	return &Vec3f{buf[idx], buf[idx+Y], buf[idx+Z]}
}

func NewVec4fFrom(buf []float32, idx int) *Vec4f {
	return &Vec4f{buf[idx], buf[idx+Y], buf[idx+Z], buf[idx+W]}
}

func (v *Vec2f) Into(buf []float32, idx int) []float32 {
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec3f) Into(buf []float32, idx int) []float32 {
	buf[idx+Z] = v[Z]
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec4f) Into(buf []float32, idx int) []float32 {
	buf[idx+W] = v[W]
	buf[idx+Z] = v[Z]
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec2f) Set(v2 *Vec2f) *Vec2f {
	v[X] = v2[X]
	v[Y] = v2[Y]
	return v
}

func (v *Vec3f) Set(v2 *Vec3f) *Vec3f {
	v[X] = v2[X]
	v[Y] = v2[Y]
	v[Z] = v2[Z]
	return v
}

func (v *Vec4f) Set(v2 *Vec4f) *Vec4f {
	v[X] = v2[X]
	v[Y] = v2[Y]
	v[Z] = v2[Z]
	v[W] = v2[W]
	return v
}

func (v *Vec2f) SetN(n float32) *Vec2f {
	v[X] = n
	v[Y] = n
	return v
}

func (v *Vec3f) SetN(n float32) *Vec3f {
	v[X] = n
	v[Y] = n
	v[Z] = n
	return v
}

func (v *Vec4f) SetN(n float32) *Vec4f {
	v[X] = n
	v[Y] = n
	v[Z] = n
	v[W] = n
	return v
}

func (v *Vec2f) Copy() *Vec2f {
	return &Vec2f{v[X], v[Y]}
}

func (v *Vec3f) Copy() *Vec3f {
	return &Vec3f{v[X], v[Y], v[Z]}
}

func (v *Vec4f) Copy() *Vec4f {
	return &Vec4f{v[X], v[Y], v[Z], v[W]}
}

// Zero is shorthand for `v.SetN(0)`
func (v *Vec2f) Zero() *Vec2f {
	return v.SetN(0)
}

// Zero is shorthand for `v.SetN(0)`
func (v *Vec3f) Zero() *Vec3f {
	return v.SetN(0)
}

// Zero is shorthand for `v.SetN(0)`
func (v *Vec4f) Zero() *Vec4f {
	return v.SetN(0)
}

// One is shorthand for `v.SetN(1)`
func (v *Vec2f) One() *Vec2f {
	return v.SetN(1)
}

// One is shorthand for `v.SetN(1)`
func (v *Vec3f) One() *Vec3f {
	return v.SetN(1)
}

// One is shorthand for `v.SetN(1)`
func (v *Vec4f) One() *Vec4f {
	return v.SetN(1)
}

func (v *Vec2f) IsZero() bool {
	return math32.Abs(v[X]) < math32.Eps &&
		math32.Abs(v[Y]) < math32.Eps
}

func (v *Vec3f) IsZero() bool {
	return math32.Abs(v[X]) < math32.Eps &&
		math32.Abs(v[Y]) < math32.Eps &&
		math32.Abs(v[Z]) < math32.Eps
}

func (v *Vec4f) IsZero() bool {
	return math32.Abs(v[X]) < math32.Eps &&
		math32.Abs(v[Y]) < math32.Eps &&
		math32.Abs(v[Z]) < math32.Eps &&
		math32.Abs(v[W]) < math32.Eps
}

func (v *Vec2f) Add(v2 *Vec2f) *Vec2f {
	v[X] += v2[X]
	v[Y] += v2[Y]
	return v
}

func (v *Vec2f) Sub(v2 *Vec2f) *Vec2f {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	return v
}

func (v *Vec2f) Mul(v2 *Vec2f) *Vec2f {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	return v
}

func (v *Vec2f) Div(v2 *Vec2f) *Vec2f {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	return v
}

func (v *Vec2f) AddN(n float32) *Vec2f {
	v[X] += n
	v[Y] += n
	return v
}

func (v *Vec2f) SubN(n float32) *Vec2f {
	v[X] -= n
	v[Y] -= n
	return v
}

func (v *Vec2f) MulN(n float32) *Vec2f {
	v[X] *= n
	v[Y] *= n
	return v
}

func (v *Vec2f) DivN(n float32) *Vec2f {
	v[X] /= n
	v[Y] /= n
	return v
}

func (v *Vec3f) Add(v2 *Vec3f) *Vec3f {
	v[X] += v2[X]
	v[Y] += v2[Y]
	v[Z] += v2[Z]
	return v
}

func (v *Vec3f) Sub(v2 *Vec3f) *Vec3f {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	v[Z] -= v2[Z]
	return v
}

func (v *Vec3f) Mul(v2 *Vec3f) *Vec3f {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	v[Z] *= v2[Z]
	return v
}

func (v *Vec3f) Div(v2 *Vec3f) *Vec3f {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	v[Z] /= v2[Z]
	return v
}

func (v *Vec3f) AddN(n float32) *Vec3f {
	v[X] += n
	v[Y] += n
	v[Z] += n
	return v
}

func (v *Vec3f) SubN(n float32) *Vec3f {
	v[X] -= n
	v[Y] -= n
	v[Z] -= n
	return v
}

func (v *Vec3f) MulN(n float32) *Vec3f {
	v[X] *= n
	v[Y] *= n
	v[Z] *= n
	return v
}

func (v *Vec3f) DivN(n float32) *Vec3f {
	v[X] /= n
	v[Y] /= n
	v[Z] /= n
	return v
}

func (v *Vec4f) Add(v2 *Vec4f) *Vec4f {
	v[X] += v2[X]
	v[Y] += v2[Y]
	v[Z] += v2[Z]
	v[W] += v2[W]
	return v
}

func (v *Vec4f) Sub(v2 *Vec4f) *Vec4f {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	v[Z] -= v2[Z]
	v[W] -= v2[W]
	return v
}

func (v *Vec4f) Mul(v2 *Vec4f) *Vec4f {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	v[Z] *= v2[Z]
	v[W] *= v2[W]
	return v
}

func (v *Vec4f) Div(v2 *Vec4f) *Vec4f {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	v[Z] /= v2[Z]
	v[W] /= v2[W]
	return v
}

func (v *Vec4f) AddN(n float32) *Vec4f {
	v[X] += n
	v[Y] += n
	v[Z] += n
	v[W] += n
	return v
}

func (v *Vec4f) SubN(n float32) *Vec4f {
	v[X] -= n
	v[Y] -= n
	v[Z] -= n
	v[W] -= n
	return v
}

func (v *Vec4f) MulN(n float32) *Vec4f {
	v[X] *= n
	v[Y] *= n
	v[Z] *= n
	v[W] *= n
	return v
}

func (v *Vec4f) DivN(n float32) *Vec4f {
	v[X] /= n
	v[Y] /= n
	v[Z] /= n
	v[W] /= n
	return v
}

func (v *Vec2f) Neg() *Vec2f {
	v[X] = -v[X]
	v[Y] = -v[Y]
	return v
}

func (v *Vec3f) Neg() *Vec3f {
	v[X] = -v[X]
	v[Y] = -v[Y]
	v[Z] = -v[Z]
	return v
}

func (v *Vec4f) Neg() *Vec4f {
	v[X] = -v[X]
	v[Y] = -v[Y]
	v[Z] = -v[Z]
	v[W] = -v[W]
	return v
}

func (v *Vec2f) Sign() *Vec2f {
	v[X] = math32.Sign(v[X])
	v[Y] = math32.Sign(v[Y])
	return v
}

func (v *Vec3f) Sign() *Vec3f {
	v[X] = math32.Sign(v[X])
	v[Y] = math32.Sign(v[Y])
	v[Z] = math32.Sign(v[Z])
	return v
}

func (v *Vec4f) Sign() *Vec4f {
	v[X] = math32.Sign(v[X])
	v[Y] = math32.Sign(v[Y])
	v[Z] = math32.Sign(v[Z])
	v[W] = math32.Sign(v[W])
	return v
}

func (v *Vec2f) MaddN(v2 *Vec2f, n float32) *Vec2f {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	return v
}

func (v *Vec3f) MaddN(v2 *Vec3f, n float32) *Vec3f {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	v[Z] += v2[Z] * n
	return v
}

func (v *Vec4f) MaddN(v2 *Vec4f, n float32) *Vec4f {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	v[Z] += v2[Z] * n
	v[W] += v2[W] * n
	return v
}

func (v *Vec2f) MaddV(v2 *Vec2f, n *Vec2f) *Vec2f {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	return v
}

func (v *Vec3f) MaddV(v2 *Vec3f, n *Vec3f) *Vec3f {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	v[Z] += v2[Z] * n[Z]
	return v
}

func (v *Vec4f) MaddV(v2 *Vec4f, n *Vec4f) *Vec4f {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	v[Z] += v2[Z] * n[Z]
	v[W] += v2[W] * n[W]
	return v
}

func (v *Vec2f) MsubN(v2 *Vec2f, n float32) *Vec2f {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	return v
}

func (v *Vec3f) MsubN(v2 *Vec3f, n float32) *Vec3f {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	v[Z] -= v2[Z] * n
	return v
}

func (v *Vec4f) MsubN(v2 *Vec4f, n float32) *Vec4f {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	v[Z] -= v2[Z] * n
	v[W] -= v2[W] * n
	return v
}

func (v *Vec2f) MsubV(v2 *Vec2f, n *Vec2f) *Vec2f {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	return v
}

func (v *Vec3f) MsubV(v2 *Vec3f, n *Vec3f) *Vec3f {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	v[Z] -= v2[Z] * n[Z]
	return v
}

func (v *Vec4f) MsubV(v2 *Vec4f, n *Vec4f) *Vec4f {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	v[Z] -= v2[Z] * n[Z]
	v[W] -= v2[W] * n[W]
	return v
}

// Sum returns sum of vector components
func (v *Vec2f) Sum() float32 {
	return v[X] + v[Y]
}

// Sum returns sum of vector components
func (v *Vec3f) Sum() float32 {
	return v[X] + v[Y] + v[Z]
}

// Sum returns sum of vector components
func (v *Vec4f) Sum() float32 {
	return v[X] + v[Y] + v[Z] + v[W]
}

func (v *Vec2f) Mix(v2 *Vec2f, t float32) *Vec2f {
	v[X] += (v2[X] - v[X]) * t
	v[Y] += (v2[Y] - v[Y]) * t
	return v
}

func (v *Vec3f) Mix(v2 *Vec3f, t float32) *Vec3f {
	v[X] += (v2[X] - v[X]) * t
	v[Y] += (v2[Y] - v[Y]) * t
	v[Z] += (v2[Z] - v[Z]) * t
	return v
}

func (v *Vec4f) Mix(v2 *Vec4f, t float32) *Vec4f {
	v[X] += (v2[X] - v[X]) * t
	v[Y] += (v2[Y] - v[Y]) * t
	v[Z] += (v2[Z] - v[Z]) * t
	v[W] += (v2[W] - v[W]) * t
	return v
}

func (a *Vec2f) MixBilinear(b, c, d *Vec2f, u, v float32) *Vec2f {
	a[X] = math32.MixBilinear(a[X], b[X], c[X], d[X], u, v)
	a[Y] = math32.MixBilinear(a[Y], b[Y], c[Y], d[Y], u, v)
	return a
}

func (a *Vec3f) MixBilinear(b, c, d *Vec3f, u, v float32) *Vec3f {
	a[X] = math32.MixBilinear(a[X], b[X], c[X], d[X], u, v)
	a[Y] = math32.MixBilinear(a[Y], b[Y], c[Y], d[Y], u, v)
	a[Z] = math32.MixBilinear(a[Z], b[Z], c[Z], d[Z], u, v)
	return a
}

func (a *Vec4f) MixBilinear(b, c, d *Vec4f, u, v float32) *Vec4f {
	a[X] = math32.MixBilinear(a[X], b[X], c[X], d[X], u, v)
	a[Y] = math32.MixBilinear(a[Y], b[Y], c[Y], d[Y], u, v)
	a[Z] = math32.MixBilinear(a[Z], b[Z], c[Z], d[Z], u, v)
	a[W] = math32.MixBilinear(a[W], b[W], c[W], d[W], u, v)
	return a
}

// https://keithmaggio.wordpress.com/2011/02/15/math-magician-lerp-slerp-and-nlerp/
func (a *Vec2f) Slerp(b *Vec2f, t float32) *Vec2f {
	dot := math32.Clamp(a.Dot(b), -1.0, 1.0)
	theta := math32.Acos(dot) * t
	r := b.Copy().MsubN(a, dot).Normalize(1)
	return a.MulN(math32.Cos(theta)).MaddN(r, math32.Sin(theta))
}

func (a *Vec3f) Slerp(b *Vec3f, t float32) *Vec3f {
	dot := math32.Clamp(a.Dot(b), -1.0, 1.0)
	theta := math32.Acos(dot) * t
	r := b.Copy().MsubN(a, dot).Normalize(1)
	return a.MulN(math32.Cos(theta)).MaddN(r, math32.Sin(theta))
}

func (a *Vec4f) Slerp(b *Vec4f, t float32) *Vec4f {
	dot := math32.Clamp(a.Dot(b), -1.0, 1.0)
	theta := math32.Acos(dot) * t
	r := b.Copy().MsubN(a, dot).Normalize(1)
	return a.MulN(math32.Cos(theta)).MaddN(r, math32.Sin(theta))
}

func (v *Vec2f) Step(e *Vec2f) *Vec2f {
	v[X] = math32.Step(e[X], v[X])
	v[Y] = math32.Step(e[Y], v[Y])
	return v
}

func (v *Vec3f) Step(e *Vec3f) *Vec3f {
	v[X] = math32.Step(e[X], v[X])
	v[Y] = math32.Step(e[Y], v[Y])
	v[Z] = math32.Step(e[Z], v[Z])
	return v
}

func (v *Vec4f) Step(e *Vec4f) *Vec4f {
	v[X] = math32.Step(e[X], v[X])
	v[Y] = math32.Step(e[Y], v[Y])
	v[Z] = math32.Step(e[Z], v[Z])
	v[W] = math32.Step(e[W], v[W])
	return v
}

func (v *Vec2f) SmoothStep(e, e2 *Vec2f) *Vec2f {
	v[X] = math32.SmoothStep(e[X], e2[X], v[X])
	v[Y] = math32.SmoothStep(e[Y], e2[Y], v[Y])
	return v
}

func (v *Vec3f) SmoothStep(e, e2 *Vec3f) *Vec3f {
	v[X] = math32.SmoothStep(e[X], e2[X], v[X])
	v[Y] = math32.SmoothStep(e[Y], e2[Y], v[Y])
	v[Z] = math32.SmoothStep(e[Z], e2[Z], v[Z])
	return v
}

func (v *Vec4f) SmoothStep(e, e2 *Vec4f) *Vec4f {
	v[X] = math32.SmoothStep(e[X], e2[X], v[X])
	v[Y] = math32.SmoothStep(e[Y], e2[Y], v[Y])
	v[Z] = math32.SmoothStep(e[Z], e2[Z], v[Z])
	v[W] = math32.SmoothStep(e[W], e2[W], v[W])
	return v
}

func (v *Vec2f) SmootherStep(e, e2 *Vec2f) *Vec2f {
	v[X] = math32.SmootherStep(e[X], e2[X], v[X])
	v[Y] = math32.SmootherStep(e[Y], e2[Y], v[Y])
	return v
}

func (v *Vec3f) SmootherStep(e, e2 *Vec3f) *Vec3f {
	v[X] = math32.SmootherStep(e[X], e2[X], v[X])
	v[Y] = math32.SmootherStep(e[Y], e2[Y], v[Y])
	v[Z] = math32.SmootherStep(e[Z], e2[Z], v[Z])
	return v
}

func (v *Vec4f) SmootherStep(e, e2 *Vec4f) *Vec4f {
	v[X] = math32.SmootherStep(e[X], e2[X], v[X])
	v[Y] = math32.SmootherStep(e[Y], e2[Y], v[Y])
	v[Z] = math32.SmootherStep(e[Z], e2[Z], v[Z])
	v[W] = math32.SmootherStep(e[W], e2[W], v[W])
	return v
}

func (v *Vec2f) Abs() *Vec2f {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	return v
}

func (v *Vec3f) Abs() *Vec3f {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	if v[Z] < 0 {
		v[Z] = -v[Z]
	}
	return v
}

func (v *Vec4f) Abs() *Vec4f {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	if v[Z] < 0 {
		v[Z] = -v[Z]
	}
	if v[W] < 0 {
		v[W] = -v[W]
	}
	return v
}

func (v *Vec2f) Dot(v2 *Vec2f) float32 {
	return v[X]*v2[X] + v[Y]*v2[Y]
}

func (v *Vec3f) Dot(v2 *Vec3f) float32 {
	return v[X]*v2[X] + v[Y]*v2[Y] + v[Z]*v2[Z]
}

func (v *Vec4f) Dot(v2 *Vec4f) float32 {
	return v[X]*v2[X] + v[Y]*v2[Y] + v[Z]*v2[Z] + v[W]*v2[W]
}

func (v *Vec2f) Cross(v2 *Vec2f) float32 {
	return v[X]*v2[Y] - v[Y]*v2[X]
}

func (v *Vec3f) Cross(v2 *Vec3f) *Vec3f {
	x := v[Y]*v2[Z] - v[Z]*v2[Y]
	y := v[Z]*v2[X] - v[Z]*v2[X]
	v[Z] = v[X]*v2[Y] - v[Y]*v2[X]
	v[Y] = y
	v[X] = x
	return v
}

func (v *Vec4f) Cross(v2 *Vec4f) *Vec4f {
	x := v[Y]*v2[Z] - v[Z]*v2[Y]
	y := v[Z]*v2[X] - v[Z]*v2[X]
	v[Z] = v[X]*v2[Y] - v[Y]*v2[X]
	v[Y] = y
	v[X] = x
	return v
}

func (v *Vec2f) IsColinear(v2 *Vec2f, eps float32) bool {
	return math32.EqDelta(v.Cross(v2), 0, eps)
}

func (v *Vec3f) IsColinear(v2 *Vec3f, eps float32) bool {
	return math32.EqDelta(v.Copy().Cross(v2).MagSq(), 0, eps*eps)
}

func (a *Vec3f) OrthoNormal(b, c *Vec3f) *Vec3f {
	ba := &Vec3f{b[X] - a[X], b[Y] - a[Y], b[Z] - a[Z]}
	ca := &Vec3f{c[X] - a[X], c[Y] - a[Y], c[Z] - a[Z]}
	return ca.Cross(ba)
}

func (v *Vec3f) Plane() (*Vec3f, *Vec3f) {
	if math32.Abs(v[Z]) > math32.Sqrt2 {
		d := v[Y]*v[Y] + v[Z]*v[Z]
		k := 1 / math32.Sqrt(d)
		p := &Vec3f{0, -v[Z] * k, v[Y] * k}
		return p, &Vec3f{d * k, -v[X] * p[Z], v[X] * p[Y]}
	} else {
		d := v[X]*v[X] + v[Y]*v[Y]
		k := 1 / math32.Sqrt(d)
		p := &Vec3f{-v[Y] * k, v[X] * k, 0}
		return p, &Vec3f{-v[Z] * p[Y], v[Z] * p[X], d * k}
	}
}

func (v *Vec2f) Mag() float32 {
	return math32.Sqrt(v.MagSq())
}

func (v *Vec2f) MagSq() float32 {
	return v[X]*v[X] + v[Y]*v[Y]
}

func (v *Vec3f) Mag() float32 {
	return math32.Sqrt(v.MagSq())
}

func (v *Vec3f) MagSq() float32 {
	return v[X]*v[X] + v[Y]*v[Y] + v[Z]*v[Z]
}

func (v *Vec4f) Mag() float32 {
	return math32.Sqrt(v.MagSq())
}

func (v *Vec4f) MagSq() float32 {
	return v[X]*v[X] + v[Y]*v[Y] + v[Z]*v[Z] + v[W]*v[W]
}

func (v *Vec2f) Normalize(n float32) *Vec2f {
	m := v.Mag()
	if m > 0 {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec3f) Normalize(n float32) *Vec3f {
	m := v.Mag()
	if m > 0 {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec4f) Normalize(n float32) *Vec4f {
	m := v.Mag()
	if m > 0 {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec2f) Limit(n float32) *Vec2f {
	m := v.Mag()
	if m > n {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec3f) Limit(n float32) *Vec3f {
	m := v.Mag()
	if m > n {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec4f) Limit(n float32) *Vec4f {
	m := v.Mag()
	if m > n {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec2f) Dist(v2 *Vec2f) float32 {
	return math32.Sqrt(v.DistSq(v2))
}

func (v *Vec3f) Dist(v2 *Vec3f) float32 {
	return math32.Sqrt(v.DistSq(v2))
}

func (v *Vec2f) DistSq(v2 *Vec2f) float32 {
	dx := v[X] - v2[X]
	dy := v[Y] - v2[Y]
	return dx*dx + dy*dy
}

func (v *Vec3f) DistSq(v2 *Vec3f) float32 {
	dx := v[X] - v2[X]
	dy := v[Y] - v2[Y]
	dz := v[Z] - v2[Z]
	return dx*dx + dy*dy + dz*dz
}

func (v *Vec2f) DistManhattan(v2 *Vec2f) float32 {
	return math32.AbsDiff(v[X], v2[X]) +
		math32.AbsDiff(v[Y], v2[Y])
}

func (v *Vec3f) DistManhattan(v2 *Vec3f) float32 {
	return math32.AbsDiff(v[X], v2[X]) +
		math32.AbsDiff(v[Y], v2[Y]) +
		math32.AbsDiff(v[Z], v2[Z])
}

func (v *Vec2f) DistChebychev(v2 *Vec2f) float32 {
	return math32.Max(
		math32.AbsDiff(v[X], v2[X]),
		math32.AbsDiff(v[Y], v2[Y]))
}

func (v *Vec3f) DistChebychev(v2 *Vec3f) float32 {
	return math32.Max3(
		math32.AbsDiff(v[X], v2[X]),
		math32.AbsDiff(v[Y], v2[Y]),
		math32.AbsDiff(v[Z], v2[Z]))
}

func (v *Vec2f) PerpendicularLeft() *Vec2f {
	x := v[X]
	v[X] = -v[Y]
	v[Y] = x
	return v
}

func (v *Vec2f) PerpendicularRight() *Vec2f {
	x := -v[X]
	v[X] = v[Y]
	v[Y] = x
	return v
}

// Reflect calculates the reflection direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
func (v *Vec2f) Reflect(n *Vec2f) *Vec2f {
	return v.MsubN(n, 2*v.Dot(n))
}

// Reflect calculates the reflection direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
func (v *Vec3f) Reflect(n *Vec3f) *Vec3f {
	return v.MsubN(n, 2*v.Dot(n))
}

// Reflect calculates the reflection direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
func (v *Vec4f) Reflect(n *Vec4f) *Vec4f {
	return v.MsubN(n, 2*v.Dot(n))
}

// Refract computes the refraction direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
func (v *Vec2f) Refract(n *Vec2f, eta float32) *Vec2f {
	if eta == 1.0 {
		return v.Reflect(n)
	}
	d := v.Dot(n)
	k := 1 - eta*eta*(1-d*d)
	if k < 0 {
		return v.Zero()
	}
	k = eta*d + math32.Sqrt(k)
	v[X] = v[X]*eta - n[X]*k
	v[Y] = v[Y]*eta - n[Y]*k
	return v
}

// Refract computes the refraction direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
func (v *Vec3f) Refract(n *Vec3f, eta float32) *Vec3f {
	if eta == 1.0 {
		return v.Reflect(n)
	}
	d := v.Dot(n)
	k := 1 - eta*eta*(1-d*d)
	if k < 0 {
		return v.Zero()
	}
	k = eta*d + math32.Sqrt(k)
	v[X] = v[X]*eta - n[X]*k
	v[Y] = v[Y]*eta - n[Y]*k
	v[Z] = v[Z]*eta - n[Z]*k
	return v
}

// Refract computes the refraction direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
func (v *Vec4f) Refract(n *Vec4f, eta float32) *Vec4f {
	if eta == 1.0 {
		return v.Reflect(n)
	}
	d := v.Dot(n)
	k := 1 - eta*eta*(1-d*d)
	if k < 0 {
		return v.Zero()
	}
	k = eta*d + math32.Sqrt(k)
	v[X] = v[X]*eta - n[X]*k
	v[Y] = v[Y]*eta - n[Y]*k
	v[Z] = v[Z]*eta - n[Z]*k
	v[W] = v[W]*eta - n[W]*k
	return v
}

func (v *Vec2f) Heading() float32 {
	return math32.Atan2Abs(v[Y], v[X])
}

func (v *Vec3f) HeadingXY() float32 {
	return math32.Atan2Abs(v[Y], v[X])
}

func (v *Vec3f) HeadingXZ() float32 {
	return math32.Atan2Abs(v[Z], v[X])
}

func (v *Vec3f) HeadingYZ() float32 {
	return math32.Atan2Abs(v[Z], v[Y])
}

func (v *Vec4f) HeadingXY() float32 {
	return math32.Atan2Abs(v[Y], v[X])
}

func (v *Vec4f) HeadingXZ() float32 {
	return math32.Atan2Abs(v[Z], v[X])
}

func (v *Vec4f) HeadingYZ() float32 {
	return math32.Atan2Abs(v[Z], v[Y])
}

func (v *Vec2f) Polar() *Vec2f {
	theta := v.Heading()
	v[X] = v.Mag()
	v[Y] = theta
	return v
}

func (v *Vec3f) Polar() *Vec3f {
	r := v.Mag()
	z := v[Z]
	v[Z] = math32.Atan2(v[Y], v[X])
	v[Y] = math32.Asin(z / r)
	v[X] = r
	return v
}

func (v *Vec2f) Cartesian() *Vec2f {
	r := v[X]
	theta := v[Y]
	v[X] = r * math32.Cos(theta)
	v[Y] = r * math32.Sin(theta)
	return v
}

func (v *Vec3f) Cartesian() *Vec3f {
	r, y, z := v.Elem()
	rcos := r * math32.Acos(y)
	v[X] = rcos * math32.Cos(z)
	v[Y] = rcos * math32.Sin(z)
	v[Z] = r * math32.Sin(y)
	return v
}

func (v *Vec2f) Rotate(theta float32) *Vec2f {
	s := math32.Sin(theta)
	c := math32.Cos(theta)
	x, y := v.Elem()
	v[X] = x*c - y*s
	v[Y] = x*s + y*c
	return v
}

// RotateAround applies vector rotation around given point p, i.e. uses
// p as rotation center
func (v *Vec2f) RotateAround(p *Vec2f, theta float32) *Vec2f {
	s := math32.Sin(theta)
	c := math32.Cos(theta)
	x, y := v.Elem()
	px, py := p.Elem()
	x -= px
	y -= py
	v[X] = x*c - y*s + px
	v[Y] = x*s + y*c + py
	return v
}

func (v *Vec2f) Min(v2 *Vec2f) *Vec2f {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	return v
}

func (v *Vec2f) Max(v2 *Vec2f) *Vec2f {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	return v
}

func (v *Vec3f) Min(v2 *Vec3f) *Vec3f {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] < v[Z] {
		v[Z] = v2[Z]
	}
	return v
}

func (v *Vec3f) Max(v2 *Vec3f) *Vec3f {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] > v[Z] {
		v[Z] = v2[Z]
	}
	return v
}

func (v *Vec4f) Min(v2 *Vec4f) *Vec4f {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] < v[Z] {
		v[Z] = v2[Z]
	}
	if v2[W] < v[W] {
		v[W] = v2[W]
	}
	return v
}

func (v *Vec4f) Max(v2 *Vec4f) *Vec4f {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] > v[Z] {
		v[Z] = v2[Z]
	}
	if v2[W] > v[W] {
		v[W] = v2[W]
	}
	return v
}

func (v *Vec2f) Clamp(min, max *Vec2f) *Vec2f {
	return v.Min(max).Max(min)
}

func (v *Vec3f) Clamp(min, max *Vec3f) *Vec3f {
	return v.Min(max).Max(min)
}

func (v *Vec4f) Clamp(min, max *Vec4f) *Vec4f {
	return v.Min(max).Max(min)
}

func (v *Vec2f) Floor() *Vec2f {
	v[X] = math32.Floor(v[X])
	v[Y] = math32.Floor(v[Y])
	return v
}

func (v *Vec3f) Floor() *Vec3f {
	v[X] = math32.Floor(v[X])
	v[Y] = math32.Floor(v[Y])
	v[Z] = math32.Floor(v[Z])
	return v
}

func (v *Vec4f) Floor() *Vec4f {
	v[X] = math32.Floor(v[X])
	v[Y] = math32.Floor(v[Y])
	v[Z] = math32.Floor(v[Z])
	v[W] = math32.Floor(v[W])
	return v
}

func (v *Vec2f) Ceil() *Vec2f {
	v[X] = math32.Ceil(v[X])
	v[Y] = math32.Ceil(v[Y])
	return v
}

func (v *Vec3f) Ceil() *Vec3f {
	v[X] = math32.Ceil(v[X])
	v[Y] = math32.Ceil(v[Y])
	v[Z] = math32.Ceil(v[Z])
	return v
}

func (v *Vec4f) Ceil() *Vec4f {
	v[X] = math32.Ceil(v[X])
	v[Y] = math32.Ceil(v[Y])
	v[Z] = math32.Ceil(v[Z])
	v[W] = math32.Ceil(v[W])
	return v
}

func (v *Vec2f) Mod(v2 *Vec2f) *Vec2f {
	v[X] = math32.Mod(v[X], v2[X])
	v[Y] = math32.Mod(v[Y], v2[Y])
	return v
}

func (v *Vec3f) Mod(v2 *Vec3f) *Vec3f {
	v[X] = math32.Mod(v[X], v2[X])
	v[Y] = math32.Mod(v[Y], v2[Y])
	v[Z] = math32.Mod(v[Z], v2[Z])
	return v
}

func (v *Vec4f) Mod(v2 *Vec4f) *Vec4f {
	v[X] = math32.Mod(v[X], v2[X])
	v[Y] = math32.Mod(v[Y], v2[Y])
	v[Z] = math32.Mod(v[Z], v2[Z])
	v[W] = math32.Mod(v[W], v2[W])
	return v
}

func (v *Vec2f) MajorAxis() uint {
	if math32.Abs(v[X]) >= math32.Abs(v[Y]) {
		return X
	}
	return Y
}

func (v *Vec2f) MinorAxis() uint {
	if math32.Abs(v[X]) <= math32.Abs(v[Y]) {
		return X
	}
	return Y
}

func (v *Vec3f) MajorAxis() uint {
	x := math32.Abs(v[X])
	y := math32.Abs(v[Y])
	z := math32.Abs(v[Z])
	if x >= y {
		if x >= z {
			return X
		}
		return Z
	}
	if x >= z {
		if x >= y {
			return X
		}
		return Y
	}
	if y >= z {
		return Y
	}
	return Z
}

func (v *Vec3f) MinorAxis() uint {
	x := math32.Abs(v[X])
	y := math32.Abs(v[Y])
	z := math32.Abs(v[Z])
	if x <= y {
		if x <= z {
			return X
		}
		return Z
	}
	if x <= z {
		if x <= y {
			return X
		}
		return Y
	}
	if y <= z {
		return Y
	}
	return Z
}

func (v *Vec2f) Eq(v2 *Vec2f) bool {
	return v[X] == v2[X] && v[Y] == v2[Y]
}

func (v *Vec3f) Eq(v2 *Vec3f) bool {
	return v[X] == v2[X] && v[Y] == v2[Y] && v[Z] == v2[Z]
}

func (v *Vec4f) Eq(v2 *Vec4f) bool {
	return v[X] == v2[X] && v[Y] == v2[Y] && v[Z] == v2[Z] && v[W] == v2[W]
}

func (v *Vec2f) EqDelta(v2 *Vec2f, eps float32) bool {
	return math32.EqDelta(v[X], v2[X], eps) &&
		math32.EqDelta(v[Y], v2[Y], eps)
}

func (v *Vec3f) EqDelta(v2 *Vec3f, eps float32) bool {
	return math32.EqDelta(v[X], v2[X], eps) &&
		math32.EqDelta(v[Y], v2[Y], eps) &&
		math32.EqDelta(v[Z], v2[Z], eps)
}

func (v *Vec4f) EqDelta(v2 *Vec4f, eps float32) bool {
	return math32.EqDelta(v[X], v2[X], eps) &&
		math32.EqDelta(v[Y], v2[Y], eps) &&
		math32.EqDelta(v[Z], v2[Z], eps) &&
		math32.EqDelta(v[W], v2[W], eps)
}

func (v *Vec2f) CompareBy(v2 *Vec2f, order Order2) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXY
	}
	x, y := order>>2&1, order&1
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			return 0
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec3f) CompareBy(v2 *Vec3f, order Order3) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXYZ
	}
	x, y, z := order>>4&3, order>>2&3, order&3
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			if v[z] == v2[z] {
				return 0
			} else if v[z] < v2[z] {
				return -3
			}
			return 3
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec4f) CompareBy(v2 *Vec4f, order Order4) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXYZW
	}
	x, y, z, w := order>>6&3, order>>4&3, order>>2&3, order&3
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			if v[z] == v2[z] {
				if v[w] == v2[w] {
					return 0
				} else if v[w] < v2[w] {
					return -4
				}
				return 4
			} else if v[z] < v2[z] {
				return -3
			}
			return 3
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec2f) String() string {
	return fmt.Sprintf("[%.4f, %.4f]", v[X], v[Y])
}

func (v *Vec3f) String() string {
	return fmt.Sprintf("[%.4f, %.4f, %.4f]", v[X], v[Y], v[Z])
}

func (v *Vec4f) String() string {
	return fmt.Sprintf("[%.4f, %.4f, %.4f, %.4f]", v[X], v[Y], v[Z], v[W])
}

func (v *Vec2f) GoString() string {
	return fmt.Sprintf("vec.Vec2{%#v, %#v}", v[X], v[Y])
}

func (v *Vec3f) GoString() string {
	return fmt.Sprintf("vec.Vec3{%#v, %#v, %#v}", v[X], v[Y], v[Z])
}

func (v *Vec4f) GoString() string {
	return fmt.Sprintf("vec.Vec4{%#v, %#v, %#v, %#v}", v[X], v[Y], v[Z], v[W])
}
