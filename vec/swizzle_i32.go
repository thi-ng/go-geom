package vec

func (v *Vec2i) XX() *Vec2i   { return &Vec2i{v[0], v[0]} }
func (v *Vec2i) XXX() *Vec3i  { return &Vec3i{v[0], v[0], v[0]} }
func (v *Vec2i) XXXX() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[0]} }
func (v *Vec2i) XXXY() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[1]} }
func (v *Vec2i) XXY() *Vec3i  { return &Vec3i{v[0], v[0], v[1]} }
func (v *Vec2i) XXYX() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[0]} }
func (v *Vec2i) XXYY() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[1]} }
func (v *Vec2i) XY() *Vec2i   { return &Vec2i{v[0], v[1]} }
func (v *Vec2i) XYX() *Vec3i  { return &Vec3i{v[0], v[1], v[0]} }
func (v *Vec2i) XYXX() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[0]} }
func (v *Vec2i) XYXY() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[1]} }
func (v *Vec2i) XYY() *Vec3i  { return &Vec3i{v[0], v[1], v[1]} }
func (v *Vec2i) XYYX() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[0]} }
func (v *Vec2i) XYYY() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[1]} }
func (v *Vec2i) YX() *Vec2i   { return &Vec2i{v[1], v[0]} }
func (v *Vec2i) YXX() *Vec3i  { return &Vec3i{v[1], v[0], v[0]} }
func (v *Vec2i) YXXX() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[0]} }
func (v *Vec2i) YXXY() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[1]} }
func (v *Vec2i) YXY() *Vec3i  { return &Vec3i{v[1], v[0], v[1]} }
func (v *Vec2i) YXYX() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[0]} }
func (v *Vec2i) YXYY() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[1]} }
func (v *Vec2i) YY() *Vec2i   { return &Vec2i{v[1], v[1]} }
func (v *Vec2i) YYX() *Vec3i  { return &Vec3i{v[1], v[1], v[0]} }
func (v *Vec2i) YYXX() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[0]} }
func (v *Vec2i) YYXY() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[1]} }
func (v *Vec2i) YYY() *Vec3i  { return &Vec3i{v[1], v[1], v[1]} }
func (v *Vec2i) YYYX() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[0]} }
func (v *Vec2i) YYYY() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[1]} }

func (v *Vec3i) XX() *Vec2i   { return &Vec2i{v[0], v[0]} }
func (v *Vec3i) XXX() *Vec3i  { return &Vec3i{v[0], v[0], v[0]} }
func (v *Vec3i) XXXX() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[0]} }
func (v *Vec3i) XXXY() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[1]} }
func (v *Vec3i) XXXZ() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[2]} }
func (v *Vec3i) XXY() *Vec3i  { return &Vec3i{v[0], v[0], v[1]} }
func (v *Vec3i) XXYX() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[0]} }
func (v *Vec3i) XXYY() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[1]} }
func (v *Vec3i) XXYZ() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[2]} }
func (v *Vec3i) XXZ() *Vec3i  { return &Vec3i{v[0], v[0], v[2]} }
func (v *Vec3i) XXZX() *Vec4i { return &Vec4i{v[0], v[0], v[2], v[0]} }
func (v *Vec3i) XXZY() *Vec4i { return &Vec4i{v[0], v[0], v[2], v[1]} }
func (v *Vec3i) XXZZ() *Vec4i { return &Vec4i{v[0], v[0], v[2], v[2]} }
func (v *Vec3i) XY() *Vec2i   { return &Vec2i{v[0], v[1]} }
func (v *Vec3i) XYX() *Vec3i  { return &Vec3i{v[0], v[1], v[0]} }
func (v *Vec3i) XYXX() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[0]} }
func (v *Vec3i) XYXY() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[1]} }
func (v *Vec3i) XYXZ() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[2]} }
func (v *Vec3i) XYY() *Vec3i  { return &Vec3i{v[0], v[1], v[1]} }
func (v *Vec3i) XYYX() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[0]} }
func (v *Vec3i) XYYY() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[1]} }
func (v *Vec3i) XYYZ() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[2]} }
func (v *Vec3i) XYZ() *Vec3i  { return &Vec3i{v[0], v[1], v[2]} }
func (v *Vec3i) XYZX() *Vec4i { return &Vec4i{v[0], v[1], v[2], v[0]} }
func (v *Vec3i) XYZY() *Vec4i { return &Vec4i{v[0], v[1], v[2], v[1]} }
func (v *Vec3i) XYZZ() *Vec4i { return &Vec4i{v[0], v[1], v[2], v[2]} }
func (v *Vec3i) XZ() *Vec2i   { return &Vec2i{v[0], v[2]} }
func (v *Vec3i) XZX() *Vec3i  { return &Vec3i{v[0], v[2], v[0]} }
func (v *Vec3i) XZXX() *Vec4i { return &Vec4i{v[0], v[2], v[0], v[0]} }
func (v *Vec3i) XZXY() *Vec4i { return &Vec4i{v[0], v[2], v[0], v[1]} }
func (v *Vec3i) XZXZ() *Vec4i { return &Vec4i{v[0], v[2], v[0], v[2]} }
func (v *Vec3i) XZY() *Vec3i  { return &Vec3i{v[0], v[2], v[1]} }
func (v *Vec3i) XZYX() *Vec4i { return &Vec4i{v[0], v[2], v[1], v[0]} }
func (v *Vec3i) XZYY() *Vec4i { return &Vec4i{v[0], v[2], v[1], v[1]} }
func (v *Vec3i) XZYZ() *Vec4i { return &Vec4i{v[0], v[2], v[1], v[2]} }
func (v *Vec3i) XZZ() *Vec3i  { return &Vec3i{v[0], v[2], v[2]} }
func (v *Vec3i) XZZX() *Vec4i { return &Vec4i{v[0], v[2], v[2], v[0]} }
func (v *Vec3i) XZZY() *Vec4i { return &Vec4i{v[0], v[2], v[2], v[1]} }
func (v *Vec3i) XZZZ() *Vec4i { return &Vec4i{v[0], v[2], v[2], v[2]} }
func (v *Vec3i) YX() *Vec2i   { return &Vec2i{v[1], v[0]} }
func (v *Vec3i) YXX() *Vec3i  { return &Vec3i{v[1], v[0], v[0]} }
func (v *Vec3i) YXXX() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[0]} }
func (v *Vec3i) YXXY() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[1]} }
func (v *Vec3i) YXXZ() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[2]} }
func (v *Vec3i) YXY() *Vec3i  { return &Vec3i{v[1], v[0], v[1]} }
func (v *Vec3i) YXYX() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[0]} }
func (v *Vec3i) YXYY() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[1]} }
func (v *Vec3i) YXYZ() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[2]} }
func (v *Vec3i) YXZ() *Vec3i  { return &Vec3i{v[1], v[0], v[2]} }
func (v *Vec3i) YXZX() *Vec4i { return &Vec4i{v[1], v[0], v[2], v[0]} }
func (v *Vec3i) YXZY() *Vec4i { return &Vec4i{v[1], v[0], v[2], v[1]} }
func (v *Vec3i) YXZZ() *Vec4i { return &Vec4i{v[1], v[0], v[2], v[2]} }
func (v *Vec3i) YY() *Vec2i   { return &Vec2i{v[1], v[1]} }
func (v *Vec3i) YYX() *Vec3i  { return &Vec3i{v[1], v[1], v[0]} }
func (v *Vec3i) YYXX() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[0]} }
func (v *Vec3i) YYXY() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[1]} }
func (v *Vec3i) YYXZ() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[2]} }
func (v *Vec3i) YYY() *Vec3i  { return &Vec3i{v[1], v[1], v[1]} }
func (v *Vec3i) YYYX() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[0]} }
func (v *Vec3i) YYYY() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[1]} }
func (v *Vec3i) YYYZ() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[2]} }
func (v *Vec3i) YYZ() *Vec3i  { return &Vec3i{v[1], v[1], v[2]} }
func (v *Vec3i) YYZX() *Vec4i { return &Vec4i{v[1], v[1], v[2], v[0]} }
func (v *Vec3i) YYZY() *Vec4i { return &Vec4i{v[1], v[1], v[2], v[1]} }
func (v *Vec3i) YYZZ() *Vec4i { return &Vec4i{v[1], v[1], v[2], v[2]} }
func (v *Vec3i) YZ() *Vec2i   { return &Vec2i{v[1], v[2]} }
func (v *Vec3i) YZX() *Vec3i  { return &Vec3i{v[1], v[2], v[0]} }
func (v *Vec3i) YZXX() *Vec4i { return &Vec4i{v[1], v[2], v[0], v[0]} }
func (v *Vec3i) YZXY() *Vec4i { return &Vec4i{v[1], v[2], v[0], v[1]} }
func (v *Vec3i) YZXZ() *Vec4i { return &Vec4i{v[1], v[2], v[0], v[2]} }
func (v *Vec3i) YZY() *Vec3i  { return &Vec3i{v[1], v[2], v[1]} }
func (v *Vec3i) YZYX() *Vec4i { return &Vec4i{v[1], v[2], v[1], v[0]} }
func (v *Vec3i) YZYY() *Vec4i { return &Vec4i{v[1], v[2], v[1], v[1]} }
func (v *Vec3i) YZYZ() *Vec4i { return &Vec4i{v[1], v[2], v[1], v[2]} }
func (v *Vec3i) YZZ() *Vec3i  { return &Vec3i{v[1], v[2], v[2]} }
func (v *Vec3i) YZZX() *Vec4i { return &Vec4i{v[1], v[2], v[2], v[0]} }
func (v *Vec3i) YZZY() *Vec4i { return &Vec4i{v[1], v[2], v[2], v[1]} }
func (v *Vec3i) YZZZ() *Vec4i { return &Vec4i{v[1], v[2], v[2], v[2]} }
func (v *Vec3i) ZX() *Vec2i   { return &Vec2i{v[2], v[0]} }
func (v *Vec3i) ZXX() *Vec3i  { return &Vec3i{v[2], v[0], v[0]} }
func (v *Vec3i) ZXXX() *Vec4i { return &Vec4i{v[2], v[0], v[0], v[0]} }
func (v *Vec3i) ZXXY() *Vec4i { return &Vec4i{v[2], v[0], v[0], v[1]} }
func (v *Vec3i) ZXXZ() *Vec4i { return &Vec4i{v[2], v[0], v[0], v[2]} }
func (v *Vec3i) ZXY() *Vec3i  { return &Vec3i{v[2], v[0], v[1]} }
func (v *Vec3i) ZXYX() *Vec4i { return &Vec4i{v[2], v[0], v[1], v[0]} }
func (v *Vec3i) ZXYY() *Vec4i { return &Vec4i{v[2], v[0], v[1], v[1]} }
func (v *Vec3i) ZXYZ() *Vec4i { return &Vec4i{v[2], v[0], v[1], v[2]} }
func (v *Vec3i) ZXZ() *Vec3i  { return &Vec3i{v[2], v[0], v[2]} }
func (v *Vec3i) ZXZX() *Vec4i { return &Vec4i{v[2], v[0], v[2], v[0]} }
func (v *Vec3i) ZXZY() *Vec4i { return &Vec4i{v[2], v[0], v[2], v[1]} }
func (v *Vec3i) ZXZZ() *Vec4i { return &Vec4i{v[2], v[0], v[2], v[2]} }
func (v *Vec3i) ZY() *Vec2i   { return &Vec2i{v[2], v[1]} }
func (v *Vec3i) ZYX() *Vec3i  { return &Vec3i{v[2], v[1], v[0]} }
func (v *Vec3i) ZYXX() *Vec4i { return &Vec4i{v[2], v[1], v[0], v[0]} }
func (v *Vec3i) ZYXY() *Vec4i { return &Vec4i{v[2], v[1], v[0], v[1]} }
func (v *Vec3i) ZYXZ() *Vec4i { return &Vec4i{v[2], v[1], v[0], v[2]} }
func (v *Vec3i) ZYY() *Vec3i  { return &Vec3i{v[2], v[1], v[1]} }
func (v *Vec3i) ZYYX() *Vec4i { return &Vec4i{v[2], v[1], v[1], v[0]} }
func (v *Vec3i) ZYYY() *Vec4i { return &Vec4i{v[2], v[1], v[1], v[1]} }
func (v *Vec3i) ZYYZ() *Vec4i { return &Vec4i{v[2], v[1], v[1], v[2]} }
func (v *Vec3i) ZYZ() *Vec3i  { return &Vec3i{v[2], v[1], v[2]} }
func (v *Vec3i) ZYZX() *Vec4i { return &Vec4i{v[2], v[1], v[2], v[0]} }
func (v *Vec3i) ZYZY() *Vec4i { return &Vec4i{v[2], v[1], v[2], v[1]} }
func (v *Vec3i) ZYZZ() *Vec4i { return &Vec4i{v[2], v[1], v[2], v[2]} }
func (v *Vec3i) ZZ() *Vec2i   { return &Vec2i{v[2], v[2]} }
func (v *Vec3i) ZZX() *Vec3i  { return &Vec3i{v[2], v[2], v[0]} }
func (v *Vec3i) ZZXX() *Vec4i { return &Vec4i{v[2], v[2], v[0], v[0]} }
func (v *Vec3i) ZZXY() *Vec4i { return &Vec4i{v[2], v[2], v[0], v[1]} }
func (v *Vec3i) ZZXZ() *Vec4i { return &Vec4i{v[2], v[2], v[0], v[2]} }
func (v *Vec3i) ZZY() *Vec3i  { return &Vec3i{v[2], v[2], v[1]} }
func (v *Vec3i) ZZYX() *Vec4i { return &Vec4i{v[2], v[2], v[1], v[0]} }
func (v *Vec3i) ZZYY() *Vec4i { return &Vec4i{v[2], v[2], v[1], v[1]} }
func (v *Vec3i) ZZYZ() *Vec4i { return &Vec4i{v[2], v[2], v[1], v[2]} }
func (v *Vec3i) ZZZ() *Vec3i  { return &Vec3i{v[2], v[2], v[2]} }
func (v *Vec3i) ZZZX() *Vec4i { return &Vec4i{v[2], v[2], v[2], v[0]} }
func (v *Vec3i) ZZZY() *Vec4i { return &Vec4i{v[2], v[2], v[2], v[1]} }
func (v *Vec3i) ZZZZ() *Vec4i { return &Vec4i{v[2], v[2], v[2], v[2]} }

func (v *Vec4i) WW() *Vec2i   { return &Vec2i{v[3], v[3]} }
func (v *Vec4i) WWW() *Vec3i  { return &Vec3i{v[3], v[3], v[3]} }
func (v *Vec4i) WWWW() *Vec4i { return &Vec4i{v[3], v[3], v[3], v[3]} }
func (v *Vec4i) WWWX() *Vec4i { return &Vec4i{v[3], v[3], v[3], v[0]} }
func (v *Vec4i) WWWY() *Vec4i { return &Vec4i{v[3], v[3], v[3], v[1]} }
func (v *Vec4i) WWWZ() *Vec4i { return &Vec4i{v[3], v[3], v[3], v[2]} }
func (v *Vec4i) WWX() *Vec3i  { return &Vec3i{v[3], v[3], v[0]} }
func (v *Vec4i) WWXW() *Vec4i { return &Vec4i{v[3], v[3], v[0], v[3]} }
func (v *Vec4i) WWXX() *Vec4i { return &Vec4i{v[3], v[3], v[0], v[0]} }
func (v *Vec4i) WWXY() *Vec4i { return &Vec4i{v[3], v[3], v[0], v[1]} }
func (v *Vec4i) WWXZ() *Vec4i { return &Vec4i{v[3], v[3], v[0], v[2]} }
func (v *Vec4i) WWY() *Vec3i  { return &Vec3i{v[3], v[3], v[1]} }
func (v *Vec4i) WWYW() *Vec4i { return &Vec4i{v[3], v[3], v[1], v[3]} }
func (v *Vec4i) WWYX() *Vec4i { return &Vec4i{v[3], v[3], v[1], v[0]} }
func (v *Vec4i) WWYY() *Vec4i { return &Vec4i{v[3], v[3], v[1], v[1]} }
func (v *Vec4i) WWYZ() *Vec4i { return &Vec4i{v[3], v[3], v[1], v[2]} }
func (v *Vec4i) WWZ() *Vec3i  { return &Vec3i{v[3], v[3], v[2]} }
func (v *Vec4i) WWZW() *Vec4i { return &Vec4i{v[3], v[3], v[2], v[3]} }
func (v *Vec4i) WWZX() *Vec4i { return &Vec4i{v[3], v[3], v[2], v[0]} }
func (v *Vec4i) WWZY() *Vec4i { return &Vec4i{v[3], v[3], v[2], v[1]} }
func (v *Vec4i) WWZZ() *Vec4i { return &Vec4i{v[3], v[3], v[2], v[2]} }
func (v *Vec4i) WX() *Vec2i   { return &Vec2i{v[3], v[0]} }
func (v *Vec4i) WXW() *Vec3i  { return &Vec3i{v[3], v[0], v[3]} }
func (v *Vec4i) WXWW() *Vec4i { return &Vec4i{v[3], v[0], v[3], v[3]} }
func (v *Vec4i) WXWX() *Vec4i { return &Vec4i{v[3], v[0], v[3], v[0]} }
func (v *Vec4i) WXWY() *Vec4i { return &Vec4i{v[3], v[0], v[3], v[1]} }
func (v *Vec4i) WXWZ() *Vec4i { return &Vec4i{v[3], v[0], v[3], v[2]} }
func (v *Vec4i) WXX() *Vec3i  { return &Vec3i{v[3], v[0], v[0]} }
func (v *Vec4i) WXXW() *Vec4i { return &Vec4i{v[3], v[0], v[0], v[3]} }
func (v *Vec4i) WXXX() *Vec4i { return &Vec4i{v[3], v[0], v[0], v[0]} }
func (v *Vec4i) WXXY() *Vec4i { return &Vec4i{v[3], v[0], v[0], v[1]} }
func (v *Vec4i) WXXZ() *Vec4i { return &Vec4i{v[3], v[0], v[0], v[2]} }
func (v *Vec4i) WXY() *Vec3i  { return &Vec3i{v[3], v[0], v[1]} }
func (v *Vec4i) WXYW() *Vec4i { return &Vec4i{v[3], v[0], v[1], v[3]} }
func (v *Vec4i) WXYX() *Vec4i { return &Vec4i{v[3], v[0], v[1], v[0]} }
func (v *Vec4i) WXYY() *Vec4i { return &Vec4i{v[3], v[0], v[1], v[1]} }
func (v *Vec4i) WXYZ() *Vec4i { return &Vec4i{v[3], v[0], v[1], v[2]} }
func (v *Vec4i) WXZ() *Vec3i  { return &Vec3i{v[3], v[0], v[2]} }
func (v *Vec4i) WXZW() *Vec4i { return &Vec4i{v[3], v[0], v[2], v[3]} }
func (v *Vec4i) WXZX() *Vec4i { return &Vec4i{v[3], v[0], v[2], v[0]} }
func (v *Vec4i) WXZY() *Vec4i { return &Vec4i{v[3], v[0], v[2], v[1]} }
func (v *Vec4i) WXZZ() *Vec4i { return &Vec4i{v[3], v[0], v[2], v[2]} }
func (v *Vec4i) WY() *Vec2i   { return &Vec2i{v[3], v[1]} }
func (v *Vec4i) WYW() *Vec3i  { return &Vec3i{v[3], v[1], v[3]} }
func (v *Vec4i) WYWW() *Vec4i { return &Vec4i{v[3], v[1], v[3], v[3]} }
func (v *Vec4i) WYWX() *Vec4i { return &Vec4i{v[3], v[1], v[3], v[0]} }
func (v *Vec4i) WYWY() *Vec4i { return &Vec4i{v[3], v[1], v[3], v[1]} }
func (v *Vec4i) WYWZ() *Vec4i { return &Vec4i{v[3], v[1], v[3], v[2]} }
func (v *Vec4i) WYX() *Vec3i  { return &Vec3i{v[3], v[1], v[0]} }
func (v *Vec4i) WYXW() *Vec4i { return &Vec4i{v[3], v[1], v[0], v[3]} }
func (v *Vec4i) WYXX() *Vec4i { return &Vec4i{v[3], v[1], v[0], v[0]} }
func (v *Vec4i) WYXY() *Vec4i { return &Vec4i{v[3], v[1], v[0], v[1]} }
func (v *Vec4i) WYXZ() *Vec4i { return &Vec4i{v[3], v[1], v[0], v[2]} }
func (v *Vec4i) WYY() *Vec3i  { return &Vec3i{v[3], v[1], v[1]} }
func (v *Vec4i) WYYW() *Vec4i { return &Vec4i{v[3], v[1], v[1], v[3]} }
func (v *Vec4i) WYYX() *Vec4i { return &Vec4i{v[3], v[1], v[1], v[0]} }
func (v *Vec4i) WYYY() *Vec4i { return &Vec4i{v[3], v[1], v[1], v[1]} }
func (v *Vec4i) WYYZ() *Vec4i { return &Vec4i{v[3], v[1], v[1], v[2]} }
func (v *Vec4i) WYZ() *Vec3i  { return &Vec3i{v[3], v[1], v[2]} }
func (v *Vec4i) WYZW() *Vec4i { return &Vec4i{v[3], v[1], v[2], v[3]} }
func (v *Vec4i) WYZX() *Vec4i { return &Vec4i{v[3], v[1], v[2], v[0]} }
func (v *Vec4i) WYZY() *Vec4i { return &Vec4i{v[3], v[1], v[2], v[1]} }
func (v *Vec4i) WYZZ() *Vec4i { return &Vec4i{v[3], v[1], v[2], v[2]} }
func (v *Vec4i) WZ() *Vec2i   { return &Vec2i{v[3], v[2]} }
func (v *Vec4i) WZW() *Vec3i  { return &Vec3i{v[3], v[2], v[3]} }
func (v *Vec4i) WZWW() *Vec4i { return &Vec4i{v[3], v[2], v[3], v[3]} }
func (v *Vec4i) WZWX() *Vec4i { return &Vec4i{v[3], v[2], v[3], v[0]} }
func (v *Vec4i) WZWY() *Vec4i { return &Vec4i{v[3], v[2], v[3], v[1]} }
func (v *Vec4i) WZWZ() *Vec4i { return &Vec4i{v[3], v[2], v[3], v[2]} }
func (v *Vec4i) WZX() *Vec3i  { return &Vec3i{v[3], v[2], v[0]} }
func (v *Vec4i) WZXW() *Vec4i { return &Vec4i{v[3], v[2], v[0], v[3]} }
func (v *Vec4i) WZXX() *Vec4i { return &Vec4i{v[3], v[2], v[0], v[0]} }
func (v *Vec4i) WZXY() *Vec4i { return &Vec4i{v[3], v[2], v[0], v[1]} }
func (v *Vec4i) WZXZ() *Vec4i { return &Vec4i{v[3], v[2], v[0], v[2]} }
func (v *Vec4i) WZY() *Vec3i  { return &Vec3i{v[3], v[2], v[1]} }
func (v *Vec4i) WZYW() *Vec4i { return &Vec4i{v[3], v[2], v[1], v[3]} }
func (v *Vec4i) WZYX() *Vec4i { return &Vec4i{v[3], v[2], v[1], v[0]} }
func (v *Vec4i) WZYY() *Vec4i { return &Vec4i{v[3], v[2], v[1], v[1]} }
func (v *Vec4i) WZYZ() *Vec4i { return &Vec4i{v[3], v[2], v[1], v[2]} }
func (v *Vec4i) WZZ() *Vec3i  { return &Vec3i{v[3], v[2], v[2]} }
func (v *Vec4i) WZZW() *Vec4i { return &Vec4i{v[3], v[2], v[2], v[3]} }
func (v *Vec4i) WZZX() *Vec4i { return &Vec4i{v[3], v[2], v[2], v[0]} }
func (v *Vec4i) WZZY() *Vec4i { return &Vec4i{v[3], v[2], v[2], v[1]} }
func (v *Vec4i) WZZZ() *Vec4i { return &Vec4i{v[3], v[2], v[2], v[2]} }
func (v *Vec4i) XW() *Vec2i   { return &Vec2i{v[0], v[3]} }
func (v *Vec4i) XWW() *Vec3i  { return &Vec3i{v[0], v[3], v[3]} }
func (v *Vec4i) XWWW() *Vec4i { return &Vec4i{v[0], v[3], v[3], v[3]} }
func (v *Vec4i) XWWX() *Vec4i { return &Vec4i{v[0], v[3], v[3], v[0]} }
func (v *Vec4i) XWWY() *Vec4i { return &Vec4i{v[0], v[3], v[3], v[1]} }
func (v *Vec4i) XWWZ() *Vec4i { return &Vec4i{v[0], v[3], v[3], v[2]} }
func (v *Vec4i) XWX() *Vec3i  { return &Vec3i{v[0], v[3], v[0]} }
func (v *Vec4i) XWXW() *Vec4i { return &Vec4i{v[0], v[3], v[0], v[3]} }
func (v *Vec4i) XWXX() *Vec4i { return &Vec4i{v[0], v[3], v[0], v[0]} }
func (v *Vec4i) XWXY() *Vec4i { return &Vec4i{v[0], v[3], v[0], v[1]} }
func (v *Vec4i) XWXZ() *Vec4i { return &Vec4i{v[0], v[3], v[0], v[2]} }
func (v *Vec4i) XWY() *Vec3i  { return &Vec3i{v[0], v[3], v[1]} }
func (v *Vec4i) XWYW() *Vec4i { return &Vec4i{v[0], v[3], v[1], v[3]} }
func (v *Vec4i) XWYX() *Vec4i { return &Vec4i{v[0], v[3], v[1], v[0]} }
func (v *Vec4i) XWYY() *Vec4i { return &Vec4i{v[0], v[3], v[1], v[1]} }
func (v *Vec4i) XWYZ() *Vec4i { return &Vec4i{v[0], v[3], v[1], v[2]} }
func (v *Vec4i) XWZ() *Vec3i  { return &Vec3i{v[0], v[3], v[2]} }
func (v *Vec4i) XWZW() *Vec4i { return &Vec4i{v[0], v[3], v[2], v[3]} }
func (v *Vec4i) XWZX() *Vec4i { return &Vec4i{v[0], v[3], v[2], v[0]} }
func (v *Vec4i) XWZY() *Vec4i { return &Vec4i{v[0], v[3], v[2], v[1]} }
func (v *Vec4i) XWZZ() *Vec4i { return &Vec4i{v[0], v[3], v[2], v[2]} }
func (v *Vec4i) XX() *Vec2i   { return &Vec2i{v[0], v[0]} }
func (v *Vec4i) XXW() *Vec3i  { return &Vec3i{v[0], v[0], v[3]} }
func (v *Vec4i) XXWW() *Vec4i { return &Vec4i{v[0], v[0], v[3], v[3]} }
func (v *Vec4i) XXWX() *Vec4i { return &Vec4i{v[0], v[0], v[3], v[0]} }
func (v *Vec4i) XXWY() *Vec4i { return &Vec4i{v[0], v[0], v[3], v[1]} }
func (v *Vec4i) XXWZ() *Vec4i { return &Vec4i{v[0], v[0], v[3], v[2]} }
func (v *Vec4i) XXX() *Vec3i  { return &Vec3i{v[0], v[0], v[0]} }
func (v *Vec4i) XXXW() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[3]} }
func (v *Vec4i) XXXX() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[0]} }
func (v *Vec4i) XXXY() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[1]} }
func (v *Vec4i) XXXZ() *Vec4i { return &Vec4i{v[0], v[0], v[0], v[2]} }
func (v *Vec4i) XXY() *Vec3i  { return &Vec3i{v[0], v[0], v[1]} }
func (v *Vec4i) XXYW() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[3]} }
func (v *Vec4i) XXYX() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[0]} }
func (v *Vec4i) XXYY() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[1]} }
func (v *Vec4i) XXYZ() *Vec4i { return &Vec4i{v[0], v[0], v[1], v[2]} }
func (v *Vec4i) XXZ() *Vec3i  { return &Vec3i{v[0], v[0], v[2]} }
func (v *Vec4i) XXZW() *Vec4i { return &Vec4i{v[0], v[0], v[2], v[3]} }
func (v *Vec4i) XXZX() *Vec4i { return &Vec4i{v[0], v[0], v[2], v[0]} }
func (v *Vec4i) XXZY() *Vec4i { return &Vec4i{v[0], v[0], v[2], v[1]} }
func (v *Vec4i) XXZZ() *Vec4i { return &Vec4i{v[0], v[0], v[2], v[2]} }
func (v *Vec4i) XY() *Vec2i   { return &Vec2i{v[0], v[1]} }
func (v *Vec4i) XYW() *Vec3i  { return &Vec3i{v[0], v[1], v[3]} }
func (v *Vec4i) XYWW() *Vec4i { return &Vec4i{v[0], v[1], v[3], v[3]} }
func (v *Vec4i) XYWX() *Vec4i { return &Vec4i{v[0], v[1], v[3], v[0]} }
func (v *Vec4i) XYWY() *Vec4i { return &Vec4i{v[0], v[1], v[3], v[1]} }
func (v *Vec4i) XYWZ() *Vec4i { return &Vec4i{v[0], v[1], v[3], v[2]} }
func (v *Vec4i) XYX() *Vec3i  { return &Vec3i{v[0], v[1], v[0]} }
func (v *Vec4i) XYXW() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[3]} }
func (v *Vec4i) XYXX() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[0]} }
func (v *Vec4i) XYXY() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[1]} }
func (v *Vec4i) XYXZ() *Vec4i { return &Vec4i{v[0], v[1], v[0], v[2]} }
func (v *Vec4i) XYY() *Vec3i  { return &Vec3i{v[0], v[1], v[1]} }
func (v *Vec4i) XYYW() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[3]} }
func (v *Vec4i) XYYX() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[0]} }
func (v *Vec4i) XYYY() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[1]} }
func (v *Vec4i) XYYZ() *Vec4i { return &Vec4i{v[0], v[1], v[1], v[2]} }
func (v *Vec4i) XYZ() *Vec3i  { return &Vec3i{v[0], v[1], v[2]} }
func (v *Vec4i) XYZW() *Vec4i { return &Vec4i{v[0], v[1], v[2], v[3]} }
func (v *Vec4i) XYZX() *Vec4i { return &Vec4i{v[0], v[1], v[2], v[0]} }
func (v *Vec4i) XYZY() *Vec4i { return &Vec4i{v[0], v[1], v[2], v[1]} }
func (v *Vec4i) XYZZ() *Vec4i { return &Vec4i{v[0], v[1], v[2], v[2]} }
func (v *Vec4i) XZ() *Vec2i   { return &Vec2i{v[0], v[2]} }
func (v *Vec4i) XZW() *Vec3i  { return &Vec3i{v[0], v[2], v[3]} }
func (v *Vec4i) XZWW() *Vec4i { return &Vec4i{v[0], v[2], v[3], v[3]} }
func (v *Vec4i) XZWX() *Vec4i { return &Vec4i{v[0], v[2], v[3], v[0]} }
func (v *Vec4i) XZWY() *Vec4i { return &Vec4i{v[0], v[2], v[3], v[1]} }
func (v *Vec4i) XZWZ() *Vec4i { return &Vec4i{v[0], v[2], v[3], v[2]} }
func (v *Vec4i) XZX() *Vec3i  { return &Vec3i{v[0], v[2], v[0]} }
func (v *Vec4i) XZXW() *Vec4i { return &Vec4i{v[0], v[2], v[0], v[3]} }
func (v *Vec4i) XZXX() *Vec4i { return &Vec4i{v[0], v[2], v[0], v[0]} }
func (v *Vec4i) XZXY() *Vec4i { return &Vec4i{v[0], v[2], v[0], v[1]} }
func (v *Vec4i) XZXZ() *Vec4i { return &Vec4i{v[0], v[2], v[0], v[2]} }
func (v *Vec4i) XZY() *Vec3i  { return &Vec3i{v[0], v[2], v[1]} }
func (v *Vec4i) XZYW() *Vec4i { return &Vec4i{v[0], v[2], v[1], v[3]} }
func (v *Vec4i) XZYX() *Vec4i { return &Vec4i{v[0], v[2], v[1], v[0]} }
func (v *Vec4i) XZYY() *Vec4i { return &Vec4i{v[0], v[2], v[1], v[1]} }
func (v *Vec4i) XZYZ() *Vec4i { return &Vec4i{v[0], v[2], v[1], v[2]} }
func (v *Vec4i) XZZ() *Vec3i  { return &Vec3i{v[0], v[2], v[2]} }
func (v *Vec4i) XZZW() *Vec4i { return &Vec4i{v[0], v[2], v[2], v[3]} }
func (v *Vec4i) XZZX() *Vec4i { return &Vec4i{v[0], v[2], v[2], v[0]} }
func (v *Vec4i) XZZY() *Vec4i { return &Vec4i{v[0], v[2], v[2], v[1]} }
func (v *Vec4i) XZZZ() *Vec4i { return &Vec4i{v[0], v[2], v[2], v[2]} }
func (v *Vec4i) YW() *Vec2i   { return &Vec2i{v[1], v[3]} }
func (v *Vec4i) YWW() *Vec3i  { return &Vec3i{v[1], v[3], v[3]} }
func (v *Vec4i) YWWW() *Vec4i { return &Vec4i{v[1], v[3], v[3], v[3]} }
func (v *Vec4i) YWWX() *Vec4i { return &Vec4i{v[1], v[3], v[3], v[0]} }
func (v *Vec4i) YWWY() *Vec4i { return &Vec4i{v[1], v[3], v[3], v[1]} }
func (v *Vec4i) YWWZ() *Vec4i { return &Vec4i{v[1], v[3], v[3], v[2]} }
func (v *Vec4i) YWX() *Vec3i  { return &Vec3i{v[1], v[3], v[0]} }
func (v *Vec4i) YWXW() *Vec4i { return &Vec4i{v[1], v[3], v[0], v[3]} }
func (v *Vec4i) YWXX() *Vec4i { return &Vec4i{v[1], v[3], v[0], v[0]} }
func (v *Vec4i) YWXY() *Vec4i { return &Vec4i{v[1], v[3], v[0], v[1]} }
func (v *Vec4i) YWXZ() *Vec4i { return &Vec4i{v[1], v[3], v[0], v[2]} }
func (v *Vec4i) YWY() *Vec3i  { return &Vec3i{v[1], v[3], v[1]} }
func (v *Vec4i) YWYW() *Vec4i { return &Vec4i{v[1], v[3], v[1], v[3]} }
func (v *Vec4i) YWYX() *Vec4i { return &Vec4i{v[1], v[3], v[1], v[0]} }
func (v *Vec4i) YWYY() *Vec4i { return &Vec4i{v[1], v[3], v[1], v[1]} }
func (v *Vec4i) YWYZ() *Vec4i { return &Vec4i{v[1], v[3], v[1], v[2]} }
func (v *Vec4i) YWZ() *Vec3i  { return &Vec3i{v[1], v[3], v[2]} }
func (v *Vec4i) YWZW() *Vec4i { return &Vec4i{v[1], v[3], v[2], v[3]} }
func (v *Vec4i) YWZX() *Vec4i { return &Vec4i{v[1], v[3], v[2], v[0]} }
func (v *Vec4i) YWZY() *Vec4i { return &Vec4i{v[1], v[3], v[2], v[1]} }
func (v *Vec4i) YWZZ() *Vec4i { return &Vec4i{v[1], v[3], v[2], v[2]} }
func (v *Vec4i) YX() *Vec2i   { return &Vec2i{v[1], v[0]} }
func (v *Vec4i) YXW() *Vec3i  { return &Vec3i{v[1], v[0], v[3]} }
func (v *Vec4i) YXWW() *Vec4i { return &Vec4i{v[1], v[0], v[3], v[3]} }
func (v *Vec4i) YXWX() *Vec4i { return &Vec4i{v[1], v[0], v[3], v[0]} }
func (v *Vec4i) YXWY() *Vec4i { return &Vec4i{v[1], v[0], v[3], v[1]} }
func (v *Vec4i) YXWZ() *Vec4i { return &Vec4i{v[1], v[0], v[3], v[2]} }
func (v *Vec4i) YXX() *Vec3i  { return &Vec3i{v[1], v[0], v[0]} }
func (v *Vec4i) YXXW() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[3]} }
func (v *Vec4i) YXXX() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[0]} }
func (v *Vec4i) YXXY() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[1]} }
func (v *Vec4i) YXXZ() *Vec4i { return &Vec4i{v[1], v[0], v[0], v[2]} }
func (v *Vec4i) YXY() *Vec3i  { return &Vec3i{v[1], v[0], v[1]} }
func (v *Vec4i) YXYW() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[3]} }
func (v *Vec4i) YXYX() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[0]} }
func (v *Vec4i) YXYY() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[1]} }
func (v *Vec4i) YXYZ() *Vec4i { return &Vec4i{v[1], v[0], v[1], v[2]} }
func (v *Vec4i) YXZ() *Vec3i  { return &Vec3i{v[1], v[0], v[2]} }
func (v *Vec4i) YXZW() *Vec4i { return &Vec4i{v[1], v[0], v[2], v[3]} }
func (v *Vec4i) YXZX() *Vec4i { return &Vec4i{v[1], v[0], v[2], v[0]} }
func (v *Vec4i) YXZY() *Vec4i { return &Vec4i{v[1], v[0], v[2], v[1]} }
func (v *Vec4i) YXZZ() *Vec4i { return &Vec4i{v[1], v[0], v[2], v[2]} }
func (v *Vec4i) YY() *Vec2i   { return &Vec2i{v[1], v[1]} }
func (v *Vec4i) YYW() *Vec3i  { return &Vec3i{v[1], v[1], v[3]} }
func (v *Vec4i) YYWW() *Vec4i { return &Vec4i{v[1], v[1], v[3], v[3]} }
func (v *Vec4i) YYWX() *Vec4i { return &Vec4i{v[1], v[1], v[3], v[0]} }
func (v *Vec4i) YYWY() *Vec4i { return &Vec4i{v[1], v[1], v[3], v[1]} }
func (v *Vec4i) YYWZ() *Vec4i { return &Vec4i{v[1], v[1], v[3], v[2]} }
func (v *Vec4i) YYX() *Vec3i  { return &Vec3i{v[1], v[1], v[0]} }
func (v *Vec4i) YYXW() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[3]} }
func (v *Vec4i) YYXX() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[0]} }
func (v *Vec4i) YYXY() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[1]} }
func (v *Vec4i) YYXZ() *Vec4i { return &Vec4i{v[1], v[1], v[0], v[2]} }
func (v *Vec4i) YYY() *Vec3i  { return &Vec3i{v[1], v[1], v[1]} }
func (v *Vec4i) YYYW() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[3]} }
func (v *Vec4i) YYYX() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[0]} }
func (v *Vec4i) YYYY() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[1]} }
func (v *Vec4i) YYYZ() *Vec4i { return &Vec4i{v[1], v[1], v[1], v[2]} }
func (v *Vec4i) YYZ() *Vec3i  { return &Vec3i{v[1], v[1], v[2]} }
func (v *Vec4i) YYZW() *Vec4i { return &Vec4i{v[1], v[1], v[2], v[3]} }
func (v *Vec4i) YYZX() *Vec4i { return &Vec4i{v[1], v[1], v[2], v[0]} }
func (v *Vec4i) YYZY() *Vec4i { return &Vec4i{v[1], v[1], v[2], v[1]} }
func (v *Vec4i) YYZZ() *Vec4i { return &Vec4i{v[1], v[1], v[2], v[2]} }
func (v *Vec4i) YZ() *Vec2i   { return &Vec2i{v[1], v[2]} }
func (v *Vec4i) YZW() *Vec3i  { return &Vec3i{v[1], v[2], v[3]} }
func (v *Vec4i) YZWW() *Vec4i { return &Vec4i{v[1], v[2], v[3], v[3]} }
func (v *Vec4i) YZWX() *Vec4i { return &Vec4i{v[1], v[2], v[3], v[0]} }
func (v *Vec4i) YZWY() *Vec4i { return &Vec4i{v[1], v[2], v[3], v[1]} }
func (v *Vec4i) YZWZ() *Vec4i { return &Vec4i{v[1], v[2], v[3], v[2]} }
func (v *Vec4i) YZX() *Vec3i  { return &Vec3i{v[1], v[2], v[0]} }
func (v *Vec4i) YZXW() *Vec4i { return &Vec4i{v[1], v[2], v[0], v[3]} }
func (v *Vec4i) YZXX() *Vec4i { return &Vec4i{v[1], v[2], v[0], v[0]} }
func (v *Vec4i) YZXY() *Vec4i { return &Vec4i{v[1], v[2], v[0], v[1]} }
func (v *Vec4i) YZXZ() *Vec4i { return &Vec4i{v[1], v[2], v[0], v[2]} }
func (v *Vec4i) YZY() *Vec3i  { return &Vec3i{v[1], v[2], v[1]} }
func (v *Vec4i) YZYW() *Vec4i { return &Vec4i{v[1], v[2], v[1], v[3]} }
func (v *Vec4i) YZYX() *Vec4i { return &Vec4i{v[1], v[2], v[1], v[0]} }
func (v *Vec4i) YZYY() *Vec4i { return &Vec4i{v[1], v[2], v[1], v[1]} }
func (v *Vec4i) YZYZ() *Vec4i { return &Vec4i{v[1], v[2], v[1], v[2]} }
func (v *Vec4i) YZZ() *Vec3i  { return &Vec3i{v[1], v[2], v[2]} }
func (v *Vec4i) YZZW() *Vec4i { return &Vec4i{v[1], v[2], v[2], v[3]} }
func (v *Vec4i) YZZX() *Vec4i { return &Vec4i{v[1], v[2], v[2], v[0]} }
func (v *Vec4i) YZZY() *Vec4i { return &Vec4i{v[1], v[2], v[2], v[1]} }
func (v *Vec4i) YZZZ() *Vec4i { return &Vec4i{v[1], v[2], v[2], v[2]} }
func (v *Vec4i) ZW() *Vec2i   { return &Vec2i{v[2], v[3]} }
func (v *Vec4i) ZWW() *Vec3i  { return &Vec3i{v[2], v[3], v[3]} }
func (v *Vec4i) ZWWW() *Vec4i { return &Vec4i{v[2], v[3], v[3], v[3]} }
func (v *Vec4i) ZWWX() *Vec4i { return &Vec4i{v[2], v[3], v[3], v[0]} }
func (v *Vec4i) ZWWY() *Vec4i { return &Vec4i{v[2], v[3], v[3], v[1]} }
func (v *Vec4i) ZWWZ() *Vec4i { return &Vec4i{v[2], v[3], v[3], v[2]} }
func (v *Vec4i) ZWX() *Vec3i  { return &Vec3i{v[2], v[3], v[0]} }
func (v *Vec4i) ZWXW() *Vec4i { return &Vec4i{v[2], v[3], v[0], v[3]} }
func (v *Vec4i) ZWXX() *Vec4i { return &Vec4i{v[2], v[3], v[0], v[0]} }
func (v *Vec4i) ZWXY() *Vec4i { return &Vec4i{v[2], v[3], v[0], v[1]} }
func (v *Vec4i) ZWXZ() *Vec4i { return &Vec4i{v[2], v[3], v[0], v[2]} }
func (v *Vec4i) ZWY() *Vec3i  { return &Vec3i{v[2], v[3], v[1]} }
func (v *Vec4i) ZWYW() *Vec4i { return &Vec4i{v[2], v[3], v[1], v[3]} }
func (v *Vec4i) ZWYX() *Vec4i { return &Vec4i{v[2], v[3], v[1], v[0]} }
func (v *Vec4i) ZWYY() *Vec4i { return &Vec4i{v[2], v[3], v[1], v[1]} }
func (v *Vec4i) ZWYZ() *Vec4i { return &Vec4i{v[2], v[3], v[1], v[2]} }
func (v *Vec4i) ZWZ() *Vec3i  { return &Vec3i{v[2], v[3], v[2]} }
func (v *Vec4i) ZWZW() *Vec4i { return &Vec4i{v[2], v[3], v[2], v[3]} }
func (v *Vec4i) ZWZX() *Vec4i { return &Vec4i{v[2], v[3], v[2], v[0]} }
func (v *Vec4i) ZWZY() *Vec4i { return &Vec4i{v[2], v[3], v[2], v[1]} }
func (v *Vec4i) ZWZZ() *Vec4i { return &Vec4i{v[2], v[3], v[2], v[2]} }
func (v *Vec4i) ZX() *Vec2i   { return &Vec2i{v[2], v[0]} }
func (v *Vec4i) ZXW() *Vec3i  { return &Vec3i{v[2], v[0], v[3]} }
func (v *Vec4i) ZXWW() *Vec4i { return &Vec4i{v[2], v[0], v[3], v[3]} }
func (v *Vec4i) ZXWX() *Vec4i { return &Vec4i{v[2], v[0], v[3], v[0]} }
func (v *Vec4i) ZXWY() *Vec4i { return &Vec4i{v[2], v[0], v[3], v[1]} }
func (v *Vec4i) ZXWZ() *Vec4i { return &Vec4i{v[2], v[0], v[3], v[2]} }
func (v *Vec4i) ZXX() *Vec3i  { return &Vec3i{v[2], v[0], v[0]} }
func (v *Vec4i) ZXXW() *Vec4i { return &Vec4i{v[2], v[0], v[0], v[3]} }
func (v *Vec4i) ZXXX() *Vec4i { return &Vec4i{v[2], v[0], v[0], v[0]} }
func (v *Vec4i) ZXXY() *Vec4i { return &Vec4i{v[2], v[0], v[0], v[1]} }
func (v *Vec4i) ZXXZ() *Vec4i { return &Vec4i{v[2], v[0], v[0], v[2]} }
func (v *Vec4i) ZXY() *Vec3i  { return &Vec3i{v[2], v[0], v[1]} }
func (v *Vec4i) ZXYW() *Vec4i { return &Vec4i{v[2], v[0], v[1], v[3]} }
func (v *Vec4i) ZXYX() *Vec4i { return &Vec4i{v[2], v[0], v[1], v[0]} }
func (v *Vec4i) ZXYY() *Vec4i { return &Vec4i{v[2], v[0], v[1], v[1]} }
func (v *Vec4i) ZXYZ() *Vec4i { return &Vec4i{v[2], v[0], v[1], v[2]} }
func (v *Vec4i) ZXZ() *Vec3i  { return &Vec3i{v[2], v[0], v[2]} }
func (v *Vec4i) ZXZW() *Vec4i { return &Vec4i{v[2], v[0], v[2], v[3]} }
func (v *Vec4i) ZXZX() *Vec4i { return &Vec4i{v[2], v[0], v[2], v[0]} }
func (v *Vec4i) ZXZY() *Vec4i { return &Vec4i{v[2], v[0], v[2], v[1]} }
func (v *Vec4i) ZXZZ() *Vec4i { return &Vec4i{v[2], v[0], v[2], v[2]} }
func (v *Vec4i) ZY() *Vec2i   { return &Vec2i{v[2], v[1]} }
func (v *Vec4i) ZYW() *Vec3i  { return &Vec3i{v[2], v[1], v[3]} }
func (v *Vec4i) ZYWW() *Vec4i { return &Vec4i{v[2], v[1], v[3], v[3]} }
func (v *Vec4i) ZYWX() *Vec4i { return &Vec4i{v[2], v[1], v[3], v[0]} }
func (v *Vec4i) ZYWY() *Vec4i { return &Vec4i{v[2], v[1], v[3], v[1]} }
func (v *Vec4i) ZYWZ() *Vec4i { return &Vec4i{v[2], v[1], v[3], v[2]} }
func (v *Vec4i) ZYX() *Vec3i  { return &Vec3i{v[2], v[1], v[0]} }
func (v *Vec4i) ZYXW() *Vec4i { return &Vec4i{v[2], v[1], v[0], v[3]} }
func (v *Vec4i) ZYXX() *Vec4i { return &Vec4i{v[2], v[1], v[0], v[0]} }
func (v *Vec4i) ZYXY() *Vec4i { return &Vec4i{v[2], v[1], v[0], v[1]} }
func (v *Vec4i) ZYXZ() *Vec4i { return &Vec4i{v[2], v[1], v[0], v[2]} }
func (v *Vec4i) ZYY() *Vec3i  { return &Vec3i{v[2], v[1], v[1]} }
func (v *Vec4i) ZYYW() *Vec4i { return &Vec4i{v[2], v[1], v[1], v[3]} }
func (v *Vec4i) ZYYX() *Vec4i { return &Vec4i{v[2], v[1], v[1], v[0]} }
func (v *Vec4i) ZYYY() *Vec4i { return &Vec4i{v[2], v[1], v[1], v[1]} }
func (v *Vec4i) ZYYZ() *Vec4i { return &Vec4i{v[2], v[1], v[1], v[2]} }
func (v *Vec4i) ZYZ() *Vec3i  { return &Vec3i{v[2], v[1], v[2]} }
func (v *Vec4i) ZYZW() *Vec4i { return &Vec4i{v[2], v[1], v[2], v[3]} }
func (v *Vec4i) ZYZX() *Vec4i { return &Vec4i{v[2], v[1], v[2], v[0]} }
func (v *Vec4i) ZYZY() *Vec4i { return &Vec4i{v[2], v[1], v[2], v[1]} }
func (v *Vec4i) ZYZZ() *Vec4i { return &Vec4i{v[2], v[1], v[2], v[2]} }
func (v *Vec4i) ZZ() *Vec2i   { return &Vec2i{v[2], v[2]} }
func (v *Vec4i) ZZW() *Vec3i  { return &Vec3i{v[2], v[2], v[3]} }
func (v *Vec4i) ZZWW() *Vec4i { return &Vec4i{v[2], v[2], v[3], v[3]} }
func (v *Vec4i) ZZWX() *Vec4i { return &Vec4i{v[2], v[2], v[3], v[0]} }
func (v *Vec4i) ZZWY() *Vec4i { return &Vec4i{v[2], v[2], v[3], v[1]} }
func (v *Vec4i) ZZWZ() *Vec4i { return &Vec4i{v[2], v[2], v[3], v[2]} }
func (v *Vec4i) ZZX() *Vec3i  { return &Vec3i{v[2], v[2], v[0]} }
func (v *Vec4i) ZZXW() *Vec4i { return &Vec4i{v[2], v[2], v[0], v[3]} }
func (v *Vec4i) ZZXX() *Vec4i { return &Vec4i{v[2], v[2], v[0], v[0]} }
func (v *Vec4i) ZZXY() *Vec4i { return &Vec4i{v[2], v[2], v[0], v[1]} }
func (v *Vec4i) ZZXZ() *Vec4i { return &Vec4i{v[2], v[2], v[0], v[2]} }
func (v *Vec4i) ZZY() *Vec3i  { return &Vec3i{v[2], v[2], v[1]} }
func (v *Vec4i) ZZYW() *Vec4i { return &Vec4i{v[2], v[2], v[1], v[3]} }
func (v *Vec4i) ZZYX() *Vec4i { return &Vec4i{v[2], v[2], v[1], v[0]} }
func (v *Vec4i) ZZYY() *Vec4i { return &Vec4i{v[2], v[2], v[1], v[1]} }
func (v *Vec4i) ZZYZ() *Vec4i { return &Vec4i{v[2], v[2], v[1], v[2]} }
func (v *Vec4i) ZZZ() *Vec3i  { return &Vec3i{v[2], v[2], v[2]} }
func (v *Vec4i) ZZZW() *Vec4i { return &Vec4i{v[2], v[2], v[2], v[3]} }
func (v *Vec4i) ZZZX() *Vec4i { return &Vec4i{v[2], v[2], v[2], v[0]} }
func (v *Vec4i) ZZZY() *Vec4i { return &Vec4i{v[2], v[2], v[2], v[1]} }
func (v *Vec4i) ZZZZ() *Vec4i { return &Vec4i{v[2], v[2], v[2], v[2]} }

func (v *Vec2i) X() int32 { return v[0] }
func (v *Vec2i) Y() int32 { return v[1] }

func (v *Vec3i) X() int32 { return v[0] }
func (v *Vec3i) Y() int32 { return v[1] }
func (v *Vec3i) Z() int32 { return v[2] }

func (v *Vec4i) X() int32 { return v[0] }
func (v *Vec4i) Y() int32 { return v[1] }
func (v *Vec4i) Z() int32 { return v[2] }
func (v *Vec4i) W() int32 { return v[3] }

func (v *Vec2i) XYZ(z int32) *Vec3i {
	return &Vec3i{v[0], v[1], z}
}

func (v *Vec2i) XYZW(z, w int32) *Vec4i {
	return &Vec4i{v[0], v[1], z, w}
}

func (v *Vec3i) XYZW(w int32) *Vec4i {
	return &Vec4i{v[0], v[1], v[2], w}
}

func (v *Vec2i) Elem() (x, y int32) {
	return v[0], v[1]
}

func (v *Vec3i) Elem() (x, y, z int32) {
	return v[0], v[1], v[2]
}

func (v *Vec4i) Elem() (x, y, z, w int32) {
	return v[0], v[1], v[2], v[3]
}
