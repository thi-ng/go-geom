// Package vec provides 2D, 3D and 4D vector types and algebraic
// operations for int32, float32 and float64
package vec
