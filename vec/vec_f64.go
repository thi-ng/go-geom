package vec

import (
	"fmt"
	"math"

	"go.thi.ng/geom/math64"
)

type (
	Vec2 [2]float64
	Vec3 [3]float64
	Vec4 [4]float64
)

// Axis / vector component constants

func NewVec2(x, y float64) *Vec2 {
	return &Vec2{x, y}
}

func NewVec3(x, y, z float64) *Vec3 {
	return &Vec3{x, y, z}
}

func NewVec4(x, y, z, w float64) *Vec4 {
	return &Vec4{x, y, z, w}
}

func RandVec2(n float64) *Vec2 {
	return &Vec2{
		math64.RandNorm() * n,
		math64.RandNorm() * n}
}

func RandVec3(n float64) *Vec3 {
	return &Vec3{
		math64.RandNorm() * n,
		math64.RandNorm() * n,
		math64.RandNorm() * n}
}

func RandVec4(n float64) *Vec4 {
	return &Vec4{
		math64.RandNorm() * n,
		math64.RandNorm() * n,
		math64.RandNorm() * n,
		math64.RandNorm() * n}
}

func NewVec2From(buf []float64, idx int) *Vec2 {
	return &Vec2{buf[idx], buf[idx+Y]}
}

func NewVec3From(buf []float64, idx int) *Vec3 {
	return &Vec3{buf[idx], buf[idx+Y], buf[idx+Z]}
}

func NewVec4From(buf []float64, idx int) *Vec4 {
	return &Vec4{buf[idx], buf[idx+Y], buf[idx+Z], buf[idx+W]}
}

func (v *Vec2) Into(buf []float64, idx int) []float64 {
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec3) Into(buf []float64, idx int) []float64 {
	buf[idx+Z] = v[Z]
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec4) Into(buf []float64, idx int) []float64 {
	buf[idx+W] = v[W]
	buf[idx+Z] = v[Z]
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec2) Set(v2 *Vec2) *Vec2 {
	v[X] = v2[X]
	v[Y] = v2[Y]
	return v
}

func (v *Vec3) Set(v2 *Vec3) *Vec3 {
	v[X] = v2[X]
	v[Y] = v2[Y]
	v[Z] = v2[Z]
	return v
}

func (v *Vec4) Set(v2 *Vec4) *Vec4 {
	v[X] = v2[X]
	v[Y] = v2[Y]
	v[Z] = v2[Z]
	v[W] = v2[W]
	return v
}

func (v *Vec2) SetN(n float64) *Vec2 {
	v[X] = n
	v[Y] = n
	return v
}

func (v *Vec3) SetN(n float64) *Vec3 {
	v[X] = n
	v[Y] = n
	v[Z] = n
	return v
}

func (v *Vec4) SetN(n float64) *Vec4 {
	v[X] = n
	v[Y] = n
	v[Z] = n
	v[W] = n
	return v
}

func (v *Vec2) Copy() *Vec2 {
	return &Vec2{v[X], v[Y]}
}

func (v *Vec3) Copy() *Vec3 {
	return &Vec3{v[X], v[Y], v[Z]}
}

func (v *Vec4) Copy() *Vec4 {
	return &Vec4{v[X], v[Y], v[Z], v[W]}
}

// Zero is shorthand for v.SetN(0)
func (v *Vec2) Zero() *Vec2 {
	return v.SetN(0)
}

// Zero is shorthand for v.SetN(0)
func (v *Vec3) Zero() *Vec3 {
	return v.SetN(0)
}

// Zero is shorthand for v.SetN(0)
func (v *Vec4) Zero() *Vec4 {
	return v.SetN(0)
}

// One is shorthand for v.SetN(1)
func (v *Vec2) One() *Vec2 {
	return v.SetN(1)
}

// One is shorthand for v.SetN(1)
func (v *Vec3) One() *Vec3 {
	return v.SetN(1)
}

// One is shorthand for v.SetN(1)
func (v *Vec4) One() *Vec4 {
	return v.SetN(1)
}

func (v *Vec2) IsZero() bool {
	return math.Abs(v[X]) < math64.Eps &&
		math.Abs(v[Y]) < math64.Eps
}

func (v *Vec3) IsZero() bool {
	return math.Abs(v[X]) < math64.Eps &&
		math.Abs(v[Y]) < math64.Eps &&
		math.Abs(v[Z]) < math64.Eps
}

func (v *Vec4) IsZero() bool {
	return math.Abs(v[X]) < math64.Eps &&
		math.Abs(v[Y]) < math64.Eps &&
		math.Abs(v[Z]) < math64.Eps &&
		math.Abs(v[W]) < math64.Eps
}

func (v *Vec2) Add(v2 *Vec2) *Vec2 {
	v[X] += v2[X]
	v[Y] += v2[Y]
	return v
}

func (v *Vec2) Sub(v2 *Vec2) *Vec2 {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	return v
}

func (v *Vec2) Mul(v2 *Vec2) *Vec2 {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	return v
}

func (v *Vec2) Div(v2 *Vec2) *Vec2 {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	return v
}

func (v *Vec2) AddN(n float64) *Vec2 {
	v[X] += n
	v[Y] += n
	return v
}

func (v *Vec2) SubN(n float64) *Vec2 {
	v[X] -= n
	v[Y] -= n
	return v
}

func (v *Vec2) MulN(n float64) *Vec2 {
	v[X] *= n
	v[Y] *= n
	return v
}

func (v *Vec2) DivN(n float64) *Vec2 {
	v[X] /= n
	v[Y] /= n
	return v
}

func (v *Vec3) Add(v2 *Vec3) *Vec3 {
	v[X] += v2[X]
	v[Y] += v2[Y]
	v[Z] += v2[Z]
	return v
}

func (v *Vec3) Sub(v2 *Vec3) *Vec3 {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	v[Z] -= v2[Z]
	return v
}

func (v *Vec3) Mul(v2 *Vec3) *Vec3 {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	v[Z] *= v2[Z]
	return v
}

func (v *Vec3) Div(v2 *Vec3) *Vec3 {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	v[Z] /= v2[Z]
	return v
}

func (v *Vec3) AddN(n float64) *Vec3 {
	v[X] += n
	v[Y] += n
	v[Z] += n
	return v
}

func (v *Vec3) SubN(n float64) *Vec3 {
	v[X] -= n
	v[Y] -= n
	v[Z] -= n
	return v
}

func (v *Vec3) MulN(n float64) *Vec3 {
	v[X] *= n
	v[Y] *= n
	v[Z] *= n
	return v
}

func (v *Vec3) DivN(n float64) *Vec3 {
	v[X] /= n
	v[Y] /= n
	v[Z] /= n
	return v
}

func (v *Vec4) Add(v2 *Vec4) *Vec4 {
	v[X] += v2[X]
	v[Y] += v2[Y]
	v[Z] += v2[Z]
	v[W] += v2[W]
	return v
}

func (v *Vec4) Sub(v2 *Vec4) *Vec4 {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	v[Z] -= v2[Z]
	v[W] -= v2[W]
	return v
}

func (v *Vec4) Mul(v2 *Vec4) *Vec4 {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	v[Z] *= v2[Z]
	v[W] *= v2[W]
	return v
}

func (v *Vec4) Div(v2 *Vec4) *Vec4 {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	v[Z] /= v2[Z]
	v[W] /= v2[W]
	return v
}

func (v *Vec4) AddN(n float64) *Vec4 {
	v[X] += n
	v[Y] += n
	v[Z] += n
	v[W] += n
	return v
}

func (v *Vec4) SubN(n float64) *Vec4 {
	v[X] -= n
	v[Y] -= n
	v[Z] -= n
	v[W] -= n
	return v
}

func (v *Vec4) MulN(n float64) *Vec4 {
	v[X] *= n
	v[Y] *= n
	v[Z] *= n
	v[W] *= n
	return v
}

func (v *Vec4) DivN(n float64) *Vec4 {
	v[X] /= n
	v[Y] /= n
	v[Z] /= n
	v[W] /= n
	return v
}

func (v *Vec2) Neg() *Vec2 {
	v[X] = -v[X]
	v[Y] = -v[Y]
	return v
}

func (v *Vec3) Neg() *Vec3 {
	v[X] = -v[X]
	v[Y] = -v[Y]
	v[Z] = -v[Z]
	return v
}

func (v *Vec4) Neg() *Vec4 {
	v[X] = -v[X]
	v[Y] = -v[Y]
	v[Z] = -v[Z]
	v[W] = -v[W]
	return v
}

func (v *Vec2) Sign() *Vec2 {
	v[X] = math64.Sign(v[X])
	v[Y] = math64.Sign(v[Y])
	return v
}

func (v *Vec3) Sign() *Vec3 {
	v[X] = math64.Sign(v[X])
	v[Y] = math64.Sign(v[Y])
	v[Z] = math64.Sign(v[Z])
	return v
}

func (v *Vec4) Sign() *Vec4 {
	v[X] = math64.Sign(v[X])
	v[Y] = math64.Sign(v[Y])
	v[Z] = math64.Sign(v[Z])
	v[W] = math64.Sign(v[W])
	return v
}

func (v *Vec2) MaddN(v2 *Vec2, n float64) *Vec2 {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	return v
}

func (v *Vec3) MaddN(v2 *Vec3, n float64) *Vec3 {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	v[Z] += v2[Z] * n
	return v
}

func (v *Vec4) MaddN(v2 *Vec4, n float64) *Vec4 {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	v[Z] += v2[Z] * n
	v[W] += v2[W] * n
	return v
}

func (v *Vec2) MaddV(v2 *Vec2, n *Vec2) *Vec2 {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	return v
}

func (v *Vec3) MaddV(v2 *Vec3, n *Vec3) *Vec3 {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	v[Z] += v2[Z] * n[Z]
	return v
}

func (v *Vec4) MaddV(v2 *Vec4, n *Vec4) *Vec4 {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	v[Z] += v2[Z] * n[Z]
	v[W] += v2[W] * n[W]
	return v
}

func (v *Vec2) MsubN(v2 *Vec2, n float64) *Vec2 {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	return v
}

func (v *Vec3) MsubN(v2 *Vec3, n float64) *Vec3 {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	v[Z] -= v2[Z] * n
	return v
}

func (v *Vec4) MsubN(v2 *Vec4, n float64) *Vec4 {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	v[Z] -= v2[Z] * n
	v[W] -= v2[W] * n
	return v
}

func (v *Vec2) MsubV(v2 *Vec2, n *Vec2) *Vec2 {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	return v
}

func (v *Vec3) MsubV(v2 *Vec3, n *Vec3) *Vec3 {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	v[Z] -= v2[Z] * n[Z]
	return v
}

func (v *Vec4) MsubV(v2 *Vec4, n *Vec4) *Vec4 {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	v[Z] -= v2[Z] * n[Z]
	v[W] -= v2[W] * n[W]
	return v
}

// Sum returns sum of vector components
func (v *Vec2) Sum() float64 {
	return v[X] + v[Y]
}

// Sum returns sum of vector components
func (v *Vec3) Sum() float64 {
	return v[X] + v[Y] + v[Z]
}

// Sum returns sum of vector components
func (v *Vec4) Sum() float64 {
	return v[X] + v[Y] + v[Z] + v[W]
}

func (v *Vec2) Mix(v2 *Vec2, t float64) *Vec2 {
	v[X] += (v2[X] - v[X]) * t
	v[Y] += (v2[Y] - v[Y]) * t
	return v
}

func (v *Vec3) Mix(v2 *Vec3, t float64) *Vec3 {
	v[X] += (v2[X] - v[X]) * t
	v[Y] += (v2[Y] - v[Y]) * t
	v[Z] += (v2[Z] - v[Z]) * t
	return v
}

func (v *Vec4) Mix(v2 *Vec4, t float64) *Vec4 {
	v[X] += (v2[X] - v[X]) * t
	v[Y] += (v2[Y] - v[Y]) * t
	v[Z] += (v2[Z] - v[Z]) * t
	v[W] += (v2[W] - v[W]) * t
	return v
}

func (a *Vec2) MixBilinear(b, c, d *Vec2, u, v float64) *Vec2 {
	a[X] = math64.MixBilinear(a[X], b[X], c[X], d[X], u, v)
	a[Y] = math64.MixBilinear(a[Y], b[Y], c[Y], d[Y], u, v)
	return a
}

func (a *Vec3) MixBilinear(b, c, d *Vec3, u, v float64) *Vec3 {
	a[X] = math64.MixBilinear(a[X], b[X], c[X], d[X], u, v)
	a[Y] = math64.MixBilinear(a[Y], b[Y], c[Y], d[Y], u, v)
	a[Z] = math64.MixBilinear(a[Z], b[Z], c[Z], d[Z], u, v)
	return a
}

func (a *Vec4) MixBilinear(b, c, d *Vec4, u, v float64) *Vec4 {
	a[X] = math64.MixBilinear(a[X], b[X], c[X], d[X], u, v)
	a[Y] = math64.MixBilinear(a[Y], b[Y], c[Y], d[Y], u, v)
	a[Z] = math64.MixBilinear(a[Z], b[Z], c[Z], d[Z], u, v)
	a[W] = math64.MixBilinear(a[W], b[W], c[W], d[W], u, v)
	return a
}

// https://keithmaggio.wordpress.com/2011/02/15/math-magician-lerp-slerp-and-nlerp/
func (a *Vec2) Slerp(b *Vec2, t float64) *Vec2 {
	dot := math64.Clamp(a.Dot(b), -1.0, 1.0)
	theta := math.Acos(dot) * t
	r := b.Copy().MsubN(a, dot).Normalize(1)
	return a.MulN(math.Cos(theta)).MaddN(r, math.Sin(theta))
}

func (a *Vec3) Slerp(b *Vec3, t float64) *Vec3 {
	dot := math64.Clamp(a.Dot(b), -1.0, 1.0)
	theta := math.Acos(dot) * t
	r := b.Copy().MsubN(a, dot).Normalize(1)
	return a.MulN(math.Cos(theta)).MaddN(r, math.Sin(theta))
}

func (a *Vec4) Slerp(b *Vec4, t float64) *Vec4 {
	dot := math64.Clamp(a.Dot(b), -1.0, 1.0)
	theta := math.Acos(dot) * t
	r := b.Copy().MsubN(a, dot).Normalize(1)
	return a.MulN(math.Cos(theta)).MaddN(r, math.Sin(theta))
}

func (v *Vec2) Step(e *Vec2) *Vec2 {
	v[X] = math64.Step(e[X], v[X])
	v[Y] = math64.Step(e[Y], v[Y])
	return v
}

func (v *Vec3) Step(e *Vec3) *Vec3 {
	v[X] = math64.Step(e[X], v[X])
	v[Y] = math64.Step(e[Y], v[Y])
	v[Z] = math64.Step(e[Z], v[Z])
	return v
}

func (v *Vec4) Step(e *Vec4) *Vec4 {
	v[X] = math64.Step(e[X], v[X])
	v[Y] = math64.Step(e[Y], v[Y])
	v[Z] = math64.Step(e[Z], v[Z])
	v[W] = math64.Step(e[W], v[W])
	return v
}

func (v *Vec2) SmoothStep(e, e2 *Vec2) *Vec2 {
	v[X] = math64.SmoothStep(e[X], e2[X], v[X])
	v[Y] = math64.SmoothStep(e[Y], e2[Y], v[Y])
	return v
}

func (v *Vec3) SmoothStep(e, e2 *Vec3) *Vec3 {
	v[X] = math64.SmoothStep(e[X], e2[X], v[X])
	v[Y] = math64.SmoothStep(e[Y], e2[Y], v[Y])
	v[Z] = math64.SmoothStep(e[Z], e2[Z], v[Z])
	return v
}

func (v *Vec4) SmoothStep(e, e2 *Vec4) *Vec4 {
	v[X] = math64.SmoothStep(e[X], e2[X], v[X])
	v[Y] = math64.SmoothStep(e[Y], e2[Y], v[Y])
	v[Z] = math64.SmoothStep(e[Z], e2[Z], v[Z])
	v[W] = math64.SmoothStep(e[W], e2[W], v[W])
	return v
}

func (v *Vec2) SmootherStep(e, e2 *Vec2) *Vec2 {
	v[X] = math64.SmootherStep(e[X], e2[X], v[X])
	v[Y] = math64.SmootherStep(e[Y], e2[Y], v[Y])
	return v
}

func (v *Vec3) SmootherStep(e, e2 *Vec3) *Vec3 {
	v[X] = math64.SmootherStep(e[X], e2[X], v[X])
	v[Y] = math64.SmootherStep(e[Y], e2[Y], v[Y])
	v[Z] = math64.SmootherStep(e[Z], e2[Z], v[Z])
	return v
}

func (v *Vec4) SmootherStep(e, e2 *Vec4) *Vec4 {
	v[X] = math64.SmootherStep(e[X], e2[X], v[X])
	v[Y] = math64.SmootherStep(e[Y], e2[Y], v[Y])
	v[Z] = math64.SmootherStep(e[Z], e2[Z], v[Z])
	v[W] = math64.SmootherStep(e[W], e2[W], v[W])
	return v
}

func (v *Vec2) Abs() *Vec2 {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	return v
}

func (v *Vec3) Abs() *Vec3 {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	if v[Z] < 0 {
		v[Z] = -v[Z]
	}
	return v
}

func (v *Vec4) Abs() *Vec4 {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	if v[Z] < 0 {
		v[Z] = -v[Z]
	}
	if v[W] < 0 {
		v[W] = -v[W]
	}
	return v
}

func (v *Vec2) Dot(v2 *Vec2) float64 {
	return v[X]*v2[X] + v[Y]*v2[Y]
}

func (v *Vec3) Dot(v2 *Vec3) float64 {
	return v[X]*v2[X] + v[Y]*v2[Y] + v[Z]*v2[Z]
}

func (v *Vec4) Dot(v2 *Vec4) float64 {
	return v[X]*v2[X] + v[Y]*v2[Y] + v[Z]*v2[Z] + v[W]*v2[W]
}

func (v *Vec2) Cross(v2 *Vec2) float64 {
	return v[X]*v2[Y] - v[Y]*v2[X]
}

func (v *Vec3) Cross(v2 *Vec3) *Vec3 {
	x := v[Y]*v2[Z] - v[Z]*v2[Y]
	y := v[Z]*v2[X] - v[Z]*v2[X]
	v[Z] = v[X]*v2[Y] - v[Y]*v2[X]
	v[Y] = y
	v[X] = x
	return v
}

func (v *Vec4) Cross(v2 *Vec4) *Vec4 {
	x := v[Y]*v2[Z] - v[Z]*v2[Y]
	y := v[Z]*v2[X] - v[Z]*v2[X]
	v[Z] = v[X]*v2[Y] - v[Y]*v2[X]
	v[Y] = y
	v[X] = x
	return v
}

func (v *Vec2) IsColinear(v2 *Vec2, eps float64) bool {
	return math64.EqDelta(v.Cross(v2), 0, eps)
}

func (v *Vec3) IsColinear(v2 *Vec3, eps float64) bool {
	return math64.EqDelta(v.Copy().Cross(v2).MagSq(), 0, eps*eps)
}

func (a *Vec3) OrthoNormal(b, c *Vec3) *Vec3 {
	ba := &Vec3{b[X] - a[X], b[Y] - a[Y], b[Z] - a[Z]}
	ca := &Vec3{c[X] - a[X], c[Y] - a[Y], c[Z] - a[Z]}
	return ca.Cross(ba)
}

func (v *Vec3) Plane() (*Vec3, *Vec3) {
	if math.Abs(v[Z]) > math64.Sqrt2 {
		d := v[Y]*v[Y] + v[Z]*v[Z]
		k := 1 / math.Sqrt(d)
		p := &Vec3{0, -v[Z] * k, v[Y] * k}
		return p, &Vec3{d * k, -v[X] * p[Z], v[X] * p[Y]}
	} else {
		d := v[X]*v[X] + v[Y]*v[Y]
		k := 1 / math.Sqrt(d)
		p := &Vec3{-v[Y] * k, v[X] * k, 0}
		return p, &Vec3{-v[Z] * p[Y], v[Z] * p[X], d * k}
	}
}

func (v *Vec2) Mag() float64 {
	return math.Sqrt(v.MagSq())
}

func (v *Vec2) MagSq() float64 {
	return v[X]*v[X] + v[Y]*v[Y]
}

func (v *Vec3) Mag() float64 {
	return math.Sqrt(v.MagSq())
}

func (v *Vec3) MagSq() float64 {
	return v[X]*v[X] + v[Y]*v[Y] + v[Z]*v[Z]
}

func (v *Vec4) Mag() float64 {
	return math.Sqrt(v.MagSq())
}

func (v *Vec4) MagSq() float64 {
	return v[X]*v[X] + v[Y]*v[Y] + v[Z]*v[Z] + v[W]*v[W]
}

func (v *Vec2) Normalize(n float64) *Vec2 {
	m := v.Mag()
	if m > 0 {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec3) Normalize(n float64) *Vec3 {
	m := v.Mag()
	if m > 0 {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec4) Normalize(n float64) *Vec4 {
	m := v.Mag()
	if m > 0 {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec2) Limit(n float64) *Vec2 {
	m := v.Mag()
	if m > n {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec3) Limit(n float64) *Vec3 {
	m := v.Mag()
	if m > n {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec4) Limit(n float64) *Vec4 {
	m := v.Mag()
	if m > n {
		return v.MulN(n / m)
	}
	return v
}

func (v *Vec2) Dist(v2 *Vec2) float64 {
	return math.Sqrt(v.DistSq(v2))
}

func (v *Vec3) Dist(v2 *Vec3) float64 {
	return math.Sqrt(v.DistSq(v2))
}

func (v *Vec2) DistSq(v2 *Vec2) float64 {
	dx := v[X] - v2[X]
	dy := v[Y] - v2[Y]
	return dx*dx + dy*dy
}

func (v *Vec3) DistSq(v2 *Vec3) float64 {
	dx := v[X] - v2[X]
	dy := v[Y] - v2[Y]
	dz := v[Z] - v2[Z]
	return dx*dx + dy*dy + dz*dz
}

func (v *Vec2) DistManhattan(v2 *Vec2) float64 {
	return math64.AbsDiff(v[X], v2[X]) +
		math64.AbsDiff(v[Y], v2[Y])
}

func (v *Vec3) DistManhattan(v2 *Vec3) float64 {
	return math64.AbsDiff(v[X], v2[X]) +
		math64.AbsDiff(v[Y], v2[Y]) +
		math64.AbsDiff(v[Z], v2[Z])
}

func (v *Vec2) DistChebychev(v2 *Vec2) float64 {
	return math.Max(
		math64.AbsDiff(v[X], v2[X]),
		math64.AbsDiff(v[Y], v2[Y]))
}

func (v *Vec3) DistChebychev(v2 *Vec3) float64 {
	return math64.Max3(
		math64.AbsDiff(v[X], v2[X]),
		math64.AbsDiff(v[Y], v2[Y]),
		math64.AbsDiff(v[Z], v2[Z]))
}

func (v *Vec2) PerpendicularLeft() *Vec2 {
	x := v[X]
	v[X] = -v[Y]
	v[Y] = x
	return v
}

func (v *Vec2) PerpendicularRight() *Vec2 {
	x := -v[X]
	v[X] = v[Y]
	v[Y] = x
	return v
}

// Reflect calculates the reflection direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
func (v *Vec2) Reflect(n *Vec2) *Vec2 {
	return v.MsubN(n, 2*v.Dot(n))
}

// Reflect calculates the reflection direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
func (v *Vec3) Reflect(n *Vec3) *Vec3 {
	return v.MsubN(n, 2*v.Dot(n))
}

// Reflect calculates the reflection direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
func (v *Vec4) Reflect(n *Vec4) *Vec4 {
	return v.MsubN(n, 2*v.Dot(n))
}

// Refract computes the refraction direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
func (v *Vec2) Refract(n *Vec2, eta float64) *Vec2 {
	if eta == 1.0 {
		return v.Reflect(n)
	}
	d := v.Dot(n)
	k := 1 - eta*eta*(1-d*d)
	if k < 0 {
		return v.Zero()
	}
	k = eta*d + math.Sqrt(k)
	v[X] = v[X]*eta - n[X]*k
	v[Y] = v[Y]*eta - n[Y]*k
	return v
}

// Refract computes the refraction direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
func (v *Vec3) Refract(n *Vec3, eta float64) *Vec3 {
	if eta == 1.0 {
		return v.Reflect(n)
	}
	d := v.Dot(n)
	k := 1 - eta*eta*(1-d*d)
	if k < 0 {
		return v.Zero()
	}
	k = eta*d + math.Sqrt(k)
	v[X] = v[X]*eta - n[X]*k
	v[Y] = v[Y]*eta - n[Y]*k
	v[Z] = v[Z]*eta - n[Z]*k
	return v
}

// Refract computes the refraction direction for an incident vector.
// Based on: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
func (v *Vec4) Refract(n *Vec4, eta float64) *Vec4 {
	if eta == 1.0 {
		return v.Reflect(n)
	}
	d := v.Dot(n)
	k := 1 - eta*eta*(1-d*d)
	if k < 0 {
		return v.Zero()
	}
	k = eta*d + math.Sqrt(k)
	v[X] = v[X]*eta - n[X]*k
	v[Y] = v[Y]*eta - n[Y]*k
	v[Z] = v[Z]*eta - n[Z]*k
	v[W] = v[W]*eta - n[W]*k
	return v
}

func (v *Vec2) Heading() float64 {
	return math64.Atan2Abs(v[Y], v[X])
}

func (v *Vec3) HeadingXY() float64 {
	return math64.Atan2Abs(v[Y], v[X])
}

func (v *Vec3) HeadingXZ() float64 {
	return math64.Atan2Abs(v[Z], v[X])
}

func (v *Vec3) HeadingYZ() float64 {
	return math64.Atan2Abs(v[Z], v[Y])
}

func (v *Vec4) HeadingXY() float64 {
	return math64.Atan2Abs(v[Y], v[X])
}

func (v *Vec4) HeadingXZ() float64 {
	return math64.Atan2Abs(v[Z], v[X])
}

func (v *Vec4) HeadingYZ() float64 {
	return math64.Atan2Abs(v[Z], v[Y])
}

func (v *Vec2) Polar() *Vec2 {
	theta := v.Heading()
	v[X] = v.Mag()
	v[Y] = theta
	return v
}

func (v *Vec3) Polar() *Vec3 {
	r := v.Mag()
	z := v[Z]
	v[Z] = math.Atan2(v[Y], v[X])
	v[Y] = math.Asin(z / r)
	v[X] = r
	return v
}

func (v *Vec2) Cartesian() *Vec2 {
	r := v[X]
	theta := v[Y]
	v[X] = r * math.Cos(theta)
	v[Y] = r * math.Sin(theta)
	return v
}

func (v *Vec3) Cartesian() *Vec3 {
	r, y, z := v.Elem()
	rcos := r * math.Acos(y)
	v[X] = rcos * math.Cos(z)
	v[Y] = rcos * math.Sin(z)
	v[Z] = r * math.Sin(y)
	return v
}

func (v *Vec2) Rotate(theta float64) *Vec2 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	x, y := v.Elem()
	v[X] = x*c - y*s
	v[Y] = x*s + y*c
	return v
}

// RotateAround applies vector rotation around given point p, i.e. uses
// p as rotation center
func (v *Vec2) RotateAround(p *Vec2, theta float64) *Vec2 {
	s := math.Sin(theta)
	c := math.Cos(theta)
	x, y := v.Elem()
	px, py := p.Elem()
	x -= px
	y -= py
	v[X] = x*c - y*s + px
	v[Y] = x*s + y*c + py
	return v
}

func (v *Vec2) Min(v2 *Vec2) *Vec2 {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	return v
}

func (v *Vec2) Max(v2 *Vec2) *Vec2 {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	return v
}

func (v *Vec3) Min(v2 *Vec3) *Vec3 {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] < v[Z] {
		v[Z] = v2[Z]
	}
	return v
}

func (v *Vec3) Max(v2 *Vec3) *Vec3 {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] > v[Z] {
		v[Z] = v2[Z]
	}
	return v
}

func (v *Vec4) Min(v2 *Vec4) *Vec4 {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] < v[Z] {
		v[Z] = v2[Z]
	}
	if v2[W] < v[W] {
		v[W] = v2[W]
	}
	return v
}

func (v *Vec4) Max(v2 *Vec4) *Vec4 {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] > v[Z] {
		v[Z] = v2[Z]
	}
	if v2[W] > v[W] {
		v[W] = v2[W]
	}
	return v
}

func (v *Vec2) Clamp(min, max *Vec2) *Vec2 {
	return v.Min(max).Max(min)
}

func (v *Vec3) Clamp(min, max *Vec3) *Vec3 {
	return v.Min(max).Max(min)
}

func (v *Vec4) Clamp(min, max *Vec4) *Vec4 {
	return v.Min(max).Max(min)
}

func (v *Vec2) Floor() *Vec2 {
	v[X] = math.Floor(v[X])
	v[Y] = math.Floor(v[Y])
	return v
}

func (v *Vec3) Floor() *Vec3 {
	v[X] = math.Floor(v[X])
	v[Y] = math.Floor(v[Y])
	v[Z] = math.Floor(v[Z])
	return v
}

func (v *Vec4) Floor() *Vec4 {
	v[X] = math.Floor(v[X])
	v[Y] = math.Floor(v[Y])
	v[Z] = math.Floor(v[Z])
	v[W] = math.Floor(v[W])
	return v
}

func (v *Vec2) Ceil() *Vec2 {
	v[X] = math.Ceil(v[X])
	v[Y] = math.Ceil(v[Y])
	return v
}

func (v *Vec3) Ceil() *Vec3 {
	v[X] = math.Ceil(v[X])
	v[Y] = math.Ceil(v[Y])
	v[Z] = math.Ceil(v[Z])
	return v
}

func (v *Vec4) Ceil() *Vec4 {
	v[X] = math.Ceil(v[X])
	v[Y] = math.Ceil(v[Y])
	v[Z] = math.Ceil(v[Z])
	v[W] = math.Ceil(v[W])
	return v
}

func (v *Vec2) Mod(v2 *Vec2) *Vec2 {
	v[X] = math64.Mod(v[X], v2[X])
	v[Y] = math64.Mod(v[Y], v2[Y])
	return v
}

func (v *Vec3) Mod(v2 *Vec3) *Vec3 {
	v[X] = math64.Mod(v[X], v2[X])
	v[Y] = math64.Mod(v[Y], v2[Y])
	v[Z] = math64.Mod(v[Z], v2[Z])
	return v
}

func (v *Vec4) Mod(v2 *Vec4) *Vec4 {
	v[X] = math64.Mod(v[X], v2[X])
	v[Y] = math64.Mod(v[Y], v2[Y])
	v[Z] = math64.Mod(v[Z], v2[Z])
	v[W] = math64.Mod(v[W], v2[W])
	return v
}

func (v *Vec2) MajorAxis() uint {
	if math.Abs(v[X]) >= math.Abs(v[Y]) {
		return X
	}
	return Y
}

func (v *Vec2) MinorAxis() uint {
	if math.Abs(v[X]) <= math.Abs(v[Y]) {
		return X
	}
	return Y
}

func (v *Vec3) MajorAxis() uint {
	x := math.Abs(v[X])
	y := math.Abs(v[Y])
	z := math.Abs(v[Z])
	if x >= y {
		if x >= z {
			return X
		}
		return Z
	}
	if x >= z {
		if x >= y {
			return X
		}
		return Y
	}
	if y >= z {
		return Y
	}
	return Z
}

func (v *Vec3) MinorAxis() uint {
	x := math.Abs(v[X])
	y := math.Abs(v[Y])
	z := math.Abs(v[Z])
	if x <= y {
		if x <= z {
			return X
		}
		return Z
	}
	if x <= z {
		if x <= y {
			return X
		}
		return Y
	}
	if y <= z {
		return Y
	}
	return Z
}

func (v *Vec2) Eq(v2 *Vec2) bool {
	return v[X] == v2[X] && v[Y] == v2[Y]
}

func (v *Vec3) Eq(v2 *Vec3) bool {
	return v[X] == v2[X] && v[Y] == v2[Y] && v[Z] == v2[Z]
}

func (v *Vec4) Eq(v2 *Vec4) bool {
	return v[X] == v2[X] && v[Y] == v2[Y] && v[Z] == v2[Z] && v[W] == v2[W]
}

func (v *Vec2) EqDelta(v2 *Vec2, eps float64) bool {
	return math64.EqDelta(v[X], v2[X], eps) &&
		math64.EqDelta(v[Y], v2[Y], eps)
}

func (v *Vec3) EqDelta(v2 *Vec3, eps float64) bool {
	return math64.EqDelta(v[X], v2[X], eps) &&
		math64.EqDelta(v[Y], v2[Y], eps) &&
		math64.EqDelta(v[Z], v2[Z], eps)
}

func (v *Vec4) EqDelta(v2 *Vec4, eps float64) bool {
	return math64.EqDelta(v[X], v2[X], eps) &&
		math64.EqDelta(v[Y], v2[Y], eps) &&
		math64.EqDelta(v[Z], v2[Z], eps) &&
		math64.EqDelta(v[W], v2[W], eps)
}

func (v *Vec2) CompareBy(v2 *Vec2, order Order2) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXY
	}
	x, y := order>>2&1, order&1
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			return 0
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec3) CompareBy(v2 *Vec3, order Order3) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXYZ
	}
	x, y, z := order>>4&3, order>>2&3, order&3
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			if v[z] == v2[z] {
				return 0
			} else if v[z] < v2[z] {
				return -3
			}
			return 3
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec4) CompareBy(v2 *Vec4, order Order4) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXYZW
	}
	x, y, z, w := order>>6&3, order>>4&3, order>>2&3, order&3
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			if v[z] == v2[z] {
				if v[w] == v2[w] {
					return 0
				} else if v[w] < v2[w] {
					return -4
				}
				return 4
			} else if v[z] < v2[z] {
				return -3
			}
			return 3
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec2) String() string {
	return fmt.Sprintf("[%.4f, %.4f]", v[X], v[Y])
}

func (v *Vec3) String() string {
	return fmt.Sprintf("[%.4f, %.4f, %.4f]", v[X], v[Y], v[Z])
}

func (v *Vec4) String() string {
	return fmt.Sprintf("[%.4f, %.4f, %.4f, %.4f]", v[X], v[Y], v[Z], v[W])
}

func (v *Vec2) GoString() string {
	return fmt.Sprintf("vec.Vec2{%#v, %#v}", v[X], v[Y])
}

func (v *Vec3) GoString() string {
	return fmt.Sprintf("vec.Vec3{%#v, %#v, %#v}", v[X], v[Y], v[Z])
}

func (v *Vec4) GoString() string {
	return fmt.Sprintf("vec.Vec4{%#v, %#v, %#v, %#v}", v[X], v[Y], v[Z], v[W])
}
