package vec

import (
	"fmt"

	"go.thi.ng/geom/math32"
	"go.thi.ng/geom/math32i"
)

type (
	Vec2i [2]int32
	Vec3i [3]int32
	Vec4i [4]int32
)

// Axis / vector component constants

func NewVec2i(x, y int32) *Vec2i {
	return &Vec2i{x, y}
}

func NewVec3i(x, y, z int32) *Vec3i {
	return &Vec3i{x, y, z}
}

func NewVec4i(x, y, z, w int32) *Vec4i {
	return &Vec4i{x, y, z, w}
}

func RandVec2i(n int32) *Vec2i {
	return &Vec2i{
		math32i.RandNorm(n),
		math32i.RandNorm(n)}
}

func RandVec3i(n int32) *Vec3i {
	return &Vec3i{
		math32i.RandNorm(n),
		math32i.RandNorm(n),
		math32i.RandNorm(n)}
}

func RandVec4i(n int32) *Vec4i {
	return &Vec4i{
		math32i.RandNorm(n),
		math32i.RandNorm(n),
		math32i.RandNorm(n),
		math32i.RandNorm(n)}
}

func NewVec2iFrom(buf []int32, idx int) *Vec2i {
	return &Vec2i{buf[idx], buf[idx+Y]}
}

func NewVec3iFrom(buf []int32, idx int) *Vec3i {
	return &Vec3i{buf[idx], buf[idx+Y], buf[idx+Z]}
}

func NewVec4iFrom(buf []int32, idx int) *Vec4i {
	return &Vec4i{buf[idx], buf[idx+Y], buf[idx+Z], buf[idx+W]}
}

func (v *Vec2i) Into(buf []int32, idx int) []int32 {
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec3i) Into(buf []int32, idx int) []int32 {
	buf[idx+Z] = v[Z]
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec4i) Into(buf []int32, idx int) []int32 {
	buf[idx+W] = v[W]
	buf[idx+Z] = v[Z]
	buf[idx+Y] = v[Y]
	buf[idx] = v[X]
	return buf
}

func (v *Vec2i) AsVec2f() *Vec2f {
	return NewVec2f(float32(v[X]), float32(v[Y]))
}

func (v *Vec3i) AsVec3f() *Vec3f {
	return NewVec3f(float32(v[X]), float32(v[Y]), float32(v[Z]))
}

func (v *Vec4i) AsVec4f() *Vec4f {
	return NewVec4f(float32(v[X]), float32(v[Y]), float32(v[Z]), float32(v[W]))
}

func (v *Vec2i) Set(v2 *Vec2i) *Vec2i {
	v[X] = v2[X]
	v[Y] = v2[Y]
	return v
}

func (v *Vec3i) Set(v2 *Vec3i) *Vec3i {
	v[X] = v2[X]
	v[Y] = v2[Y]
	v[Z] = v2[Z]
	return v
}

func (v *Vec4i) Set(v2 *Vec4i) *Vec4i {
	v[X] = v2[X]
	v[Y] = v2[Y]
	v[Z] = v2[Z]
	v[W] = v2[W]
	return v
}

func (v *Vec2i) SetN(n int32) *Vec2i {
	v[X] = n
	v[Y] = n
	return v
}

func (v *Vec3i) SetN(n int32) *Vec3i {
	v[X] = n
	v[Y] = n
	v[Z] = n
	return v
}

func (v *Vec4i) SetN(n int32) *Vec4i {
	v[X] = n
	v[Y] = n
	v[Z] = n
	v[W] = n
	return v
}

func (v *Vec2i) Copy() *Vec2i {
	return &Vec2i{v[X], v[Y]}
}

func (v *Vec3i) Copy() *Vec3i {
	return &Vec3i{v[X], v[Y], v[Z]}
}

func (v *Vec4i) Copy() *Vec4i {
	return &Vec4i{v[X], v[Y], v[Z], v[W]}
}

// Zero is shorthand for v.SetN(0)
func (v *Vec2i) Zero() *Vec2i {
	return v.SetN(0)
}

// Zero is shorthand for v.SetN(0)
func (v *Vec3i) Zero() *Vec3i {
	return v.SetN(0)
}

// Zero is shorthand for v.SetN(0)
func (v *Vec4i) Zero() *Vec4i {
	return v.SetN(0)
}

// One is shorthand for v.SetN(1)
func (v *Vec2i) One() *Vec2i {
	return v.SetN(1)
}

// One is shorthand for v.SetN(1)
func (v *Vec3i) One() *Vec3i {
	return v.SetN(1)
}

// One is shorthand for v.SetN(1)
func (v *Vec4i) One() *Vec4i {
	return v.SetN(1)
}

func (v *Vec2i) IsZero() bool {
	return v[X] == 0 && v[Y] == 0
}

func (v *Vec3i) IsZero() bool {
	return v[X] == 0 && v[Y] == 0 && v[Z] == 0
}

func (v *Vec4i) IsZero() bool {
	return v[X] == 0 && v[Y] == 0 && v[Z] == 0 && v[W] == 0
}

func (v *Vec2i) Add(v2 *Vec2i) *Vec2i {
	v[X] += v2[X]
	v[Y] += v2[Y]
	return v
}

func (v *Vec2i) Sub(v2 *Vec2i) *Vec2i {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	return v
}

func (v *Vec2i) Mul(v2 *Vec2i) *Vec2i {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	return v
}

func (v *Vec2i) Div(v2 *Vec2i) *Vec2i {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	return v
}

func (v *Vec2i) AddN(n int32) *Vec2i {
	v[X] += n
	v[Y] += n
	return v
}

func (v *Vec2i) SubN(n int32) *Vec2i {
	v[X] -= n
	v[Y] -= n
	return v
}

func (v *Vec2i) MulN(n int32) *Vec2i {
	v[X] *= n
	v[Y] *= n
	return v
}

func (v *Vec2i) DivN(n int32) *Vec2i {
	v[X] /= n
	v[Y] /= n
	return v
}

func (v *Vec3i) Add(v2 *Vec3i) *Vec3i {
	v[X] += v2[X]
	v[Y] += v2[Y]
	v[Z] += v2[Z]
	return v
}

func (v *Vec3i) Sub(v2 *Vec3i) *Vec3i {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	v[Z] -= v2[Z]
	return v
}

func (v *Vec3i) Mul(v2 *Vec3i) *Vec3i {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	v[Z] *= v2[Z]
	return v
}

func (v *Vec3i) Div(v2 *Vec3i) *Vec3i {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	v[Z] /= v2[Z]
	return v
}

func (v *Vec3i) AddN(n int32) *Vec3i {
	v[X] += n
	v[Y] += n
	v[Z] += n
	return v
}

func (v *Vec3i) SubN(n int32) *Vec3i {
	v[X] -= n
	v[Y] -= n
	v[Z] -= n
	return v
}

func (v *Vec3i) MulN(n int32) *Vec3i {
	v[X] *= n
	v[Y] *= n
	v[Z] *= n
	return v
}

func (v *Vec3i) DivN(n int32) *Vec3i {
	v[X] /= n
	v[Y] /= n
	v[Z] /= n
	return v
}

func (v *Vec4i) Add(v2 *Vec4i) *Vec4i {
	v[X] += v2[X]
	v[Y] += v2[Y]
	v[Z] += v2[Z]
	v[W] += v2[W]
	return v
}

func (v *Vec4i) Sub(v2 *Vec4i) *Vec4i {
	v[X] -= v2[X]
	v[Y] -= v2[Y]
	v[Z] -= v2[Z]
	v[W] -= v2[W]
	return v
}

func (v *Vec4i) Mul(v2 *Vec4i) *Vec4i {
	v[X] *= v2[X]
	v[Y] *= v2[Y]
	v[Z] *= v2[Z]
	v[W] *= v2[W]
	return v
}

func (v *Vec4i) Div(v2 *Vec4i) *Vec4i {
	v[X] /= v2[X]
	v[Y] /= v2[Y]
	v[Z] /= v2[Z]
	v[W] /= v2[W]
	return v
}

func (v *Vec4i) AddN(n int32) *Vec4i {
	v[X] += n
	v[Y] += n
	v[Z] += n
	v[W] += n
	return v
}

func (v *Vec4i) SubN(n int32) *Vec4i {
	v[X] -= n
	v[Y] -= n
	v[Z] -= n
	v[W] -= n
	return v
}

func (v *Vec4i) MulN(n int32) *Vec4i {
	v[X] *= n
	v[Y] *= n
	v[Z] *= n
	v[W] *= n
	return v
}

func (v *Vec4i) DivN(n int32) *Vec4i {
	v[X] /= n
	v[Y] /= n
	v[Z] /= n
	v[W] /= n
	return v
}

func (v *Vec2i) Neg() *Vec2i {
	v[X] = -v[X]
	v[Y] = -v[Y]
	return v
}

func (v *Vec3i) Neg() *Vec3i {
	v[X] = -v[X]
	v[Y] = -v[Y]
	v[Z] = -v[Z]
	return v
}

func (v *Vec4i) Neg() *Vec4i {
	v[X] = -v[X]
	v[Y] = -v[Y]
	v[Z] = -v[Z]
	v[W] = -v[W]
	return v
}

func (v *Vec2i) Sign() *Vec2i {
	v[X] = math32i.Sign(v[X])
	v[Y] = math32i.Sign(v[Y])
	return v
}

func (v *Vec3i) Sign() *Vec3i {
	v[X] = math32i.Sign(v[X])
	v[Y] = math32i.Sign(v[Y])
	v[Z] = math32i.Sign(v[Z])
	return v
}

func (v *Vec4i) Sign() *Vec4i {
	v[X] = math32i.Sign(v[X])
	v[Y] = math32i.Sign(v[Y])
	v[Z] = math32i.Sign(v[Z])
	v[W] = math32i.Sign(v[W])
	return v
}

func (v *Vec2i) MaddN(v2 *Vec2i, n int32) *Vec2i {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	return v
}

func (v *Vec3i) MaddN(v2 *Vec3i, n int32) *Vec3i {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	v[Z] += v2[Z] * n
	return v
}

func (v *Vec4i) MaddN(v2 *Vec4i, n int32) *Vec4i {
	v[X] += v2[X] * n
	v[Y] += v2[Y] * n
	v[Z] += v2[Z] * n
	v[W] += v2[W] * n
	return v
}

func (v *Vec2i) MaddV(v2 *Vec2i, n *Vec2i) *Vec2i {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	return v
}

func (v *Vec3i) MaddV(v2 *Vec3i, n *Vec3i) *Vec3i {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	v[Z] += v2[Z] * n[Z]
	return v
}

func (v *Vec4i) MaddV(v2 *Vec4i, n *Vec4i) *Vec4i {
	v[X] += v2[X] * n[X]
	v[Y] += v2[Y] * n[Y]
	v[Z] += v2[Z] * n[Z]
	v[W] += v2[W] * n[W]
	return v
}

func (v *Vec2i) MsubN(v2 *Vec2i, n int32) *Vec2i {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	return v
}

func (v *Vec3i) MsubN(v2 *Vec3i, n int32) *Vec3i {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	v[Z] -= v2[Z] * n
	return v
}

func (v *Vec4i) MsubN(v2 *Vec4i, n int32) *Vec4i {
	v[X] -= v2[X] * n
	v[Y] -= v2[Y] * n
	v[Z] -= v2[Z] * n
	v[W] -= v2[W] * n
	return v
}

func (v *Vec2i) MsubV(v2 *Vec2i, n *Vec2i) *Vec2i {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	return v
}

func (v *Vec3i) MsubV(v2 *Vec3i, n *Vec3i) *Vec3i {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	v[Z] -= v2[Z] * n[Z]
	return v
}

func (v *Vec4i) MsubV(v2 *Vec4i, n *Vec4i) *Vec4i {
	v[X] -= v2[X] * n[X]
	v[Y] -= v2[Y] * n[Y]
	v[Z] -= v2[Z] * n[Z]
	v[W] -= v2[W] * n[W]
	return v
}

// Sum returns sum of vector components
func (v *Vec2i) Sum() int32 {
	return v[X] + v[Y]
}

// Sum returns sum of vector components
func (v *Vec3i) Sum() int32 {
	return v[X] + v[Y] + v[Z]
}

// Sum returns sum of vector components
func (v *Vec4i) Sum() int32 {
	return v[X] + v[Y] + v[Z] + v[W]
}

func (v *Vec2i) Abs() *Vec2i {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	return v
}

func (v *Vec3i) Abs() *Vec3i {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	if v[Z] < 0 {
		v[Z] = -v[Z]
	}
	return v
}

func (v *Vec4i) Abs() *Vec4i {
	if v[X] < 0 {
		v[X] = -v[X]
	}
	if v[Y] < 0 {
		v[Y] = -v[Y]
	}
	if v[Z] < 0 {
		v[Z] = -v[Z]
	}
	if v[W] < 0 {
		v[W] = -v[W]
	}
	return v
}

func (v *Vec2i) Dot(v2 *Vec2i) int32 {
	return v[X]*v2[X] + v[Y]*v2[Y]
}

func (v *Vec3i) Dot(v2 *Vec3i) int32 {
	return v[X]*v2[X] + v[Y]*v2[Y] + v[Z]*v2[Z]
}

func (v *Vec4i) Dot(v2 *Vec4i) int32 {
	return v[X]*v2[X] + v[Y]*v2[Y] + v[Z]*v2[Z] + v[W]*v2[W]
}

func (v *Vec2i) Cross(v2 *Vec2i) int32 {
	return v[X]*v2[Y] - v[Y]*v2[X]
}

func (v *Vec3i) Cross(v2 *Vec3i) *Vec3i {
	x := v[Y]*v2[Z] - v[Z]*v2[Y]
	y := v[Z]*v2[X] - v[Z]*v2[X]
	v[Z] = v[X]*v2[Y] - v[Y]*v2[X]
	v[Y] = y
	v[X] = x
	return v
}

func (v *Vec4i) Cross(v2 *Vec4i) *Vec4i {
	x := v[Y]*v2[Z] - v[Z]*v2[Y]
	y := v[Z]*v2[X] - v[Z]*v2[X]
	v[Z] = v[X]*v2[Y] - v[Y]*v2[X]
	v[Y] = y
	v[X] = x
	return v
}

func (v *Vec2i) IsColinear(v2 *Vec2i, eps int32) bool {
	return math32i.EqDelta(v.Cross(v2), 0, eps)
}

func (v *Vec3i) IsColinear(v2 *Vec3i, eps int32) bool {
	return math32i.EqDelta(v.Copy().Cross(v2).MagSq(), 0, eps*eps)
}

func OrthoNormal3i(a, b, c *Vec3i) *Vec3i {
	ba := &Vec3i{b[X] - a[X], b[Y] - a[Y], b[Z] - a[Z]}
	ca := &Vec3i{c[X] - a[X], c[Y] - a[Y], c[Z] - a[Z]}
	return ca.Cross(ba)
}

func (v *Vec2i) Mag() float32 {
	return math32.Sqrt(float32(v.MagSq()))
}

func (v *Vec2i) MagSq() int32 {
	return v[X]*v[X] + v[Y]*v[Y]
}

func (v *Vec3i) Mag() float32 {
	return math32.Sqrt(float32(v.MagSq()))
}

func (v *Vec3i) MagSq() int32 {
	return v[X]*v[X] + v[Y]*v[Y] + v[Z]*v[Z]
}

func (v *Vec4i) Mag() float32 {
	return math32.Sqrt(float32(v.MagSq()))
}

func (v *Vec4i) MagSq() int32 {
	return v[X]*v[X] + v[Y]*v[Y] + v[Z]*v[Z] + v[W]*v[W]
}

func (v *Vec2i) Normalize(n float32) *Vec2f {
	return v.AsVec2f().Normalize(n)
}

func (v *Vec3i) Normalize(n float32) *Vec3f {
	return v.AsVec3f().Normalize(n)
}

func (v *Vec4i) Normalize(n float32) *Vec4f {
	return v.AsVec4f().Normalize(n)
}

func (v *Vec2i) Dist(v2 *Vec2i) float32 {
	return math32.Sqrt(float32(v.DistSq(v2)))
}

func (v *Vec3i) Dist(v2 *Vec3i) float32 {
	return math32.Sqrt(float32(v.DistSq(v2)))
}

func (v *Vec2i) DistSq(v2 *Vec2i) int32 {
	dx := v[X] - v2[X]
	dy := v[Y] - v2[Y]
	return dx*dx + dy*dy
}

func (v *Vec3i) DistSq(v2 *Vec3i) int32 {
	dx := v[X] - v2[X]
	dy := v[Y] - v2[Y]
	dz := v[Z] - v2[Z]
	return dx*dx + dy*dy + dz*dz
}

func (v *Vec2i) DistManhattan(v2 *Vec2i) int32 {
	return math32i.AbsDiff(v[X], v2[X]) +
		math32i.AbsDiff(v[Y], v2[Y])
}

func (v *Vec3i) DistManhattan(v2 *Vec3i) int32 {
	return math32i.AbsDiff(v[X], v2[X]) +
		math32i.AbsDiff(v[Y], v2[Y]) +
		math32i.AbsDiff(v[Z], v2[Z])
}

func (v *Vec2i) DistChebychev(v2 *Vec2i) int32 {
	return math32i.Max(
		math32i.AbsDiff(v[X], v2[X]),
		math32i.AbsDiff(v[Y], v2[Y]))
}

func (v *Vec3i) DistChebychev(v2 *Vec3i) int32 {
	return math32i.Max3(
		math32i.AbsDiff(v[X], v2[X]),
		math32i.AbsDiff(v[Y], v2[Y]),
		math32i.AbsDiff(v[Z], v2[Z]))
}

func (v *Vec2i) PerpendicularLeft() *Vec2i {
	x := v[X]
	v[X] = -v[Y]
	v[Y] = x
	return v
}

func (v *Vec2i) PerpendicularRight() *Vec2i {
	x := -v[X]
	v[X] = v[Y]
	v[Y] = x
	return v
}

func (v *Vec2i) Heading() float32 {
	return math32i.Atan2Abs(v[Y], v[X])
}

func (v *Vec3i) HeadingXY() float32 {
	return math32i.Atan2Abs(v[Y], v[X])
}

func (v *Vec3i) HeadingXZ() float32 {
	return math32i.Atan2Abs(v[Z], v[X])
}

func (v *Vec3i) HeadingYZ() float32 {
	return math32i.Atan2Abs(v[Z], v[Y])
}

func (v *Vec4i) HeadingXY() float32 {
	return math32i.Atan2Abs(v[Y], v[X])
}

func (v *Vec4i) HeadingXZ() float32 {
	return math32i.Atan2Abs(v[Z], v[X])
}

func (v *Vec4i) HeadingYZ() float32 {
	return math32i.Atan2Abs(v[Z], v[Y])
}

func (v *Vec2i) Min(v2 *Vec2i) *Vec2i {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	return v
}

func (v *Vec2i) Max(v2 *Vec2i) *Vec2i {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	return v
}

func (v *Vec3i) Min(v2 *Vec3i) *Vec3i {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] < v[Z] {
		v[Z] = v2[Z]
	}
	return v
}

func (v *Vec3i) Max(v2 *Vec3i) *Vec3i {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] > v[Z] {
		v[Z] = v2[Z]
	}
	return v
}

func (v *Vec4i) Min(v2 *Vec4i) *Vec4i {
	if v2[X] < v[X] {
		v[X] = v2[X]
	}
	if v2[Y] < v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] < v[Z] {
		v[Z] = v2[Z]
	}
	if v2[W] < v[W] {
		v[W] = v2[W]
	}
	return v
}

func (v *Vec4i) Max(v2 *Vec4i) *Vec4i {
	if v2[X] > v[X] {
		v[X] = v2[X]
	}
	if v2[Y] > v[Y] {
		v[Y] = v2[Y]
	}
	if v2[Z] > v[Z] {
		v[Z] = v2[Z]
	}
	if v2[W] > v[W] {
		v[W] = v2[W]
	}
	return v
}

func (v *Vec2i) Clamp(min, max *Vec2i) *Vec2i {
	return v.Min(max).Max(min)
}

func (v *Vec3i) Clamp(min, max *Vec3i) *Vec3i {
	return v.Min(max).Max(min)
}

func (v *Vec4i) Clamp(min, max *Vec4i) *Vec4i {
	return v.Min(max).Max(min)
}

func (v *Vec2i) Mod(v2 *Vec2i) *Vec2i {
	v[X] %= v2[X]
	v[Y] %= v2[Y]
	return v
}

func (v *Vec3i) Mod(v2 *Vec3i) *Vec3i {
	v[X] %= v2[X]
	v[Y] %= v2[Y]
	v[Z] %= v2[Z]
	return v
}

func (v *Vec4i) Mod(v2 *Vec4i) *Vec4i {
	v[X] %= v2[X]
	v[Y] %= v2[Y]
	v[Z] %= v2[Z]
	v[W] %= v2[W]
	return v
}

func (v *Vec2i) MajorAxis() uint {
	if math32i.Abs(v[X]) >= math32i.Abs(v[Y]) {
		return 0
	}
	return 1
}

func (v *Vec2i) MinorAxis() uint {
	if math32i.Abs(v[X]) <= math32i.Abs(v[Y]) {
		return X
	}
	return Y
}

func (v *Vec3i) MajorAxis() Axis {
	x := math32i.Abs(v[X])
	y := math32i.Abs(v[Y])
	z := math32i.Abs(v[Z])
	if x >= y {
		if x >= z {
			return X
		}
		return Z
	}
	if x >= z {
		if x >= y {
			return X
		}
		return Y
	}
	if y >= z {
		return Y
	}
	return Z
}

func (v *Vec3i) MinorAxis() Axis {
	x := math32i.Abs(v[X])
	y := math32i.Abs(v[Y])
	z := math32i.Abs(v[Z])
	if x <= y {
		if x <= z {
			return X
		}
		return Z
	}
	if x <= z {
		if x <= y {
			return X
		}
		return Y
	}
	if y <= z {
		return Y
	}
	return Z
}

func (v *Vec2i) Eq(v2 *Vec2i) bool {
	return v[X] == v2[X] && v[Y] == v2[Y]
}

func (v *Vec3i) Eq(v2 *Vec3i) bool {
	return v[X] == v2[X] && v[Y] == v2[Y] && v[Z] == v2[Z]
}

func (v *Vec4i) Eq(v2 *Vec4i) bool {
	return v[X] == v2[X] && v[Y] == v2[Y] && v[Z] == v2[Z] && v[W] == v2[W]
}

func (v *Vec2i) EqDelta(v2 *Vec2i, eps int32) bool {
	return math32i.EqDelta(v[X], v2[X], eps) &&
		math32i.EqDelta(v[Y], v2[Y], eps)
}

func (v *Vec3i) EqDelta(v2 *Vec3i, eps int32) bool {
	return math32i.EqDelta(v[X], v2[X], eps) &&
		math32i.EqDelta(v[Y], v2[Y], eps) &&
		math32i.EqDelta(v[Z], v2[Z], eps)
}

func (v *Vec4i) EqDelta(v2 *Vec4i, eps int32) bool {
	return math32i.EqDelta(v[X], v2[X], eps) &&
		math32i.EqDelta(v[Y], v2[Y], eps) &&
		math32i.EqDelta(v[Z], v2[Z], eps) &&
		math32i.EqDelta(v[W], v2[W], eps)
}

func (v *Vec2i) CompareBy(v2 *Vec2i, order Order2) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXY
	}
	x, y := order>>2&1, order&1
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			return 0
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec3i) CompareBy(v2 *Vec3i, order Order3) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXYZ
	}
	x, y, z := order>>4&3, order>>2&3, order&3
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			if v[z] == v2[z] {
				return 0
			} else if v[z] < v2[z] {
				return -3
			}
			return 3
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec4i) CompareBy(v2 *Vec4i, order Order4) int {
	if v == v2 {
		return 0
	}
	if order == 0 {
		order = OrderXYZW
	}
	x, y, z, w := order>>6&3, order>>4&3, order>>2&3, order&3
	if v[x] == v2[x] {
		if v[y] == v2[y] {
			if v[z] == v2[z] {
				if v[w] == v2[w] {
					return 0
				} else if v[w] < v2[w] {
					return -4
				}
				return 4
			} else if v[z] < v2[z] {
				return -3
			}
			return 3
		} else if v[y] < v2[y] {
			return -2
		}
		return 2
	} else if v[x] < v2[x] {
		return -1
	}
	return 1
}

func (v *Vec2i) String() string {
	return fmt.Sprintf("[%d, %d]", v[X], v[Y])
}

func (v *Vec3i) String() string {
	return fmt.Sprintf("[%d, %d, %d]", v[X], v[Y], v[Z])
}

func (v *Vec4i) String() string {
	return fmt.Sprintf("[%d, %d, %d, %d]", v[X], v[Y], v[Z], v[W])
}

func (v *Vec2i) GoString() string {
	return fmt.Sprintf("vec.Vec2{%#v, %#v}", v[X], v[Y])
}

func (v *Vec3i) GoString() string {
	return fmt.Sprintf("vec.Vec3{%#v, %#v, %#v}", v[X], v[Y], v[Z])
}

func (v *Vec4i) GoString() string {
	return fmt.Sprintf("vec.Vec4{%#v, %#v, %#v, %#v}", v[X], v[Y], v[Z], v[W])
}
