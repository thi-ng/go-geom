package vec

import (
	"testing"
)

func TestAdd(t *testing.T) {
	tests := []struct {
		A, B, Result *Vec3f
	}{
		{&Vec3f{0, 0, 0}, &Vec3f{0, 0, 0}, &Vec3f{0, 0, 0}},
		{&Vec3f{1, 2, 3}, &Vec3f{4, 5, 6}, &Vec3f{5, 7, 9}},
	}
	for _, tt := range tests {
		if r := tt.A.Add(tt.B); !r.Eq(tt.Result) {
			t.Errorf("Add(%v, %v) != %v (got %v)", tt.A, tt.B, tt.Result, r)
		}
	}
}

func BenchmarkAddV2(b *testing.B) {
	v := &Vec2f{1, 2}
	v2 := &Vec2f{4, 5}
	for i := 0; i < b.N; i++ {
		v.Copy().Add(v2)
	}
}

func BenchmarkAddV3f(b *testing.B) {
	v := &Vec3f{1, 2, 3}
	v2 := &Vec3f{4, 5, 6}
	for i := 0; i < b.N; i++ {
		v.Copy().Add(v2)
	}
}

func BenchmarkAddV3(b *testing.B) {
	v := &Vec3{1, 2, 3}
	v2 := &Vec3{4, 5, 6}
	for i := 0; i < b.N; i++ {
		v.Copy().Add(v2)
	}
}

func BenchmarkNormalizeV3f(b *testing.B) {
	v := &Vec3f{1, 2, 3}
	for i := 0; i < b.N; i++ {
		v.Copy().Normalize(1)
	}
}

func BenchmarkNormalizeV3(b *testing.B) {
	v := &Vec3{1, 2, 3}
	for i := 0; i < b.N; i++ {
		v.Copy().Normalize(1)
	}
}

func BenchmarkDistManV2(b *testing.B) {
	v := &Vec2f{1, 2}
	v2 := &Vec2f{10, 20}
	for i := 0; i < b.N; i++ {
		v.DistManhattan(v2)
	}
}

func BenchmarkDistManV3(b *testing.B) {
	v := &Vec3f{1, 2, 3}
	v2 := &Vec3f{10, 20, 30}
	for i := 0; i < b.N; i++ {
		v.DistManhattan(v2)
	}
}
