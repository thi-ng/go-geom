package vec

func (v *Vec2) XX() *Vec2   { return &Vec2{v[0], v[0]} }
func (v *Vec2) XXX() *Vec3  { return &Vec3{v[0], v[0], v[0]} }
func (v *Vec2) XXXX() *Vec4 { return &Vec4{v[0], v[0], v[0], v[0]} }
func (v *Vec2) XXXY() *Vec4 { return &Vec4{v[0], v[0], v[0], v[1]} }
func (v *Vec2) XXY() *Vec3  { return &Vec3{v[0], v[0], v[1]} }
func (v *Vec2) XXYX() *Vec4 { return &Vec4{v[0], v[0], v[1], v[0]} }
func (v *Vec2) XXYY() *Vec4 { return &Vec4{v[0], v[0], v[1], v[1]} }
func (v *Vec2) XY() *Vec2   { return &Vec2{v[0], v[1]} }
func (v *Vec2) XYX() *Vec3  { return &Vec3{v[0], v[1], v[0]} }
func (v *Vec2) XYXX() *Vec4 { return &Vec4{v[0], v[1], v[0], v[0]} }
func (v *Vec2) XYXY() *Vec4 { return &Vec4{v[0], v[1], v[0], v[1]} }
func (v *Vec2) XYY() *Vec3  { return &Vec3{v[0], v[1], v[1]} }
func (v *Vec2) XYYX() *Vec4 { return &Vec4{v[0], v[1], v[1], v[0]} }
func (v *Vec2) XYYY() *Vec4 { return &Vec4{v[0], v[1], v[1], v[1]} }
func (v *Vec2) YX() *Vec2   { return &Vec2{v[1], v[0]} }
func (v *Vec2) YXX() *Vec3  { return &Vec3{v[1], v[0], v[0]} }
func (v *Vec2) YXXX() *Vec4 { return &Vec4{v[1], v[0], v[0], v[0]} }
func (v *Vec2) YXXY() *Vec4 { return &Vec4{v[1], v[0], v[0], v[1]} }
func (v *Vec2) YXY() *Vec3  { return &Vec3{v[1], v[0], v[1]} }
func (v *Vec2) YXYX() *Vec4 { return &Vec4{v[1], v[0], v[1], v[0]} }
func (v *Vec2) YXYY() *Vec4 { return &Vec4{v[1], v[0], v[1], v[1]} }
func (v *Vec2) YY() *Vec2   { return &Vec2{v[1], v[1]} }
func (v *Vec2) YYX() *Vec3  { return &Vec3{v[1], v[1], v[0]} }
func (v *Vec2) YYXX() *Vec4 { return &Vec4{v[1], v[1], v[0], v[0]} }
func (v *Vec2) YYXY() *Vec4 { return &Vec4{v[1], v[1], v[0], v[1]} }
func (v *Vec2) YYY() *Vec3  { return &Vec3{v[1], v[1], v[1]} }
func (v *Vec2) YYYX() *Vec4 { return &Vec4{v[1], v[1], v[1], v[0]} }
func (v *Vec2) YYYY() *Vec4 { return &Vec4{v[1], v[1], v[1], v[1]} }

func (v *Vec3) XX() *Vec2   { return &Vec2{v[0], v[0]} }
func (v *Vec3) XXX() *Vec3  { return &Vec3{v[0], v[0], v[0]} }
func (v *Vec3) XXXX() *Vec4 { return &Vec4{v[0], v[0], v[0], v[0]} }
func (v *Vec3) XXXY() *Vec4 { return &Vec4{v[0], v[0], v[0], v[1]} }
func (v *Vec3) XXXZ() *Vec4 { return &Vec4{v[0], v[0], v[0], v[2]} }
func (v *Vec3) XXY() *Vec3  { return &Vec3{v[0], v[0], v[1]} }
func (v *Vec3) XXYX() *Vec4 { return &Vec4{v[0], v[0], v[1], v[0]} }
func (v *Vec3) XXYY() *Vec4 { return &Vec4{v[0], v[0], v[1], v[1]} }
func (v *Vec3) XXYZ() *Vec4 { return &Vec4{v[0], v[0], v[1], v[2]} }
func (v *Vec3) XXZ() *Vec3  { return &Vec3{v[0], v[0], v[2]} }
func (v *Vec3) XXZX() *Vec4 { return &Vec4{v[0], v[0], v[2], v[0]} }
func (v *Vec3) XXZY() *Vec4 { return &Vec4{v[0], v[0], v[2], v[1]} }
func (v *Vec3) XXZZ() *Vec4 { return &Vec4{v[0], v[0], v[2], v[2]} }
func (v *Vec3) XY() *Vec2   { return &Vec2{v[0], v[1]} }
func (v *Vec3) XYX() *Vec3  { return &Vec3{v[0], v[1], v[0]} }
func (v *Vec3) XYXX() *Vec4 { return &Vec4{v[0], v[1], v[0], v[0]} }
func (v *Vec3) XYXY() *Vec4 { return &Vec4{v[0], v[1], v[0], v[1]} }
func (v *Vec3) XYXZ() *Vec4 { return &Vec4{v[0], v[1], v[0], v[2]} }
func (v *Vec3) XYY() *Vec3  { return &Vec3{v[0], v[1], v[1]} }
func (v *Vec3) XYYX() *Vec4 { return &Vec4{v[0], v[1], v[1], v[0]} }
func (v *Vec3) XYYY() *Vec4 { return &Vec4{v[0], v[1], v[1], v[1]} }
func (v *Vec3) XYYZ() *Vec4 { return &Vec4{v[0], v[1], v[1], v[2]} }
func (v *Vec3) XYZ() *Vec3  { return &Vec3{v[0], v[1], v[2]} }
func (v *Vec3) XYZX() *Vec4 { return &Vec4{v[0], v[1], v[2], v[0]} }
func (v *Vec3) XYZY() *Vec4 { return &Vec4{v[0], v[1], v[2], v[1]} }
func (v *Vec3) XYZZ() *Vec4 { return &Vec4{v[0], v[1], v[2], v[2]} }
func (v *Vec3) XZ() *Vec2   { return &Vec2{v[0], v[2]} }
func (v *Vec3) XZX() *Vec3  { return &Vec3{v[0], v[2], v[0]} }
func (v *Vec3) XZXX() *Vec4 { return &Vec4{v[0], v[2], v[0], v[0]} }
func (v *Vec3) XZXY() *Vec4 { return &Vec4{v[0], v[2], v[0], v[1]} }
func (v *Vec3) XZXZ() *Vec4 { return &Vec4{v[0], v[2], v[0], v[2]} }
func (v *Vec3) XZY() *Vec3  { return &Vec3{v[0], v[2], v[1]} }
func (v *Vec3) XZYX() *Vec4 { return &Vec4{v[0], v[2], v[1], v[0]} }
func (v *Vec3) XZYY() *Vec4 { return &Vec4{v[0], v[2], v[1], v[1]} }
func (v *Vec3) XZYZ() *Vec4 { return &Vec4{v[0], v[2], v[1], v[2]} }
func (v *Vec3) XZZ() *Vec3  { return &Vec3{v[0], v[2], v[2]} }
func (v *Vec3) XZZX() *Vec4 { return &Vec4{v[0], v[2], v[2], v[0]} }
func (v *Vec3) XZZY() *Vec4 { return &Vec4{v[0], v[2], v[2], v[1]} }
func (v *Vec3) XZZZ() *Vec4 { return &Vec4{v[0], v[2], v[2], v[2]} }
func (v *Vec3) YX() *Vec2   { return &Vec2{v[1], v[0]} }
func (v *Vec3) YXX() *Vec3  { return &Vec3{v[1], v[0], v[0]} }
func (v *Vec3) YXXX() *Vec4 { return &Vec4{v[1], v[0], v[0], v[0]} }
func (v *Vec3) YXXY() *Vec4 { return &Vec4{v[1], v[0], v[0], v[1]} }
func (v *Vec3) YXXZ() *Vec4 { return &Vec4{v[1], v[0], v[0], v[2]} }
func (v *Vec3) YXY() *Vec3  { return &Vec3{v[1], v[0], v[1]} }
func (v *Vec3) YXYX() *Vec4 { return &Vec4{v[1], v[0], v[1], v[0]} }
func (v *Vec3) YXYY() *Vec4 { return &Vec4{v[1], v[0], v[1], v[1]} }
func (v *Vec3) YXYZ() *Vec4 { return &Vec4{v[1], v[0], v[1], v[2]} }
func (v *Vec3) YXZ() *Vec3  { return &Vec3{v[1], v[0], v[2]} }
func (v *Vec3) YXZX() *Vec4 { return &Vec4{v[1], v[0], v[2], v[0]} }
func (v *Vec3) YXZY() *Vec4 { return &Vec4{v[1], v[0], v[2], v[1]} }
func (v *Vec3) YXZZ() *Vec4 { return &Vec4{v[1], v[0], v[2], v[2]} }
func (v *Vec3) YY() *Vec2   { return &Vec2{v[1], v[1]} }
func (v *Vec3) YYX() *Vec3  { return &Vec3{v[1], v[1], v[0]} }
func (v *Vec3) YYXX() *Vec4 { return &Vec4{v[1], v[1], v[0], v[0]} }
func (v *Vec3) YYXY() *Vec4 { return &Vec4{v[1], v[1], v[0], v[1]} }
func (v *Vec3) YYXZ() *Vec4 { return &Vec4{v[1], v[1], v[0], v[2]} }
func (v *Vec3) YYY() *Vec3  { return &Vec3{v[1], v[1], v[1]} }
func (v *Vec3) YYYX() *Vec4 { return &Vec4{v[1], v[1], v[1], v[0]} }
func (v *Vec3) YYYY() *Vec4 { return &Vec4{v[1], v[1], v[1], v[1]} }
func (v *Vec3) YYYZ() *Vec4 { return &Vec4{v[1], v[1], v[1], v[2]} }
func (v *Vec3) YYZ() *Vec3  { return &Vec3{v[1], v[1], v[2]} }
func (v *Vec3) YYZX() *Vec4 { return &Vec4{v[1], v[1], v[2], v[0]} }
func (v *Vec3) YYZY() *Vec4 { return &Vec4{v[1], v[1], v[2], v[1]} }
func (v *Vec3) YYZZ() *Vec4 { return &Vec4{v[1], v[1], v[2], v[2]} }
func (v *Vec3) YZ() *Vec2   { return &Vec2{v[1], v[2]} }
func (v *Vec3) YZX() *Vec3  { return &Vec3{v[1], v[2], v[0]} }
func (v *Vec3) YZXX() *Vec4 { return &Vec4{v[1], v[2], v[0], v[0]} }
func (v *Vec3) YZXY() *Vec4 { return &Vec4{v[1], v[2], v[0], v[1]} }
func (v *Vec3) YZXZ() *Vec4 { return &Vec4{v[1], v[2], v[0], v[2]} }
func (v *Vec3) YZY() *Vec3  { return &Vec3{v[1], v[2], v[1]} }
func (v *Vec3) YZYX() *Vec4 { return &Vec4{v[1], v[2], v[1], v[0]} }
func (v *Vec3) YZYY() *Vec4 { return &Vec4{v[1], v[2], v[1], v[1]} }
func (v *Vec3) YZYZ() *Vec4 { return &Vec4{v[1], v[2], v[1], v[2]} }
func (v *Vec3) YZZ() *Vec3  { return &Vec3{v[1], v[2], v[2]} }
func (v *Vec3) YZZX() *Vec4 { return &Vec4{v[1], v[2], v[2], v[0]} }
func (v *Vec3) YZZY() *Vec4 { return &Vec4{v[1], v[2], v[2], v[1]} }
func (v *Vec3) YZZZ() *Vec4 { return &Vec4{v[1], v[2], v[2], v[2]} }
func (v *Vec3) ZX() *Vec2   { return &Vec2{v[2], v[0]} }
func (v *Vec3) ZXX() *Vec3  { return &Vec3{v[2], v[0], v[0]} }
func (v *Vec3) ZXXX() *Vec4 { return &Vec4{v[2], v[0], v[0], v[0]} }
func (v *Vec3) ZXXY() *Vec4 { return &Vec4{v[2], v[0], v[0], v[1]} }
func (v *Vec3) ZXXZ() *Vec4 { return &Vec4{v[2], v[0], v[0], v[2]} }
func (v *Vec3) ZXY() *Vec3  { return &Vec3{v[2], v[0], v[1]} }
func (v *Vec3) ZXYX() *Vec4 { return &Vec4{v[2], v[0], v[1], v[0]} }
func (v *Vec3) ZXYY() *Vec4 { return &Vec4{v[2], v[0], v[1], v[1]} }
func (v *Vec3) ZXYZ() *Vec4 { return &Vec4{v[2], v[0], v[1], v[2]} }
func (v *Vec3) ZXZ() *Vec3  { return &Vec3{v[2], v[0], v[2]} }
func (v *Vec3) ZXZX() *Vec4 { return &Vec4{v[2], v[0], v[2], v[0]} }
func (v *Vec3) ZXZY() *Vec4 { return &Vec4{v[2], v[0], v[2], v[1]} }
func (v *Vec3) ZXZZ() *Vec4 { return &Vec4{v[2], v[0], v[2], v[2]} }
func (v *Vec3) ZY() *Vec2   { return &Vec2{v[2], v[1]} }
func (v *Vec3) ZYX() *Vec3  { return &Vec3{v[2], v[1], v[0]} }
func (v *Vec3) ZYXX() *Vec4 { return &Vec4{v[2], v[1], v[0], v[0]} }
func (v *Vec3) ZYXY() *Vec4 { return &Vec4{v[2], v[1], v[0], v[1]} }
func (v *Vec3) ZYXZ() *Vec4 { return &Vec4{v[2], v[1], v[0], v[2]} }
func (v *Vec3) ZYY() *Vec3  { return &Vec3{v[2], v[1], v[1]} }
func (v *Vec3) ZYYX() *Vec4 { return &Vec4{v[2], v[1], v[1], v[0]} }
func (v *Vec3) ZYYY() *Vec4 { return &Vec4{v[2], v[1], v[1], v[1]} }
func (v *Vec3) ZYYZ() *Vec4 { return &Vec4{v[2], v[1], v[1], v[2]} }
func (v *Vec3) ZYZ() *Vec3  { return &Vec3{v[2], v[1], v[2]} }
func (v *Vec3) ZYZX() *Vec4 { return &Vec4{v[2], v[1], v[2], v[0]} }
func (v *Vec3) ZYZY() *Vec4 { return &Vec4{v[2], v[1], v[2], v[1]} }
func (v *Vec3) ZYZZ() *Vec4 { return &Vec4{v[2], v[1], v[2], v[2]} }
func (v *Vec3) ZZ() *Vec2   { return &Vec2{v[2], v[2]} }
func (v *Vec3) ZZX() *Vec3  { return &Vec3{v[2], v[2], v[0]} }
func (v *Vec3) ZZXX() *Vec4 { return &Vec4{v[2], v[2], v[0], v[0]} }
func (v *Vec3) ZZXY() *Vec4 { return &Vec4{v[2], v[2], v[0], v[1]} }
func (v *Vec3) ZZXZ() *Vec4 { return &Vec4{v[2], v[2], v[0], v[2]} }
func (v *Vec3) ZZY() *Vec3  { return &Vec3{v[2], v[2], v[1]} }
func (v *Vec3) ZZYX() *Vec4 { return &Vec4{v[2], v[2], v[1], v[0]} }
func (v *Vec3) ZZYY() *Vec4 { return &Vec4{v[2], v[2], v[1], v[1]} }
func (v *Vec3) ZZYZ() *Vec4 { return &Vec4{v[2], v[2], v[1], v[2]} }
func (v *Vec3) ZZZ() *Vec3  { return &Vec3{v[2], v[2], v[2]} }
func (v *Vec3) ZZZX() *Vec4 { return &Vec4{v[2], v[2], v[2], v[0]} }
func (v *Vec3) ZZZY() *Vec4 { return &Vec4{v[2], v[2], v[2], v[1]} }
func (v *Vec3) ZZZZ() *Vec4 { return &Vec4{v[2], v[2], v[2], v[2]} }

func (v *Vec4) WW() *Vec2   { return &Vec2{v[3], v[3]} }
func (v *Vec4) WWW() *Vec3  { return &Vec3{v[3], v[3], v[3]} }
func (v *Vec4) WWWW() *Vec4 { return &Vec4{v[3], v[3], v[3], v[3]} }
func (v *Vec4) WWWX() *Vec4 { return &Vec4{v[3], v[3], v[3], v[0]} }
func (v *Vec4) WWWY() *Vec4 { return &Vec4{v[3], v[3], v[3], v[1]} }
func (v *Vec4) WWWZ() *Vec4 { return &Vec4{v[3], v[3], v[3], v[2]} }
func (v *Vec4) WWX() *Vec3  { return &Vec3{v[3], v[3], v[0]} }
func (v *Vec4) WWXW() *Vec4 { return &Vec4{v[3], v[3], v[0], v[3]} }
func (v *Vec4) WWXX() *Vec4 { return &Vec4{v[3], v[3], v[0], v[0]} }
func (v *Vec4) WWXY() *Vec4 { return &Vec4{v[3], v[3], v[0], v[1]} }
func (v *Vec4) WWXZ() *Vec4 { return &Vec4{v[3], v[3], v[0], v[2]} }
func (v *Vec4) WWY() *Vec3  { return &Vec3{v[3], v[3], v[1]} }
func (v *Vec4) WWYW() *Vec4 { return &Vec4{v[3], v[3], v[1], v[3]} }
func (v *Vec4) WWYX() *Vec4 { return &Vec4{v[3], v[3], v[1], v[0]} }
func (v *Vec4) WWYY() *Vec4 { return &Vec4{v[3], v[3], v[1], v[1]} }
func (v *Vec4) WWYZ() *Vec4 { return &Vec4{v[3], v[3], v[1], v[2]} }
func (v *Vec4) WWZ() *Vec3  { return &Vec3{v[3], v[3], v[2]} }
func (v *Vec4) WWZW() *Vec4 { return &Vec4{v[3], v[3], v[2], v[3]} }
func (v *Vec4) WWZX() *Vec4 { return &Vec4{v[3], v[3], v[2], v[0]} }
func (v *Vec4) WWZY() *Vec4 { return &Vec4{v[3], v[3], v[2], v[1]} }
func (v *Vec4) WWZZ() *Vec4 { return &Vec4{v[3], v[3], v[2], v[2]} }
func (v *Vec4) WX() *Vec2   { return &Vec2{v[3], v[0]} }
func (v *Vec4) WXW() *Vec3  { return &Vec3{v[3], v[0], v[3]} }
func (v *Vec4) WXWW() *Vec4 { return &Vec4{v[3], v[0], v[3], v[3]} }
func (v *Vec4) WXWX() *Vec4 { return &Vec4{v[3], v[0], v[3], v[0]} }
func (v *Vec4) WXWY() *Vec4 { return &Vec4{v[3], v[0], v[3], v[1]} }
func (v *Vec4) WXWZ() *Vec4 { return &Vec4{v[3], v[0], v[3], v[2]} }
func (v *Vec4) WXX() *Vec3  { return &Vec3{v[3], v[0], v[0]} }
func (v *Vec4) WXXW() *Vec4 { return &Vec4{v[3], v[0], v[0], v[3]} }
func (v *Vec4) WXXX() *Vec4 { return &Vec4{v[3], v[0], v[0], v[0]} }
func (v *Vec4) WXXY() *Vec4 { return &Vec4{v[3], v[0], v[0], v[1]} }
func (v *Vec4) WXXZ() *Vec4 { return &Vec4{v[3], v[0], v[0], v[2]} }
func (v *Vec4) WXY() *Vec3  { return &Vec3{v[3], v[0], v[1]} }
func (v *Vec4) WXYW() *Vec4 { return &Vec4{v[3], v[0], v[1], v[3]} }
func (v *Vec4) WXYX() *Vec4 { return &Vec4{v[3], v[0], v[1], v[0]} }
func (v *Vec4) WXYY() *Vec4 { return &Vec4{v[3], v[0], v[1], v[1]} }
func (v *Vec4) WXYZ() *Vec4 { return &Vec4{v[3], v[0], v[1], v[2]} }
func (v *Vec4) WXZ() *Vec3  { return &Vec3{v[3], v[0], v[2]} }
func (v *Vec4) WXZW() *Vec4 { return &Vec4{v[3], v[0], v[2], v[3]} }
func (v *Vec4) WXZX() *Vec4 { return &Vec4{v[3], v[0], v[2], v[0]} }
func (v *Vec4) WXZY() *Vec4 { return &Vec4{v[3], v[0], v[2], v[1]} }
func (v *Vec4) WXZZ() *Vec4 { return &Vec4{v[3], v[0], v[2], v[2]} }
func (v *Vec4) WY() *Vec2   { return &Vec2{v[3], v[1]} }
func (v *Vec4) WYW() *Vec3  { return &Vec3{v[3], v[1], v[3]} }
func (v *Vec4) WYWW() *Vec4 { return &Vec4{v[3], v[1], v[3], v[3]} }
func (v *Vec4) WYWX() *Vec4 { return &Vec4{v[3], v[1], v[3], v[0]} }
func (v *Vec4) WYWY() *Vec4 { return &Vec4{v[3], v[1], v[3], v[1]} }
func (v *Vec4) WYWZ() *Vec4 { return &Vec4{v[3], v[1], v[3], v[2]} }
func (v *Vec4) WYX() *Vec3  { return &Vec3{v[3], v[1], v[0]} }
func (v *Vec4) WYXW() *Vec4 { return &Vec4{v[3], v[1], v[0], v[3]} }
func (v *Vec4) WYXX() *Vec4 { return &Vec4{v[3], v[1], v[0], v[0]} }
func (v *Vec4) WYXY() *Vec4 { return &Vec4{v[3], v[1], v[0], v[1]} }
func (v *Vec4) WYXZ() *Vec4 { return &Vec4{v[3], v[1], v[0], v[2]} }
func (v *Vec4) WYY() *Vec3  { return &Vec3{v[3], v[1], v[1]} }
func (v *Vec4) WYYW() *Vec4 { return &Vec4{v[3], v[1], v[1], v[3]} }
func (v *Vec4) WYYX() *Vec4 { return &Vec4{v[3], v[1], v[1], v[0]} }
func (v *Vec4) WYYY() *Vec4 { return &Vec4{v[3], v[1], v[1], v[1]} }
func (v *Vec4) WYYZ() *Vec4 { return &Vec4{v[3], v[1], v[1], v[2]} }
func (v *Vec4) WYZ() *Vec3  { return &Vec3{v[3], v[1], v[2]} }
func (v *Vec4) WYZW() *Vec4 { return &Vec4{v[3], v[1], v[2], v[3]} }
func (v *Vec4) WYZX() *Vec4 { return &Vec4{v[3], v[1], v[2], v[0]} }
func (v *Vec4) WYZY() *Vec4 { return &Vec4{v[3], v[1], v[2], v[1]} }
func (v *Vec4) WYZZ() *Vec4 { return &Vec4{v[3], v[1], v[2], v[2]} }
func (v *Vec4) WZ() *Vec2   { return &Vec2{v[3], v[2]} }
func (v *Vec4) WZW() *Vec3  { return &Vec3{v[3], v[2], v[3]} }
func (v *Vec4) WZWW() *Vec4 { return &Vec4{v[3], v[2], v[3], v[3]} }
func (v *Vec4) WZWX() *Vec4 { return &Vec4{v[3], v[2], v[3], v[0]} }
func (v *Vec4) WZWY() *Vec4 { return &Vec4{v[3], v[2], v[3], v[1]} }
func (v *Vec4) WZWZ() *Vec4 { return &Vec4{v[3], v[2], v[3], v[2]} }
func (v *Vec4) WZX() *Vec3  { return &Vec3{v[3], v[2], v[0]} }
func (v *Vec4) WZXW() *Vec4 { return &Vec4{v[3], v[2], v[0], v[3]} }
func (v *Vec4) WZXX() *Vec4 { return &Vec4{v[3], v[2], v[0], v[0]} }
func (v *Vec4) WZXY() *Vec4 { return &Vec4{v[3], v[2], v[0], v[1]} }
func (v *Vec4) WZXZ() *Vec4 { return &Vec4{v[3], v[2], v[0], v[2]} }
func (v *Vec4) WZY() *Vec3  { return &Vec3{v[3], v[2], v[1]} }
func (v *Vec4) WZYW() *Vec4 { return &Vec4{v[3], v[2], v[1], v[3]} }
func (v *Vec4) WZYX() *Vec4 { return &Vec4{v[3], v[2], v[1], v[0]} }
func (v *Vec4) WZYY() *Vec4 { return &Vec4{v[3], v[2], v[1], v[1]} }
func (v *Vec4) WZYZ() *Vec4 { return &Vec4{v[3], v[2], v[1], v[2]} }
func (v *Vec4) WZZ() *Vec3  { return &Vec3{v[3], v[2], v[2]} }
func (v *Vec4) WZZW() *Vec4 { return &Vec4{v[3], v[2], v[2], v[3]} }
func (v *Vec4) WZZX() *Vec4 { return &Vec4{v[3], v[2], v[2], v[0]} }
func (v *Vec4) WZZY() *Vec4 { return &Vec4{v[3], v[2], v[2], v[1]} }
func (v *Vec4) WZZZ() *Vec4 { return &Vec4{v[3], v[2], v[2], v[2]} }
func (v *Vec4) XW() *Vec2   { return &Vec2{v[0], v[3]} }
func (v *Vec4) XWW() *Vec3  { return &Vec3{v[0], v[3], v[3]} }
func (v *Vec4) XWWW() *Vec4 { return &Vec4{v[0], v[3], v[3], v[3]} }
func (v *Vec4) XWWX() *Vec4 { return &Vec4{v[0], v[3], v[3], v[0]} }
func (v *Vec4) XWWY() *Vec4 { return &Vec4{v[0], v[3], v[3], v[1]} }
func (v *Vec4) XWWZ() *Vec4 { return &Vec4{v[0], v[3], v[3], v[2]} }
func (v *Vec4) XWX() *Vec3  { return &Vec3{v[0], v[3], v[0]} }
func (v *Vec4) XWXW() *Vec4 { return &Vec4{v[0], v[3], v[0], v[3]} }
func (v *Vec4) XWXX() *Vec4 { return &Vec4{v[0], v[3], v[0], v[0]} }
func (v *Vec4) XWXY() *Vec4 { return &Vec4{v[0], v[3], v[0], v[1]} }
func (v *Vec4) XWXZ() *Vec4 { return &Vec4{v[0], v[3], v[0], v[2]} }
func (v *Vec4) XWY() *Vec3  { return &Vec3{v[0], v[3], v[1]} }
func (v *Vec4) XWYW() *Vec4 { return &Vec4{v[0], v[3], v[1], v[3]} }
func (v *Vec4) XWYX() *Vec4 { return &Vec4{v[0], v[3], v[1], v[0]} }
func (v *Vec4) XWYY() *Vec4 { return &Vec4{v[0], v[3], v[1], v[1]} }
func (v *Vec4) XWYZ() *Vec4 { return &Vec4{v[0], v[3], v[1], v[2]} }
func (v *Vec4) XWZ() *Vec3  { return &Vec3{v[0], v[3], v[2]} }
func (v *Vec4) XWZW() *Vec4 { return &Vec4{v[0], v[3], v[2], v[3]} }
func (v *Vec4) XWZX() *Vec4 { return &Vec4{v[0], v[3], v[2], v[0]} }
func (v *Vec4) XWZY() *Vec4 { return &Vec4{v[0], v[3], v[2], v[1]} }
func (v *Vec4) XWZZ() *Vec4 { return &Vec4{v[0], v[3], v[2], v[2]} }
func (v *Vec4) XX() *Vec2   { return &Vec2{v[0], v[0]} }
func (v *Vec4) XXW() *Vec3  { return &Vec3{v[0], v[0], v[3]} }
func (v *Vec4) XXWW() *Vec4 { return &Vec4{v[0], v[0], v[3], v[3]} }
func (v *Vec4) XXWX() *Vec4 { return &Vec4{v[0], v[0], v[3], v[0]} }
func (v *Vec4) XXWY() *Vec4 { return &Vec4{v[0], v[0], v[3], v[1]} }
func (v *Vec4) XXWZ() *Vec4 { return &Vec4{v[0], v[0], v[3], v[2]} }
func (v *Vec4) XXX() *Vec3  { return &Vec3{v[0], v[0], v[0]} }
func (v *Vec4) XXXW() *Vec4 { return &Vec4{v[0], v[0], v[0], v[3]} }
func (v *Vec4) XXXX() *Vec4 { return &Vec4{v[0], v[0], v[0], v[0]} }
func (v *Vec4) XXXY() *Vec4 { return &Vec4{v[0], v[0], v[0], v[1]} }
func (v *Vec4) XXXZ() *Vec4 { return &Vec4{v[0], v[0], v[0], v[2]} }
func (v *Vec4) XXY() *Vec3  { return &Vec3{v[0], v[0], v[1]} }
func (v *Vec4) XXYW() *Vec4 { return &Vec4{v[0], v[0], v[1], v[3]} }
func (v *Vec4) XXYX() *Vec4 { return &Vec4{v[0], v[0], v[1], v[0]} }
func (v *Vec4) XXYY() *Vec4 { return &Vec4{v[0], v[0], v[1], v[1]} }
func (v *Vec4) XXYZ() *Vec4 { return &Vec4{v[0], v[0], v[1], v[2]} }
func (v *Vec4) XXZ() *Vec3  { return &Vec3{v[0], v[0], v[2]} }
func (v *Vec4) XXZW() *Vec4 { return &Vec4{v[0], v[0], v[2], v[3]} }
func (v *Vec4) XXZX() *Vec4 { return &Vec4{v[0], v[0], v[2], v[0]} }
func (v *Vec4) XXZY() *Vec4 { return &Vec4{v[0], v[0], v[2], v[1]} }
func (v *Vec4) XXZZ() *Vec4 { return &Vec4{v[0], v[0], v[2], v[2]} }
func (v *Vec4) XY() *Vec2   { return &Vec2{v[0], v[1]} }
func (v *Vec4) XYW() *Vec3  { return &Vec3{v[0], v[1], v[3]} }
func (v *Vec4) XYWW() *Vec4 { return &Vec4{v[0], v[1], v[3], v[3]} }
func (v *Vec4) XYWX() *Vec4 { return &Vec4{v[0], v[1], v[3], v[0]} }
func (v *Vec4) XYWY() *Vec4 { return &Vec4{v[0], v[1], v[3], v[1]} }
func (v *Vec4) XYWZ() *Vec4 { return &Vec4{v[0], v[1], v[3], v[2]} }
func (v *Vec4) XYX() *Vec3  { return &Vec3{v[0], v[1], v[0]} }
func (v *Vec4) XYXW() *Vec4 { return &Vec4{v[0], v[1], v[0], v[3]} }
func (v *Vec4) XYXX() *Vec4 { return &Vec4{v[0], v[1], v[0], v[0]} }
func (v *Vec4) XYXY() *Vec4 { return &Vec4{v[0], v[1], v[0], v[1]} }
func (v *Vec4) XYXZ() *Vec4 { return &Vec4{v[0], v[1], v[0], v[2]} }
func (v *Vec4) XYY() *Vec3  { return &Vec3{v[0], v[1], v[1]} }
func (v *Vec4) XYYW() *Vec4 { return &Vec4{v[0], v[1], v[1], v[3]} }
func (v *Vec4) XYYX() *Vec4 { return &Vec4{v[0], v[1], v[1], v[0]} }
func (v *Vec4) XYYY() *Vec4 { return &Vec4{v[0], v[1], v[1], v[1]} }
func (v *Vec4) XYYZ() *Vec4 { return &Vec4{v[0], v[1], v[1], v[2]} }
func (v *Vec4) XYZ() *Vec3  { return &Vec3{v[0], v[1], v[2]} }
func (v *Vec4) XYZW() *Vec4 { return &Vec4{v[0], v[1], v[2], v[3]} }
func (v *Vec4) XYZX() *Vec4 { return &Vec4{v[0], v[1], v[2], v[0]} }
func (v *Vec4) XYZY() *Vec4 { return &Vec4{v[0], v[1], v[2], v[1]} }
func (v *Vec4) XYZZ() *Vec4 { return &Vec4{v[0], v[1], v[2], v[2]} }
func (v *Vec4) XZ() *Vec2   { return &Vec2{v[0], v[2]} }
func (v *Vec4) XZW() *Vec3  { return &Vec3{v[0], v[2], v[3]} }
func (v *Vec4) XZWW() *Vec4 { return &Vec4{v[0], v[2], v[3], v[3]} }
func (v *Vec4) XZWX() *Vec4 { return &Vec4{v[0], v[2], v[3], v[0]} }
func (v *Vec4) XZWY() *Vec4 { return &Vec4{v[0], v[2], v[3], v[1]} }
func (v *Vec4) XZWZ() *Vec4 { return &Vec4{v[0], v[2], v[3], v[2]} }
func (v *Vec4) XZX() *Vec3  { return &Vec3{v[0], v[2], v[0]} }
func (v *Vec4) XZXW() *Vec4 { return &Vec4{v[0], v[2], v[0], v[3]} }
func (v *Vec4) XZXX() *Vec4 { return &Vec4{v[0], v[2], v[0], v[0]} }
func (v *Vec4) XZXY() *Vec4 { return &Vec4{v[0], v[2], v[0], v[1]} }
func (v *Vec4) XZXZ() *Vec4 { return &Vec4{v[0], v[2], v[0], v[2]} }
func (v *Vec4) XZY() *Vec3  { return &Vec3{v[0], v[2], v[1]} }
func (v *Vec4) XZYW() *Vec4 { return &Vec4{v[0], v[2], v[1], v[3]} }
func (v *Vec4) XZYX() *Vec4 { return &Vec4{v[0], v[2], v[1], v[0]} }
func (v *Vec4) XZYY() *Vec4 { return &Vec4{v[0], v[2], v[1], v[1]} }
func (v *Vec4) XZYZ() *Vec4 { return &Vec4{v[0], v[2], v[1], v[2]} }
func (v *Vec4) XZZ() *Vec3  { return &Vec3{v[0], v[2], v[2]} }
func (v *Vec4) XZZW() *Vec4 { return &Vec4{v[0], v[2], v[2], v[3]} }
func (v *Vec4) XZZX() *Vec4 { return &Vec4{v[0], v[2], v[2], v[0]} }
func (v *Vec4) XZZY() *Vec4 { return &Vec4{v[0], v[2], v[2], v[1]} }
func (v *Vec4) XZZZ() *Vec4 { return &Vec4{v[0], v[2], v[2], v[2]} }
func (v *Vec4) YW() *Vec2   { return &Vec2{v[1], v[3]} }
func (v *Vec4) YWW() *Vec3  { return &Vec3{v[1], v[3], v[3]} }
func (v *Vec4) YWWW() *Vec4 { return &Vec4{v[1], v[3], v[3], v[3]} }
func (v *Vec4) YWWX() *Vec4 { return &Vec4{v[1], v[3], v[3], v[0]} }
func (v *Vec4) YWWY() *Vec4 { return &Vec4{v[1], v[3], v[3], v[1]} }
func (v *Vec4) YWWZ() *Vec4 { return &Vec4{v[1], v[3], v[3], v[2]} }
func (v *Vec4) YWX() *Vec3  { return &Vec3{v[1], v[3], v[0]} }
func (v *Vec4) YWXW() *Vec4 { return &Vec4{v[1], v[3], v[0], v[3]} }
func (v *Vec4) YWXX() *Vec4 { return &Vec4{v[1], v[3], v[0], v[0]} }
func (v *Vec4) YWXY() *Vec4 { return &Vec4{v[1], v[3], v[0], v[1]} }
func (v *Vec4) YWXZ() *Vec4 { return &Vec4{v[1], v[3], v[0], v[2]} }
func (v *Vec4) YWY() *Vec3  { return &Vec3{v[1], v[3], v[1]} }
func (v *Vec4) YWYW() *Vec4 { return &Vec4{v[1], v[3], v[1], v[3]} }
func (v *Vec4) YWYX() *Vec4 { return &Vec4{v[1], v[3], v[1], v[0]} }
func (v *Vec4) YWYY() *Vec4 { return &Vec4{v[1], v[3], v[1], v[1]} }
func (v *Vec4) YWYZ() *Vec4 { return &Vec4{v[1], v[3], v[1], v[2]} }
func (v *Vec4) YWZ() *Vec3  { return &Vec3{v[1], v[3], v[2]} }
func (v *Vec4) YWZW() *Vec4 { return &Vec4{v[1], v[3], v[2], v[3]} }
func (v *Vec4) YWZX() *Vec4 { return &Vec4{v[1], v[3], v[2], v[0]} }
func (v *Vec4) YWZY() *Vec4 { return &Vec4{v[1], v[3], v[2], v[1]} }
func (v *Vec4) YWZZ() *Vec4 { return &Vec4{v[1], v[3], v[2], v[2]} }
func (v *Vec4) YX() *Vec2   { return &Vec2{v[1], v[0]} }
func (v *Vec4) YXW() *Vec3  { return &Vec3{v[1], v[0], v[3]} }
func (v *Vec4) YXWW() *Vec4 { return &Vec4{v[1], v[0], v[3], v[3]} }
func (v *Vec4) YXWX() *Vec4 { return &Vec4{v[1], v[0], v[3], v[0]} }
func (v *Vec4) YXWY() *Vec4 { return &Vec4{v[1], v[0], v[3], v[1]} }
func (v *Vec4) YXWZ() *Vec4 { return &Vec4{v[1], v[0], v[3], v[2]} }
func (v *Vec4) YXX() *Vec3  { return &Vec3{v[1], v[0], v[0]} }
func (v *Vec4) YXXW() *Vec4 { return &Vec4{v[1], v[0], v[0], v[3]} }
func (v *Vec4) YXXX() *Vec4 { return &Vec4{v[1], v[0], v[0], v[0]} }
func (v *Vec4) YXXY() *Vec4 { return &Vec4{v[1], v[0], v[0], v[1]} }
func (v *Vec4) YXXZ() *Vec4 { return &Vec4{v[1], v[0], v[0], v[2]} }
func (v *Vec4) YXY() *Vec3  { return &Vec3{v[1], v[0], v[1]} }
func (v *Vec4) YXYW() *Vec4 { return &Vec4{v[1], v[0], v[1], v[3]} }
func (v *Vec4) YXYX() *Vec4 { return &Vec4{v[1], v[0], v[1], v[0]} }
func (v *Vec4) YXYY() *Vec4 { return &Vec4{v[1], v[0], v[1], v[1]} }
func (v *Vec4) YXYZ() *Vec4 { return &Vec4{v[1], v[0], v[1], v[2]} }
func (v *Vec4) YXZ() *Vec3  { return &Vec3{v[1], v[0], v[2]} }
func (v *Vec4) YXZW() *Vec4 { return &Vec4{v[1], v[0], v[2], v[3]} }
func (v *Vec4) YXZX() *Vec4 { return &Vec4{v[1], v[0], v[2], v[0]} }
func (v *Vec4) YXZY() *Vec4 { return &Vec4{v[1], v[0], v[2], v[1]} }
func (v *Vec4) YXZZ() *Vec4 { return &Vec4{v[1], v[0], v[2], v[2]} }
func (v *Vec4) YY() *Vec2   { return &Vec2{v[1], v[1]} }
func (v *Vec4) YYW() *Vec3  { return &Vec3{v[1], v[1], v[3]} }
func (v *Vec4) YYWW() *Vec4 { return &Vec4{v[1], v[1], v[3], v[3]} }
func (v *Vec4) YYWX() *Vec4 { return &Vec4{v[1], v[1], v[3], v[0]} }
func (v *Vec4) YYWY() *Vec4 { return &Vec4{v[1], v[1], v[3], v[1]} }
func (v *Vec4) YYWZ() *Vec4 { return &Vec4{v[1], v[1], v[3], v[2]} }
func (v *Vec4) YYX() *Vec3  { return &Vec3{v[1], v[1], v[0]} }
func (v *Vec4) YYXW() *Vec4 { return &Vec4{v[1], v[1], v[0], v[3]} }
func (v *Vec4) YYXX() *Vec4 { return &Vec4{v[1], v[1], v[0], v[0]} }
func (v *Vec4) YYXY() *Vec4 { return &Vec4{v[1], v[1], v[0], v[1]} }
func (v *Vec4) YYXZ() *Vec4 { return &Vec4{v[1], v[1], v[0], v[2]} }
func (v *Vec4) YYY() *Vec3  { return &Vec3{v[1], v[1], v[1]} }
func (v *Vec4) YYYW() *Vec4 { return &Vec4{v[1], v[1], v[1], v[3]} }
func (v *Vec4) YYYX() *Vec4 { return &Vec4{v[1], v[1], v[1], v[0]} }
func (v *Vec4) YYYY() *Vec4 { return &Vec4{v[1], v[1], v[1], v[1]} }
func (v *Vec4) YYYZ() *Vec4 { return &Vec4{v[1], v[1], v[1], v[2]} }
func (v *Vec4) YYZ() *Vec3  { return &Vec3{v[1], v[1], v[2]} }
func (v *Vec4) YYZW() *Vec4 { return &Vec4{v[1], v[1], v[2], v[3]} }
func (v *Vec4) YYZX() *Vec4 { return &Vec4{v[1], v[1], v[2], v[0]} }
func (v *Vec4) YYZY() *Vec4 { return &Vec4{v[1], v[1], v[2], v[1]} }
func (v *Vec4) YYZZ() *Vec4 { return &Vec4{v[1], v[1], v[2], v[2]} }
func (v *Vec4) YZ() *Vec2   { return &Vec2{v[1], v[2]} }
func (v *Vec4) YZW() *Vec3  { return &Vec3{v[1], v[2], v[3]} }
func (v *Vec4) YZWW() *Vec4 { return &Vec4{v[1], v[2], v[3], v[3]} }
func (v *Vec4) YZWX() *Vec4 { return &Vec4{v[1], v[2], v[3], v[0]} }
func (v *Vec4) YZWY() *Vec4 { return &Vec4{v[1], v[2], v[3], v[1]} }
func (v *Vec4) YZWZ() *Vec4 { return &Vec4{v[1], v[2], v[3], v[2]} }
func (v *Vec4) YZX() *Vec3  { return &Vec3{v[1], v[2], v[0]} }
func (v *Vec4) YZXW() *Vec4 { return &Vec4{v[1], v[2], v[0], v[3]} }
func (v *Vec4) YZXX() *Vec4 { return &Vec4{v[1], v[2], v[0], v[0]} }
func (v *Vec4) YZXY() *Vec4 { return &Vec4{v[1], v[2], v[0], v[1]} }
func (v *Vec4) YZXZ() *Vec4 { return &Vec4{v[1], v[2], v[0], v[2]} }
func (v *Vec4) YZY() *Vec3  { return &Vec3{v[1], v[2], v[1]} }
func (v *Vec4) YZYW() *Vec4 { return &Vec4{v[1], v[2], v[1], v[3]} }
func (v *Vec4) YZYX() *Vec4 { return &Vec4{v[1], v[2], v[1], v[0]} }
func (v *Vec4) YZYY() *Vec4 { return &Vec4{v[1], v[2], v[1], v[1]} }
func (v *Vec4) YZYZ() *Vec4 { return &Vec4{v[1], v[2], v[1], v[2]} }
func (v *Vec4) YZZ() *Vec3  { return &Vec3{v[1], v[2], v[2]} }
func (v *Vec4) YZZW() *Vec4 { return &Vec4{v[1], v[2], v[2], v[3]} }
func (v *Vec4) YZZX() *Vec4 { return &Vec4{v[1], v[2], v[2], v[0]} }
func (v *Vec4) YZZY() *Vec4 { return &Vec4{v[1], v[2], v[2], v[1]} }
func (v *Vec4) YZZZ() *Vec4 { return &Vec4{v[1], v[2], v[2], v[2]} }
func (v *Vec4) ZW() *Vec2   { return &Vec2{v[2], v[3]} }
func (v *Vec4) ZWW() *Vec3  { return &Vec3{v[2], v[3], v[3]} }
func (v *Vec4) ZWWW() *Vec4 { return &Vec4{v[2], v[3], v[3], v[3]} }
func (v *Vec4) ZWWX() *Vec4 { return &Vec4{v[2], v[3], v[3], v[0]} }
func (v *Vec4) ZWWY() *Vec4 { return &Vec4{v[2], v[3], v[3], v[1]} }
func (v *Vec4) ZWWZ() *Vec4 { return &Vec4{v[2], v[3], v[3], v[2]} }
func (v *Vec4) ZWX() *Vec3  { return &Vec3{v[2], v[3], v[0]} }
func (v *Vec4) ZWXW() *Vec4 { return &Vec4{v[2], v[3], v[0], v[3]} }
func (v *Vec4) ZWXX() *Vec4 { return &Vec4{v[2], v[3], v[0], v[0]} }
func (v *Vec4) ZWXY() *Vec4 { return &Vec4{v[2], v[3], v[0], v[1]} }
func (v *Vec4) ZWXZ() *Vec4 { return &Vec4{v[2], v[3], v[0], v[2]} }
func (v *Vec4) ZWY() *Vec3  { return &Vec3{v[2], v[3], v[1]} }
func (v *Vec4) ZWYW() *Vec4 { return &Vec4{v[2], v[3], v[1], v[3]} }
func (v *Vec4) ZWYX() *Vec4 { return &Vec4{v[2], v[3], v[1], v[0]} }
func (v *Vec4) ZWYY() *Vec4 { return &Vec4{v[2], v[3], v[1], v[1]} }
func (v *Vec4) ZWYZ() *Vec4 { return &Vec4{v[2], v[3], v[1], v[2]} }
func (v *Vec4) ZWZ() *Vec3  { return &Vec3{v[2], v[3], v[2]} }
func (v *Vec4) ZWZW() *Vec4 { return &Vec4{v[2], v[3], v[2], v[3]} }
func (v *Vec4) ZWZX() *Vec4 { return &Vec4{v[2], v[3], v[2], v[0]} }
func (v *Vec4) ZWZY() *Vec4 { return &Vec4{v[2], v[3], v[2], v[1]} }
func (v *Vec4) ZWZZ() *Vec4 { return &Vec4{v[2], v[3], v[2], v[2]} }
func (v *Vec4) ZX() *Vec2   { return &Vec2{v[2], v[0]} }
func (v *Vec4) ZXW() *Vec3  { return &Vec3{v[2], v[0], v[3]} }
func (v *Vec4) ZXWW() *Vec4 { return &Vec4{v[2], v[0], v[3], v[3]} }
func (v *Vec4) ZXWX() *Vec4 { return &Vec4{v[2], v[0], v[3], v[0]} }
func (v *Vec4) ZXWY() *Vec4 { return &Vec4{v[2], v[0], v[3], v[1]} }
func (v *Vec4) ZXWZ() *Vec4 { return &Vec4{v[2], v[0], v[3], v[2]} }
func (v *Vec4) ZXX() *Vec3  { return &Vec3{v[2], v[0], v[0]} }
func (v *Vec4) ZXXW() *Vec4 { return &Vec4{v[2], v[0], v[0], v[3]} }
func (v *Vec4) ZXXX() *Vec4 { return &Vec4{v[2], v[0], v[0], v[0]} }
func (v *Vec4) ZXXY() *Vec4 { return &Vec4{v[2], v[0], v[0], v[1]} }
func (v *Vec4) ZXXZ() *Vec4 { return &Vec4{v[2], v[0], v[0], v[2]} }
func (v *Vec4) ZXY() *Vec3  { return &Vec3{v[2], v[0], v[1]} }
func (v *Vec4) ZXYW() *Vec4 { return &Vec4{v[2], v[0], v[1], v[3]} }
func (v *Vec4) ZXYX() *Vec4 { return &Vec4{v[2], v[0], v[1], v[0]} }
func (v *Vec4) ZXYY() *Vec4 { return &Vec4{v[2], v[0], v[1], v[1]} }
func (v *Vec4) ZXYZ() *Vec4 { return &Vec4{v[2], v[0], v[1], v[2]} }
func (v *Vec4) ZXZ() *Vec3  { return &Vec3{v[2], v[0], v[2]} }
func (v *Vec4) ZXZW() *Vec4 { return &Vec4{v[2], v[0], v[2], v[3]} }
func (v *Vec4) ZXZX() *Vec4 { return &Vec4{v[2], v[0], v[2], v[0]} }
func (v *Vec4) ZXZY() *Vec4 { return &Vec4{v[2], v[0], v[2], v[1]} }
func (v *Vec4) ZXZZ() *Vec4 { return &Vec4{v[2], v[0], v[2], v[2]} }
func (v *Vec4) ZY() *Vec2   { return &Vec2{v[2], v[1]} }
func (v *Vec4) ZYW() *Vec3  { return &Vec3{v[2], v[1], v[3]} }
func (v *Vec4) ZYWW() *Vec4 { return &Vec4{v[2], v[1], v[3], v[3]} }
func (v *Vec4) ZYWX() *Vec4 { return &Vec4{v[2], v[1], v[3], v[0]} }
func (v *Vec4) ZYWY() *Vec4 { return &Vec4{v[2], v[1], v[3], v[1]} }
func (v *Vec4) ZYWZ() *Vec4 { return &Vec4{v[2], v[1], v[3], v[2]} }
func (v *Vec4) ZYX() *Vec3  { return &Vec3{v[2], v[1], v[0]} }
func (v *Vec4) ZYXW() *Vec4 { return &Vec4{v[2], v[1], v[0], v[3]} }
func (v *Vec4) ZYXX() *Vec4 { return &Vec4{v[2], v[1], v[0], v[0]} }
func (v *Vec4) ZYXY() *Vec4 { return &Vec4{v[2], v[1], v[0], v[1]} }
func (v *Vec4) ZYXZ() *Vec4 { return &Vec4{v[2], v[1], v[0], v[2]} }
func (v *Vec4) ZYY() *Vec3  { return &Vec3{v[2], v[1], v[1]} }
func (v *Vec4) ZYYW() *Vec4 { return &Vec4{v[2], v[1], v[1], v[3]} }
func (v *Vec4) ZYYX() *Vec4 { return &Vec4{v[2], v[1], v[1], v[0]} }
func (v *Vec4) ZYYY() *Vec4 { return &Vec4{v[2], v[1], v[1], v[1]} }
func (v *Vec4) ZYYZ() *Vec4 { return &Vec4{v[2], v[1], v[1], v[2]} }
func (v *Vec4) ZYZ() *Vec3  { return &Vec3{v[2], v[1], v[2]} }
func (v *Vec4) ZYZW() *Vec4 { return &Vec4{v[2], v[1], v[2], v[3]} }
func (v *Vec4) ZYZX() *Vec4 { return &Vec4{v[2], v[1], v[2], v[0]} }
func (v *Vec4) ZYZY() *Vec4 { return &Vec4{v[2], v[1], v[2], v[1]} }
func (v *Vec4) ZYZZ() *Vec4 { return &Vec4{v[2], v[1], v[2], v[2]} }
func (v *Vec4) ZZ() *Vec2   { return &Vec2{v[2], v[2]} }
func (v *Vec4) ZZW() *Vec3  { return &Vec3{v[2], v[2], v[3]} }
func (v *Vec4) ZZWW() *Vec4 { return &Vec4{v[2], v[2], v[3], v[3]} }
func (v *Vec4) ZZWX() *Vec4 { return &Vec4{v[2], v[2], v[3], v[0]} }
func (v *Vec4) ZZWY() *Vec4 { return &Vec4{v[2], v[2], v[3], v[1]} }
func (v *Vec4) ZZWZ() *Vec4 { return &Vec4{v[2], v[2], v[3], v[2]} }
func (v *Vec4) ZZX() *Vec3  { return &Vec3{v[2], v[2], v[0]} }
func (v *Vec4) ZZXW() *Vec4 { return &Vec4{v[2], v[2], v[0], v[3]} }
func (v *Vec4) ZZXX() *Vec4 { return &Vec4{v[2], v[2], v[0], v[0]} }
func (v *Vec4) ZZXY() *Vec4 { return &Vec4{v[2], v[2], v[0], v[1]} }
func (v *Vec4) ZZXZ() *Vec4 { return &Vec4{v[2], v[2], v[0], v[2]} }
func (v *Vec4) ZZY() *Vec3  { return &Vec3{v[2], v[2], v[1]} }
func (v *Vec4) ZZYW() *Vec4 { return &Vec4{v[2], v[2], v[1], v[3]} }
func (v *Vec4) ZZYX() *Vec4 { return &Vec4{v[2], v[2], v[1], v[0]} }
func (v *Vec4) ZZYY() *Vec4 { return &Vec4{v[2], v[2], v[1], v[1]} }
func (v *Vec4) ZZYZ() *Vec4 { return &Vec4{v[2], v[2], v[1], v[2]} }
func (v *Vec4) ZZZ() *Vec3  { return &Vec3{v[2], v[2], v[2]} }
func (v *Vec4) ZZZW() *Vec4 { return &Vec4{v[2], v[2], v[2], v[3]} }
func (v *Vec4) ZZZX() *Vec4 { return &Vec4{v[2], v[2], v[2], v[0]} }
func (v *Vec4) ZZZY() *Vec4 { return &Vec4{v[2], v[2], v[2], v[1]} }
func (v *Vec4) ZZZZ() *Vec4 { return &Vec4{v[2], v[2], v[2], v[2]} }

func (v *Vec2) X() float64 { return v[0] }
func (v *Vec2) Y() float64 { return v[1] }

func (v *Vec3) X() float64 { return v[0] }
func (v *Vec3) Y() float64 { return v[1] }
func (v *Vec3) Z() float64 { return v[2] }

func (v *Vec4) X() float64 { return v[0] }
func (v *Vec4) Y() float64 { return v[1] }
func (v *Vec4) Z() float64 { return v[2] }
func (v *Vec4) W() float64 { return v[3] }

func (v *Vec2) XYZ(z float64) *Vec3 {
	return &Vec3{v[0], v[1], z}
}

func (v *Vec2) XYZW(z, w float64) *Vec4 {
	return &Vec4{v[0], v[1], z, w}
}

func (v *Vec3) XYZW(w float64) *Vec4 {
	return &Vec4{v[0], v[1], v[2], w}
}

func (v *Vec2) Elem() (x, y float64) {
	return v[0], v[1]
}

func (v *Vec3) Elem() (x, y, z float64) {
	return v[0], v[1], v[2]
}

func (v *Vec4) Elem() (x, y, z, w float64) {
	return v[0], v[1], v[2], v[3]
}
