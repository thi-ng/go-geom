package math32

import (
	"math"
	"math/rand"

	"go.thi.ng/geom/math64"
)

type (
	ArrayF32 []float32
)

const (
	Eps float32 = float32(math64.Eps)

	Pi     = math.Pi
	Tau    = math.Pi * 2
	HalfPi = math.Pi / 2

	Deg2Rad = Pi / 180
	Rad2Deg = 180 / Pi

	Sqrt2 = 0.7071067811865475244008443621048490
)

func Abs(x float32) float32 {
	if x < 0 {
		return -x
	}
	return x
}

func AbsDiff(a, b float32) float32 {
	if a < b {
		return b - a
	}
	return a - b
}

func Sign(x float32) float32 {
	if x < 0 {
		return -1
	}
	if x > 0 {
		return 1
	}
	return 0
}

func Min(a, b float32) float32 {
	if a < b {
		return a
	}
	return b
}

func Min3(a, b, c float32) float32 {
	return Min(Min(a, b), c)
}

func Max(a, b float32) float32 {
	if a > b {
		return a
	}
	return b
}

func Max3(a, b, c float32) float32 {
	return Max(Max(a, b), c)
}

func Clamp(x, a, b float32) float32 {
	if x < a {
		return a
	}
	if x > b {
		return b
	}
	return x
}

func EqDelta(x, y, eps float32) bool {
	return AbsDiff(x, y) <= eps
}

func InRange(x, min, max float32) bool {
	return x >= min && x <= max
}

func Radians(x float32) float32 {
	return x * Deg2Rad
}

func Degrees(x float32) float32 {
	return x * Rad2Deg
}

func Sin(x float32) float32 {
	return float32(math.Sin(float64(x)))
}

func Cos(x float32) float32 {
	return float32(math.Cos(float64(x)))
}

func Tan(x float32) float32 {
	return float32(math.Tan(float64(x)))
}

func Asin(x float32) float32 {
	return float32(math.Asin(float64(x)))
}

func Acos(x float32) float32 {
	return float32(math.Acos(float64(x)))
}

func Atan2(y, x float32) float32 {
	return float32(math.Atan2(float64(y), float64(x)))
}

func Atan2Abs(y, x float32) float32 {
	t := Atan2(y, x)
	if t < 0 {
		return t + math.Pi
	}
	return t
}

func Sqrt(x float32) float32 {
	return float32(math.Sqrt(float64(x)))
}

func Pow(x, y float32) float32 {
	return float32(math.Pow(float64(x), float64(y)))
}

func Exp(x float32) float32 {
	return float32(math.Exp(float64(x)))
}

func Floor(x float32) float32 {
	return float32(math.Floor(float64(x)))
}

func Ceil(x float32) float32 {
	return float32(math.Ceil(float64(x)))
}

func Frac(x float32) float32 {
	return x - Floor(x)
}

func Mod(x, y float32) float32 {
	return x - y*Floor(x/y)
}

func Mix(a, b, t float32) float32 {
	return (b-a)*t + a
}

// MixBilinear computes bilinear interpolation between a, b, c, d at
// normalized position u, v.
//
//     C   D   V
//     +---+   ^
//     |   |   |
//     +---+   +----> U
//     A   B
//
func MixBilinear(a, b, c, d, u, v float32) float32 {
	t := a + (b-a)*u
	return t + (c+(d-c)*u-t)*v
}

func MixCircular(a, b, t float32) float32 {
	t = 1 - t
	return (b-a)*Sqrt(1-t*t) + a
}

func MixCos(a, b, t float32) float32 {
	return (a-b)*(Cos(t*math.Pi)*0.5+0.5) + b
}

func MixExp(a, b, t, e float32) float32 {
	return (b-a)*Pow(t, e) + a
}

func MixDecimated(a, b, t, n float32) float32 {
	return (b-a)*Floor(t*n)/n + a
}

func Step(e, x float32) float32 {
	if x < e {
		return 0
	}
	return 1
}

func SmoothStep(e, e2, x float32) float32 {
	t := Clamp((x-e)/(e2-e), 0, 1)
	return (-2*t + 3) * t * t
}

func SmootherStep(e, e2, x float32) float32 {
	t := Clamp((x-e)/(e2-e), 0, 1)
	return t * t * t * (t*(t*6-15) + 10)
}

func Impulse(k, t float32) float32 {
	h := k * t
	return h * Exp(1-h)
}

func RandNorm() float32 {
	return rand.Float32()*2 - 1
}

func RandMinMax(min, max float32) float32 {
	return min + (max-min)*rand.Float32()
}
