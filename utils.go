package geom

import (
	"go.thi.ng/dcons"
	"go.thi.ng/geom/vec"
)

func CopyVertexList2(src *dcons.DCons, closed bool) *dcons.DCons {
	dest := src.Map(func(v dcons.Any) dcons.Any {
		return v.(*vec.Vec2).Copy()
	})
	if closed {
		dest.Circular()
	}
	return dest
}

func CopyVertexList3(src *dcons.DCons, closed bool) *dcons.DCons {
	dest := src.Map(func(v dcons.Any) dcons.Any {
		return v.(*vec.Vec3).Copy()
	})
	if closed {
		dest.Circular()
	}
	return dest
}

func CentroidFromVertexList2(verts *dcons.DCons) *vec.Vec2 {
	c := &vec.Vec2{}
	for i, n := verts.Head, verts.Len(); n > 0; n-- {
		c.Add(i.Value.(*vec.Vec2))
		i = i.Next
	}
	return c.DivN(float64(verts.Len()))
}

func CentroidFromVertexList3(verts *dcons.DCons) *vec.Vec3 {
	c := &vec.Vec3{}
	for i, n := verts.Head, verts.Len(); n > 0; n-- {
		c.Add(i.Value.(*vec.Vec3))
		i = i.Next
	}
	return c.DivN(float64(verts.Len()))
}

func ArcLengthVertexList2(verts *dcons.DCons, closed bool) float64 {
	len := 0.0
	n := verts.Len()
	if n > 0 {
		var m int
		if closed {
			m = 0
		} else {
			m = 1
		}
		for i, j := verts.Head, verts.Head.Next; n > m; n-- {
			len += i.Value.(*vec.Vec2).Dist(j.Value.(*vec.Vec2))
			i = j
			j = j.Next
		}
	}
	return len
}

func ArcLengthVertexList3(verts *dcons.DCons, closed bool) float64 {
	len := 0.0
	n := verts.Len()
	if n > 0 {
		var m int
		if closed {
			m = 0
		} else {
			m = 1
		}
		for i, j := verts.Head, verts.Head.Next; n > m; n-- {
			len += i.Value.(*vec.Vec3).Dist(j.Value.(*vec.Vec3))
			i = j
			j = j.Next
		}
	}
	return len
}
