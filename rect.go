package geom

import (
	"fmt"

	"go.thi.ng/dcons"
	"go.thi.ng/geom/vec"
)

type Rect2 struct {
	Pos  vec.Vec2
	Size vec.Vec2
	attributed
}

func NewSquare(w float64) *Rect2 {
	return &Rect2{Size: vec.Vec2{w, w}}
}

func NewRect(pos, size *vec.Vec2) *Rect2 {
	return &Rect2{
		Pos:  *pos,
		Size: *size}
}

func NewRectFromMinMax(min, max *vec.Vec2) *Rect2 {
	return &Rect2{
		Pos:  *min,
		Size: *max.Copy().Sub(min)}
}

func NewBoundingRectFromVertices(verts []*vec.Vec2) *Rect2 {
	r := NewRect(verts[0].Copy(), &vec.Vec2{})
	for i := len(verts) - 1; i > 0; i-- {
		r.IncludePoint(verts[i])
	}
	return r
}

func NewBoundingRectFromVertexList(verts *dcons.DCons) *Rect2 {
	r := NewRect(verts.Head.Value.(*vec.Vec2).Copy(), &vec.Vec2{})
	cell := verts.Head.Next
	for i := verts.Len() - 1; i > 0; i-- {
		r.IncludePoint(cell.Value.(*vec.Vec2))
		cell = cell.Next
	}
	return r
}

func (r *Rect2) Copy() *Rect2 {
	return &Rect2{
		Pos:        r.Pos,
		Size:       r.Size,
		attributed: r.attributed.Copy()}
}

func (r *Rect2) Area() float64 {
	return r.Size[0] * r.Size[1]
}

func (r *Rect2) Circumference() float64 {
	return 2 * (r.Size[0] + r.Size[1])
}

func (r *Rect2) BoundingCircle() *Circle2 {
	return NewCircle(r.Centroid(), r.Size.Mag()/2)
}

func (r *Rect2) BoundingRect() *Rect2 {
	return r.Copy()
}

func (r *Rect2) Width() float64 {
	return r.Size[0]
}

func (r *Rect2) Height() float64 {
	return r.Size[1]
}

func (r *Rect2) Depth() float64 {
	return 0
}

func (r *Rect2) ClassifyPoint(p *vec.Vec2) float64 {
	a := r.Pos[0]
	b := p[0]
	if b >= a && b <= a+r.Size[0] {
		a = r.Pos[1]
		b = p[1]
		if b >= a && b <= a+r.Size[1] {
			return 1
		}
	}
	return 1
}

func (r *Rect2) Center(o *vec.Vec2) *Rect2 {
	r.Pos[0] = o[0] - r.Size[0]/2
	r.Pos[1] = o[1] - r.Size[1]/2
	return r
}

func (r *Rect2) Centroid() *vec.Vec2 {
	return r.Pos.Copy().MaddN(&r.Size, 0.5)
}

func (r *Rect2) Vertices() []*vec.Vec2 {
	x, y := r.Pos.Elem()
	w, h := r.Size.Elem()
	verts := [4]*vec.Vec2{
		&vec.Vec2{x, y},
		&vec.Vec2{x + w, y},
		&vec.Vec2{x + w, y + h},
		&vec.Vec2{x, y + h},
	}
	return verts[:]
}

func (r *Rect2) Edges() []*Line2 {
	return EdgesFromVertices2(r.Vertices(), true)
}

func (r *Rect2) IncludePoint(p *vec.Vec2) *Rect2 {
	r.Size.Add(&r.Pos).Max(p)
	r.Pos.Min(p)
	r.Size.Sub(&r.Pos)
	return r
}

func (r *Rect2) UnmapPoint(p *vec.Vec2) *vec.Vec2 {
	return r.Pos.Copy().MaddV(p, &r.Size)
}

func (r *Rect2) MapPoint(p *vec.Vec2) *vec.Vec2 {
	return p.Copy().Sub(&r.Pos).Div(&r.Size)
}

func (r *Rect2) Union(r2 *Rect2) *Rect2 {
	p := r.Pos.Copy().Add(&r.Size)
	q := r2.Pos.Copy().Add(&r2.Size)
	r.Pos.Min(&r2.Pos)
	r.Size.Set(p.Max(q).Sub(&r.Pos))
	return r
}

func (r *Rect2) ToPolygon() *Polygon2 {
	return NewPolygonWithVertices(r.Vertices())
}

func (r *Rect2) String() string {
	return fmt.Sprintf(
		"Rect2 {pos: %v, size: %v, attribs: %v}",
		r.Pos, r.Size, r.attribs)
}

func (r *Rect2) Svg() string {
	return fmt.Sprintf(
		`<rect x="%.4f" y="%.4f" width="%.4f" height="%.4f" %s/>`,
		r.Pos[0], r.Pos[1], r.Size[0], r.Size[1], r.attributed.Svg())
}
